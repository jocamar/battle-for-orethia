#ifndef SCENARIOPAUSEMENU_H
#define SCENARIOPAUSEMENU_H

#include "allegro5/allegro5.h"
#include "allegro5/allegro_font.h"

#include "menu.h"
#include "ScenarioExitMenu.h"

#define SCENARIO_PAUSE_MENU_TEXT_COLOR					al_map_rgb(255,255,255) //white
#define SCENARIO_PAUSE_MENU_BLOCKED_COLOR				al_map_rgb(150,150,150)  //grey
#define SCENARIO_PAUSE_MENU_BACK_COLOR					al_map_rgba(0,0,0,220)  //transparent black

#define SCENARIO_PAUSE_MENU_OPTION1					"Save"
#define SCENARIO_PAUSE_MENU_OPTION2					"Map"
#define SCENARIO_PAUSE_MENU_OPTION3					"Next Turn"
#define SCENARIO_PAUSE_MENU_OPTION4					"Exit"

class ScenarioScreen;
class Scenario;

class ScenarioPauseMenu : public Menu
{
	static int const ScenarioPauseMenuDefaultSelectedOption = 2;

	enum Fonts {UNSELECTED, SELECTED};
	enum textCoords {TEXT_X = 640, TEXT_INIT_UNSELECTED_Y = 200, TEXT_INIT_SELECTED_Y = 190, TEXT_UNSELECTED_SPACING = 100, TEXT_SELECTED_SPACING = 100};
	enum Coords {MENU_LEFT_LIMIT = 490, MENU_RIGHT_LIMIT = 790, MENU_UPPER_LIMIT = 150, MENU_LOWER_LIMIT = 600};
	enum OptionNames {SAVE,MAP,NEXT_TURN,EXIT};

	ScenarioScreen* screen;
	Scenario* scenario;

	ScenarioExitMenu* exitMenu;

public:

	ScenarioPauseMenu(ScenarioScreen* screen, Scenario* scenario, ALLEGRO_FONT* unselected, ALLEGRO_FONT* selected);

	void update(bool* keys);
	void render() const;

	~ScenarioPauseMenu();
};

#endif