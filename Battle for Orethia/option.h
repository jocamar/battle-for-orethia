#ifndef OPTION_H
#define OPTION_H

#include <string>

using namespace std;

struct Option
{
	string Name;
	bool active;

	Option(string name = "Default", bool active = true);
};

#endif