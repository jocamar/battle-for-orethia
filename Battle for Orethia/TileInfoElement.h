#ifndef TILEINFOELEMENT_H
#define TILEINFOELEMENT_H

#include "UIelement.h"
#include "Sprite.h"
#include "allegro5/allegro5.h"
#include "allegro5/allegro_font.h"

#define SWITCH_THRESHOLD 8
#define TILE_INFO_SPEED 40

#define TILE_INFO_TEXT_COLOR al_map_rgb(0,0,0)

class Tile;

class TileInfoElement : public UI_element
{
	Sprite background;
	Sprite defenseIcon;
	Tile* tile;
	ALLEGRO_FONT* font;
	ALLEGRO_FONT* smallfont;
	bool toTheRight, hasUnit;
	int state;
	int textTitle_x, textTitle_y;
	int text_defense_x, text_defense_y;
	int textUnitName_x, textUnitName_y;
	int textAmmo_x, textAmmo_y;
	int textHealth_x, textHealth_y;

	enum Coords{
				RIGHT_X = 930, 
				LEFT_X = 15, 
				TILE_INFO_Y = 450,
				TILE_INFO_WIDTH = 330, 
				TILE_INFO_HEIGHT = 243,
				TILE_SPRITE_OFFSET_X = 42,
				TILE_SPRITE_OFFSET_Y = 50,
				TEXT_TITLE_OFFSET_Y = 140,
				DEFENSE_ICON_OFFSET_X = 30,
				DEFENSE_ICON_OFFSET_Y = 190,
				DEFENSE_ICON_SIZE = 21,
				TEXT_DEFENSE_OFFSET_X = 60,
				TEXT_DEFENSE_OFFSET_Y = 187,
				TEXT_AMMO_OFFSET_X = 30,
				TEXT_AMMO_OFFSET_Y = 180,
				TEXT_HEALTH_OFFSET_Y = 200
	};
	enum States{RAISING, FALLING, STOPPED};
	enum Backgrounds {WITHOUT_UNIT, WITH_UNIT, DEFENSE_ICON = 9};

public:
	TileInfoElement() {};
	TileInfoElement(Scenario* scenario, ALLEGRO_BITMAP* background = NULL, ALLEGRO_BITMAP* icons = NULL, ALLEGRO_FONT* font = NULL, ALLEGRO_FONT* smallfont = NULL);
	void update();
	void render() const;
};

#endif