#ifndef UNITATTACKMENU_H
#define UNITATTACKMENU_H

#include "menu.h"
#include "allegro5/allegro5.h"
#include "allegro5/allegro_font.h"

#define UNIT_ATTACK_MENU_TEXT_COLOR					al_map_rgb(0,0,0) //black
#define UNIT_ATTACK_MENU_BACK_COLOR					al_map_rgba(0,0,0,220)  //transparent black
#define ROTATION_SPEED 1
#define MOVE_DELAY 2

class Scenario;
class Unit;
class Tile;

class UnitAttackMenu : public Menu
{
	static int const UnitAttackMenuDefaultSelectedOption = 0;

	enum Fonts {SMALL, MEDIUM};
	enum Coords {CURSOR_OFFSET = 10,
				CURSOR_SIZE = 100
	};
	enum States {MOVING_TO_TARGET, AT_TARGET, EXITING};


	Scenario* scenario;
	Tile* unitTile;
	vector <Unit* > attackOptions;
	int  cursor_x, cursor_y;
	int cursorAngle;
	int selectedTarget;
	int attackType;
	int state;
	int moveCounter;
	ALLEGRO_BITMAP* cursor;

	int getNextUnit(char direction, int currUnit) const;
public:
	enum AttackTypes {MELEE, RANGED};
	UnitAttackMenu(Scenario* scenario, ALLEGRO_FONT* smallfont, ALLEGRO_FONT* mediumfont, ALLEGRO_BITMAP* cursor, vector<Unit*> targets, Tile* unitTile, int attackType);

	void update(bool* keys);
	void render() const;

	~UnitAttackMenu();
};



#endif