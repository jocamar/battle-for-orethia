/*#include "MountainTile.h"
#include "unit.h"

float const MountainTile::mountainDefence = 0.3;
string const  MountainTile::mountainTypeA = "MountainTall";
string const  MountainTile::mountainTypeB = "MountainShort";
string const  MountainTile::mountainTitle = "Mountain";

MountainTile::MountainTile(int x,int y, string type, ALLEGRO_BITMAP* image) : Tile(x, y)
{
	traversable = true;
	traversableByHeavy = false;
	bonusDefense = MountainTile::mountainDefence;
	moveCost = MountainTile::mountainMoveCost;
	resourceBonus = MountainTile::mountainResourceBonus;
	this->tileName = MountainTile::mountainTitle;

	this->type = type;

	if(type == MountainTile::mountainTypeA)
	{
		this->sprite = Sprite(image, x, y, MountainTile::mountainFrameNum, TILE_WIDTH, TILE_HEIGHT, MountainTile::mountainAnimationDelay, MOUNTAIN_TALL_BOTTOM_X*TILE_WIDTH, MOUNTAIN_TALL_BOTTOM_Y*TILE_HEIGHT);
		this->mountainTop = Sprite(image, x, y-TILE_HEIGHT, MountainTile::mountainFrameNum, TILE_WIDTH, TILE_HEIGHT, MountainTile::mountainAnimationDelay, MOUNTAIN_TALL_TOP_X*TILE_WIDTH, MOUNTAIN_TALL_TOP_Y*TILE_HEIGHT);
	}
	else if (type == MountainTile::mountainTypeB)
	{
		this->sprite = Sprite(image, x, y, MountainTile::mountainFrameNum, TILE_WIDTH, TILE_HEIGHT, MountainTile::mountainAnimationDelay, MOUNTAIN_SHORT_X*TILE_WIDTH, MOUNTAIN_SHORT_Y*TILE_HEIGHT);
	}

	this->infoSprite = Sprite(image, 0, 0, 1, TILE_WIDTH, TILE_HEIGHT, MountainTile::mountainAnimationDelay, MOUNTAIN_SHORT_X*TILE_WIDTH, MOUNTAIN_SHORT_Y*TILE_HEIGHT);
}

void MountainTile::render() const
{
	if(sprite.getImage() != NULL)
	{
		sprite.render();

		if(type == MountainTile::mountainTypeA)
		{
			mountainTop.render();
		}
	}
}

void MountainTile::renderInfoSprite() const
{
	if(infoSprite.getImage() != NULL)
	{
		infoSprite.render();
	}
}

void MountainTile::updateInfoSprite(int x, int y)
{
	infoSprite.setPos(x,y);
}

void MountainTile::update(int camX, int camY)
{
	sprite.update();

	int actualX = (x - camX) * TILE_WIDTH;
	int actualY = (y - camY) * TILE_HEIGHT;

	sprite.setPos(actualX,actualY);

	if(type == MountainTile::mountainTypeA)
	{
		mountainTop.update();
		mountainTop.setPos(actualX,actualY-TILE_HEIGHT);
	}
}

void MountainTile::renderPreview(int posX, int posY) const
{
	int actualX = (x * TILE_PREVIEW_WIDTH) + posX;
	int actualY = (y * TILE_PREVIEW_HEIGHT) + posY;

	al_draw_filled_rectangle(actualX,actualY, actualX + TILE_PREVIEW_WIDTH, actualY + TILE_PREVIEW_HEIGHT,MOUNTAIN_COLOR);
}

MountainTile* MountainTile::clone() const
{
	return new MountainTile(*this);
}

MountainTile::~MountainTile()
{
}

void MountainTile::setSprite(ALLEGRO_BITMAP* newSprite)
{
	sprite.setImage(newSprite);
	mountainTop.setImage(newSprite);
	infoSprite.setImage(newSprite);
	infoTop.setImage(newSprite);
}*/