#ifndef BATTLEMENUSCREEN_H
#define BATTLEMENUSCREEN_H

#include "screen.h"
#include "allegro5/allegro5.h"
#include "allegro5/allegro_font.h"
#include "game.h"
#include "ScenarioScreen.h"
#include "BattleMainMenu.h"
#include "BarracksTile.h"
#include "BeachTile.h"
#include "DirtRoadTile.h"
#include "ForestTile.h"
#include "GrassTile.h"
#include "MountainTile.h"
#include "SandTile.h"
#include "VillageTile.h"
#include "WaterTile.h"
#include "FortressTile.h"


#define TITLE_TEXT "Battle Setup"

#define BATTLEMENU_OVERLAY_COLOR						al_map_rgba(0,0,0,220) //transparent black
#define BATTLEMENU_TITLE_TEXT_COLOR						al_map_rgb(255,255,255)
#define TRANSPARENT_BATTLEMENU							al_map_rgb(255,0,255) //magenta

class BattleMenuScreen : public Screen
{
	enum ImageID {MMENU_BACKGROUND};
	enum FontSizes {SIZE_SMALL = 35, SIZE_MEDIUM = 45};
	enum Players {PLAYER1 = 1, PLAYER2, PLAYER3, PLAYER4};
	enum OverlayCoords {LEFT_LIMIT = 0, RIGHT_LIMIT = 1280, UPPER_LIMIT = 0, LOWER_LIMIT = 720};
	enum TitleTextCoords {TITLE_TEXT_X = 640, TITLE_TEXT_Y = 40};
	enum ScenarioOptions {CHOSEN_SCENARIO, STARTING_RES, PLAYER_1, PLAYER_2, PLAYER_3, PLAYER_4};
	enum PlayerChoices {NO_PLAYER, PLAYER, CPU};

	ALLEGRO_FONT* mediumFont;
	ALLEGRO_FONT* smallFont;

	BattleMainMenu* mainMenu;

	vector<int> scenarioOptions;
	vector<Map> maps;

	void drawBackground() const;
	void drawOverlay() const;

public:
	BattleMenuScreen(Game* game);

	void update(bool* keys);
	void render() const;

	void switchToMain();
	void switchToScenario(Scenario* scenario);

	int getNumOfSelectedPlayers() const;
	bool atLeastOneHuman() const;
	Map* getChosenMap();

	int getChosenMapIndex() const;
	int getStartingRes() const;
	int getPlayer1() const;
	int getPlayer2() const;
	int getPlayer3() const;
	int getPlayer4() const;

	void setChosenMapIndex(int newVal);
	void setStartingRes(int newVal);
	void setPlayer1(int newVal);
	void setPlayer2(int newVal);
	void setPlayer3(int newVal);
	void setPlayer4(int newVal);

	vector <Map> & getMaps();

	~BattleMenuScreen();
};

#endif