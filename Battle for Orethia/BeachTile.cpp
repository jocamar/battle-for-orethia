/*#include "BeachTile.h"
#include "unit.h"

float const BeachTile::beachDefense = -0.1;
string const BeachTile::beachTypeA = "WaterSandRight";
string const BeachTile::beachTypeB = "WaterSandLeft";
string const BeachTile::beachTypeC = "WaterSandTop";
string const BeachTile::beachTypeD = "WaterSandBottom";
string const BeachTile::beachTypeE = "WaterSandUpperLeft";
string const BeachTile::beachTypeF = "WaterSandUpperRight";
string const BeachTile::beachTypeG = "WaterSandLowerLeft";
string const BeachTile::beachTypeH = "WaterSandLowerRight";
string const BeachTile::beachTypeI = "SandWaterUpperLeft";
string const BeachTile::beachTypeJ = "SandWaterUpperRight";
string const BeachTile::beachTypeL = "SandWaterLowerLeft";
string const BeachTile::beachTypeM = "SandWaterLowerRight";
string const BeachTile::beachTitle = "Beach";

BeachTile::BeachTile(int x,int y, string type, ALLEGRO_BITMAP* image) : Tile(x, y)
{
	traversable = true;
	traversableByHeavy = true;
	bonusDefense = BeachTile::beachDefense;
	moveCost = BeachTile::beachMoveCost;
	resourceBonus = BeachTile::beachResourceBonus;
	this->tileName = BeachTile::beachTitle;

	this->type = type;

	if(type == BeachTile::beachTypeA)
	{
		this->sprite = Sprite(image, x, y, BeachTile::beachFrameNum, TILE_WIDTH, TILE_HEIGHT, BeachTile::beachAnimationDelay, WATER_SAND_RIGHT_X*TILE_WIDTH, WATER_SAND_RIGHT_Y*TILE_HEIGHT);
	}
	else if (type == BeachTile::beachTypeB)
	{
		this->sprite = Sprite(image, x, y, BeachTile::beachFrameNum, TILE_WIDTH, TILE_HEIGHT, BeachTile::beachAnimationDelay, WATER_SAND_LEFT_X*TILE_WIDTH, WATER_SAND_LEFT_Y*TILE_HEIGHT);
	}
	else if (type == BeachTile::beachTypeC)
	{
		this->sprite = Sprite(image, x, y, BeachTile::beachFrameNum, TILE_WIDTH, TILE_HEIGHT, BeachTile::beachAnimationDelay, WATER_SAND_TOP_X*TILE_WIDTH, WATER_SAND_TOP_Y*TILE_HEIGHT);
	}
	else if (type == BeachTile::beachTypeD)
	{
		this->sprite = Sprite(image, x, y, BeachTile::beachFrameNum, TILE_WIDTH, TILE_HEIGHT, BeachTile::beachAnimationDelay, WATER_SAND_BOTTOM_X*TILE_WIDTH, WATER_SAND_BOTTOM_Y*TILE_HEIGHT);
	}
		else if (type == BeachTile::beachTypeE)
	{
		this->sprite = Sprite(image, x, y, BeachTile::beachFrameNum, TILE_WIDTH, TILE_HEIGHT, BeachTile::beachAnimationDelay, WATER_SAND_UPPER_LEFT_X*TILE_WIDTH, WATER_SAND_UPPER_LEFT_Y*TILE_HEIGHT);
	}
	else if (type == BeachTile::beachTypeF)
	{
		this->sprite = Sprite(image, x, y, BeachTile::beachFrameNum, TILE_WIDTH, TILE_HEIGHT, BeachTile::beachAnimationDelay, WATER_SAND_UPPER_RIGHT_X*TILE_WIDTH, WATER_SAND_UPPER_RIGHT_Y*TILE_HEIGHT);
	}
	else if (type == BeachTile::beachTypeG)
	{
		this->sprite = Sprite(image, x, y, BeachTile::beachFrameNum, TILE_WIDTH, TILE_HEIGHT, BeachTile::beachAnimationDelay, WATER_SAND_LOWER_LEFT_X*TILE_WIDTH, WATER_SAND_LOWER_LEFT_Y*TILE_HEIGHT);
	}
		else if (type == BeachTile::beachTypeH)
	{
		this->sprite = Sprite(image, x, y, BeachTile::beachFrameNum, TILE_WIDTH, TILE_HEIGHT, BeachTile::beachAnimationDelay, WATER_SAND_LOWER_RIGHT_X*TILE_WIDTH, WATER_SAND_LOWER_RIGHT_Y*TILE_HEIGHT);
	}
	else if (type == BeachTile::beachTypeI)
	{
		this->sprite = Sprite(image, x, y, BeachTile::beachFrameNum, TILE_WIDTH, TILE_HEIGHT, BeachTile::beachAnimationDelay, SAND_WATER_UPPER_LEFT_X*TILE_WIDTH, SAND_WATER_UPPER_LEFT_Y*TILE_HEIGHT);
	}
	else if (type == BeachTile::beachTypeJ)
	{
		this->sprite = Sprite(image, x, y, BeachTile::beachFrameNum, TILE_WIDTH, TILE_HEIGHT, BeachTile::beachAnimationDelay, SAND_WATER_UPPER_RIGHT_X*TILE_WIDTH, SAND_WATER_UPPER_RIGHT_Y*TILE_HEIGHT);
	}
		else if (type == BeachTile::beachTypeL)
	{
		this->sprite = Sprite(image, x, y, BeachTile::beachFrameNum, TILE_WIDTH, TILE_HEIGHT, BeachTile::beachAnimationDelay, SAND_WATER_LOWER_LEFT_X*TILE_WIDTH, SAND_WATER_LOWER_LEFT_Y*TILE_HEIGHT);
	}
	else if (type == BeachTile::beachTypeM)
	{
		this->sprite = Sprite(image, x, y, BeachTile::beachFrameNum, TILE_WIDTH, TILE_HEIGHT, BeachTile::beachAnimationDelay, SAND_WATER_LOWER_RIGHT_X*TILE_WIDTH, SAND_WATER_LOWER_RIGHT_Y*TILE_HEIGHT);
	}

	this->infoSprite = Sprite(image, 0, 0, 1, TILE_WIDTH, TILE_HEIGHT, BeachTile::beachAnimationDelay, WATER_SAND_LEFT_X*TILE_WIDTH, WATER_SAND_LEFT_Y*TILE_HEIGHT);
}

void BeachTile::renderPreview(int posX, int posY) const
{
	int actualX = (x * TILE_PREVIEW_WIDTH) + posX;
	int actualY = (y * TILE_PREVIEW_HEIGHT) + posY;

	al_draw_filled_rectangle(actualX,actualY, actualX + TILE_PREVIEW_WIDTH, actualY + TILE_PREVIEW_HEIGHT,BEACH_COLOR);
}

BeachTile* BeachTile::clone() const
{
	return new BeachTile(*this);
}

BeachTile::~BeachTile()
{
}*/