/*#include "VillageTile.h"
#include "unit.h"
#include <iostream>

float const VillageTile::villageDefence = 0.4;
string const VillageTile::villageTypeA = "VillageRed";
string const VillageTile::villageTypeB = "VillageBlue";
string const VillageTile::villageTypeC = "VillageGreen";
string const VillageTile::villageTypeD = "VillageYellow";
string const VillageTile::villageTypeE = "VillageWhite";
string const VillageTile::villageTitle = "Village";


VillageTile::VillageTile(int x,int y, string type, ALLEGRO_BITMAP* image) : Tile(x, y)
{
	traversable = true;
	traversableByHeavy = true;
	bonusDefense = VillageTile::villageDefence;
	moveCost = VillageTile::villageMoveCost;
	resourceBonus = VillageTile::villageResourceBonus;
	loyalty = VillageTile::villageLoyalty;
	this->tileName = VillageTile::villageTitle;

	this->type = type;

	for(unsigned int i = 0; i < PLAYER_NUM; i++)
	{
		this->lastKnownType[i] = this->type;
	}

	if(type == VillageTile::villageTypeA)
	{
		this->sprite = Sprite(image, x, y, VillageTile::villageFrameNum, TILE_WIDTH, TILE_HEIGHT, VillageTile::villageAnimationDelay, VILLAGE_RED_X*TILE_WIDTH, VILLAGE_RED_Y*TILE_HEIGHT);
		this->infoSprite = Sprite(image, 0, 0, 1, TILE_WIDTH, TILE_HEIGHT, VillageTile::villageAnimationDelay, VILLAGE_RED_X*TILE_WIDTH, VILLAGE_RED_Y*TILE_HEIGHT);
	}
	else if (type == VillageTile::villageTypeB)
	{
		this->sprite = Sprite(image, x, y, VillageTile::villageFrameNum, TILE_WIDTH, TILE_HEIGHT, VillageTile::villageAnimationDelay, VILLAGE_BLUE_X*TILE_WIDTH, VILLAGE_BLUE_Y*TILE_HEIGHT);
		this->infoSprite = Sprite(image, 0, 0, 1, TILE_WIDTH, TILE_HEIGHT, VillageTile::villageAnimationDelay, VILLAGE_BLUE_X*TILE_WIDTH, VILLAGE_BLUE_Y*TILE_HEIGHT);
	}
	else if (type == VillageTile::villageTypeC)
	{
		this->sprite = Sprite(image, x, y, VillageTile::villageFrameNum, TILE_WIDTH, TILE_HEIGHT, VillageTile::villageAnimationDelay, VILLAGE_GREEN_X*TILE_WIDTH, VILLAGE_GREEN_Y*TILE_HEIGHT);
		this->infoSprite = Sprite(image, 0, 0, 1, TILE_WIDTH, TILE_HEIGHT, VillageTile::villageAnimationDelay, VILLAGE_GREEN_X*TILE_WIDTH, VILLAGE_GREEN_Y*TILE_HEIGHT);
	}
	else if (type == VillageTile::villageTypeD)
	{
		this->sprite = Sprite(image, x, y, VillageTile::villageFrameNum, TILE_WIDTH, TILE_HEIGHT, VillageTile::villageAnimationDelay, VILLAGE_YELLOW_X*TILE_WIDTH, VILLAGE_YELLOW_Y*TILE_HEIGHT);
		this->infoSprite = Sprite(image, 0, 0, 1, TILE_WIDTH, TILE_HEIGHT, VillageTile::villageAnimationDelay, VILLAGE_YELLOW_X*TILE_WIDTH, VILLAGE_YELLOW_Y*TILE_HEIGHT);
	}
	else if (type == VillageTile::villageTypeE)
	{
		this->sprite = Sprite(image, x, y, VillageTile::villageFrameNum, TILE_WIDTH, TILE_HEIGHT, VillageTile::villageAnimationDelay, VILLAGE_WHITE_X*TILE_WIDTH, VILLAGE_WHITE_Y*TILE_HEIGHT);
		this->infoSprite = Sprite(image, 0, 0, 1, TILE_WIDTH, TILE_HEIGHT, VillageTile::villageAnimationDelay, VILLAGE_WHITE_X*TILE_WIDTH, VILLAGE_WHITE_Y*TILE_HEIGHT);
	}
}

bool VillageTile::isOwnable() const
{
	return true;
}

void VillageTile::renderPreview(int posX, int posY) const
{
	int actualX = (x * TILE_PREVIEW_WIDTH) + posX;
	int actualY = (y * TILE_PREVIEW_HEIGHT) + posY;

	if(type == VillageTile::villageTypeA)
	{
		al_draw_filled_rectangle(actualX,actualY, actualX + TILE_PREVIEW_WIDTH, actualY + TILE_PREVIEW_HEIGHT,VILLAGE_RED_COLOR);
	}
	else if (type == VillageTile::villageTypeB)
	{
		al_draw_filled_rectangle(actualX, actualY, actualX + TILE_PREVIEW_WIDTH, actualY + TILE_PREVIEW_HEIGHT,VILLAGE_BLUE_COLOR);
	}
	else if (type == VillageTile::villageTypeC)
	{
		al_draw_filled_rectangle(actualX, actualY, actualX + TILE_PREVIEW_WIDTH, actualY + TILE_PREVIEW_HEIGHT,VILLAGE_GREEN_COLOR);
	}
	else if (type == VillageTile::villageTypeD)
	{
		al_draw_filled_rectangle(actualX, actualY, actualX + TILE_PREVIEW_WIDTH, actualY + TILE_PREVIEW_HEIGHT,VILLAGE_YELLOW_COLOR);
	}
	else if (type == VillageTile::villageTypeE)
	{
		al_draw_filled_rectangle(actualX, actualY, actualX + TILE_PREVIEW_WIDTH, actualY + TILE_PREVIEW_HEIGHT,VILLAGE_WHITE_COLOR);
	}
}

void VillageTile::setOwnership(char color)
{
	if(color == 'r')
	{
		this->type = VillageTile::villageTypeA;
	}
	else if (color == 'b')
	{
		this->type = VillageTile::villageTypeB;
	}
	else if (color == 'g')
	{
		this->type = VillageTile::villageTypeC;
	}
	else if (color == 'y')
	{
		this->type = VillageTile::villageTypeD;
	}
	else if (color == 'w')
	{
		this->type = VillageTile::villageTypeE;
	}
}

char VillageTile::getOwnership() const
{
	if(type == VillageTile::villageTypeA)
	{
		return 'r';
	}
	else if (type == VillageTile::villageTypeB)
	{
		return 'b';
	}
	else if (type == VillageTile::villageTypeC)
	{
		return 'g';
	}
	else if (type == VillageTile::villageTypeD)
	{
		return 'y';
	}
	else if (type == VillageTile::villageTypeE)
	{
		return 'w';
	}

	return '\0';
}

char VillageTile::getPrevOwnershipFor(int playerNum) const
{
	if(lastKnownType[playerNum] == VillageTile::villageTypeA)
	{
		return 'r';
	}
	else if (lastKnownType[playerNum] == VillageTile::villageTypeB)
	{
		return 'b';
	}
	else if (lastKnownType[playerNum] == VillageTile::villageTypeC)
	{
		return 'g';
	}
	else if (lastKnownType[playerNum] == VillageTile::villageTypeD)
	{
		return 'y';
	}
	else if (lastKnownType[playerNum] == VillageTile::villageTypeE)
	{
		return 'w';
	}

	return '\0';
}

VillageTile* VillageTile::clone() const
{
	return new VillageTile(*this);
}

VillageTile::~VillageTile()
{
}

void VillageTile::updateTileFor(int playerNum)
{
	this->lastKnownType[playerNum] = this->type;
}

void VillageTile::switchSpriteFor(int playerNum)
{
	if(this->lastKnownType[playerNum] == VillageTile::villageTypeA)
	{
		this->sprite.setAnimationStartX(VILLAGE_RED_X*TILE_WIDTH);
		this->sprite.setAnimationStartY(VILLAGE_RED_Y*TILE_HEIGHT);
		this->infoSprite.setAnimationStartX(VILLAGE_RED_X*TILE_WIDTH);
		this->infoSprite.setAnimationStartY(VILLAGE_RED_Y*TILE_HEIGHT);
	}
	else if (this->lastKnownType[playerNum] == VillageTile::villageTypeB)
	{
		this->sprite.setAnimationStartX(VILLAGE_BLUE_X*TILE_WIDTH);
		this->sprite.setAnimationStartY(VILLAGE_BLUE_Y*TILE_HEIGHT);
		this->infoSprite.setAnimationStartX(VILLAGE_BLUE_X*TILE_WIDTH);
		this->infoSprite.setAnimationStartY(VILLAGE_BLUE_Y*TILE_HEIGHT);
	}
	else if (this->lastKnownType[playerNum] == VillageTile::villageTypeC)
	{
		this->sprite.setAnimationStartX(VILLAGE_GREEN_X*TILE_WIDTH);
		this->sprite.setAnimationStartY(VILLAGE_GREEN_Y*TILE_HEIGHT);
		this->infoSprite.setAnimationStartX(VILLAGE_GREEN_X*TILE_WIDTH);
		this->infoSprite.setAnimationStartY(VILLAGE_GREEN_Y*TILE_HEIGHT);
	}
	else if (this->lastKnownType[playerNum] == VillageTile::villageTypeD)
	{
		this->sprite.setAnimationStartX(VILLAGE_YELLOW_X*TILE_WIDTH);
		this->sprite.setAnimationStartY(VILLAGE_YELLOW_Y*TILE_HEIGHT);
		this->infoSprite.setAnimationStartX(VILLAGE_YELLOW_X*TILE_WIDTH);
		this->infoSprite.setAnimationStartY(VILLAGE_YELLOW_Y*TILE_HEIGHT);
	}
	else if (this->lastKnownType[playerNum] == VillageTile::villageTypeE)
	{
		this->sprite.setAnimationStartX(VILLAGE_WHITE_X*TILE_WIDTH);
		this->sprite.setAnimationStartY(VILLAGE_WHITE_Y*TILE_HEIGHT);
		this->infoSprite.setAnimationStartX(VILLAGE_WHITE_X*TILE_WIDTH);
		this->infoSprite.setAnimationStartY(VILLAGE_WHITE_Y*TILE_HEIGHT);
	}
}

void VillageTile::resetLoyalty()
{
	loyalty = VillageTile::villageLoyalty;
}

void VillageTile::capture()
{
	unitOnTile->startCapturing();
	loyalty -= unitOnTile->getConvertedHP();

	if(loyalty <= 0)
	{
		setOwnership(unitOnTile->getColor());
		resetLoyalty();
		unitOnTile->stopCapturing();
	}
}


int VillageTile::getDefaultLoyalty() const
{
	return VillageTile::villageLoyalty;
}*/