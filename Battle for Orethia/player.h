#ifndef PLAYER_H
#define PLAYER_H

#include "unit.h"
#include <vector>
#include <iostream>
#include <stdlib.h>
#include <allegro5/allegro.h>           //   Includes allegro
#include <allegro5/allegro_primitives.h>        //   Includes allegro primitives
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>

using namespace std;

#define CURSOR_SCALE 5

class Player
{
	int camX, camY;
	int cursorX, cursorY;

	vector <Unit*> units;
	
	unsigned int resources;
	bool human, active, defeated;
	char color;
	ALLEGRO_BITMAP* cursor;

public:
	Player(bool human = true, int camX = 0, int camY = 0, unsigned int resources = 5000, bool active = true, bool defeated = false, char color = 'r', ALLEGRO_BITMAP* cursor = NULL);
	Player(vector <Unit*> units, bool human = true, int camX = 0, int camY = 0, unsigned int resources = 5000, bool active = true, bool defeated = false, char color = 'r', ALLEGRO_BITMAP* cursor = NULL);

	int getCamX() const;
	int getCamY() const;
	int getCursorX() const;
	int getCursorY() const;
	char getColor() const;

	void setCamX(int newValue);
	void setCamY(int newValue);
	void setCursorImage(ALLEGRO_BITMAP* cursor);
	void addResources(int value);
	void removeResources(int value);

	vector <Unit*> getUnits() const;

	void addUnit(Unit* newUnit);
	void removeUnit(int index);
	void restUnits();
	void setUnitsTint(ALLEGRO_COLOR tint);

	unsigned int getResources() const;

	bool isHuman() const;
	bool isActive() const;
	bool isDefeated() const;

	void setActive(bool newValue);

	void update(Scenario* scenario);
	void animateUnits(int camX, int camY);
	void render() const;
	void renderUnits() const;

	~Player();

	void moveCursor(char direction, int mapSizeX, int mapSizeY);
};


#endif