#ifndef NEWBATTLEMENU_H
#define NEWBATTLEMENU_H

#include "menu.h"
#include "allegro5/allegro5.h"
#include "allegro5/allegro_font.h"
#include "BattlePlayerMenu.h"
#include "SelectScenarioMenu.h"

#define NEW_BATTLE_MENU_TEXT_COLOR				al_map_rgb(255,255,255) //white
#define NEW_BATTLE_MENU_BLOCKED_COLOR			al_map_rgb(150,150,150)  //grey
#define NEW_BATTLE_MENU_CONTOUR_COLOR			al_map_rgb(255,255,255) //white
#define NEW_BATTLE_MENU_OVERLAY_COLOR			al_map_rgba( 255*0.1f, 255*0.1f, 255*0.1f, 0.1f ) //transparent white

#define NEW_BATTLE_MENU_OPTION1					"Map:"
#define NEW_BATTLE_MENU_OPTION2					"Starting Resources:"
#define NEW_BATTLE_MENU_OPTION3					"Player 1"
#define NEW_BATTLE_MENU_OPTION4					"Player 2"
#define NEW_BATTLE_MENU_OPTION5					"Player 3"
#define NEW_BATTLE_MENU_OPTION6					"Player 4"
#define NEW_BATTLE_MENU_OPTION7					"Start"

class BattleMenuScreen;

class NewBattleMenu : public Menu
{
	static int const NewBattleMenuDefaultSelectedOption = 0;

	enum Fonts {FONT};
	enum textCoords {
					 TEXT_X = 160, 
					 INIT_OPTION_UNSELECTED_Y = 255, 
					 INIT_OPTION_SELECTED_Y = 250, 
					 OPTION2_UNSELECTED_SPACING = 200, 
					 OPTION2_SELECTED_SPACING = 200
					};
	enum ContourCoords {
						LEFT_LIMIT = 300,
						LEFTEST_LIMIT = 30,
						RIGHT_LIMIT = 1250, 
						UPPER_LIMIT = 80, 
						LOWER_LIMIT = 690, 
						OPTION1_UPPER_LIMIT = 230, 
						OPTION1_LOWER_LIMIT = 330, 
						OPTION2_UPPER_LIMIT = 430, 
						OPTION2_LOWER_LIMIT = 530,
						CONTOUR_THICKNESS = 4
					};
	enum Option1Coords {
						OPTION1_TEXT_X = 400,
						OPTION1_TEXT_Y = 150,
						OPTION1_MAP_NAME_X = 460,
						OPTION1_MAP_NAME_Y = 150,
						OPTION1_CONTOUR_LEFT_LIMIT = 450,
						OPTION1_CONTOUR_UPPER_LIMIT = 150,
						OPTION1_CONTOUR_RIGHT_LIMIT = 800,
						OPTION1_CONTOUR_LOWER_LIMIT = 200
					  };
	enum Option2Coords {
						OPTION2_TEXT_X = 370,
						OPTION2_TEXT_Y = 250,
						OPTION2_CONTOUR_LEFT_LIMIT = 750,
						OPTION2_CONTOUR_RIGHT_LIMIT = 900,
						OPTION2_CONTOUR_UPPER_LIMIT = 250,
						OPTION2_CONTOUR_LOWER_LIMIT = 300,
						OPTION2_TRIANGLE_TIP_Y = 275,
						OPTION2_LEFT_TRIANGLE_TIP_X = 720,
						OPTION2_LEFT_TRIANGLE_RIGHT_LIMIT = 740,
						OPTION2_RIGHT_TRIANGLE_TIP_X = 930,
						OPTION2_RIGHT_TRIANGLE_LEFT_LIMIT = 910,
						OPTION2_RESOURCE_AMOUNT_X = 825,
						OPTION2_RESOURCE_AMOUNT_Y = 252
					   };
	enum Option7Coords {
						OPTION7_TEXT_X = 1050,
						OPTION7_TEXT_Y = 600,
						OPTION7_CONTOUR_LEFT_LIMIT = 1020,
						OPTION7_CONTOUR_RIGHT_LIMIT = 1180,
						OPTION7_CONTOUR_UPPER_LIMIT = 600,
						OPTION7_CONTOUR_LOWER_LIMIT = 650
	                   };
	enum OptionNames {SELECTSCENARIO, STARTINGRES, P1, P2, P3, P4, START};
	enum Players {PLAYER_1, PLAYER_2, PLAYER_3, PLAYER_4};
	enum PlayerChoices {NO_PLAYER, PLAYER, CPU};
	enum Resources {MINIMUM_INCREMENT = 1000};

	//Parent
	Menu* mainBattleMenu;

	SelectScenarioMenu* selectScenario;
	BattlePlayerMenu* player1Menu;
	BattlePlayerMenu* player2Menu;
	BattlePlayerMenu* player3Menu;
	BattlePlayerMenu* player4Menu;

	BattleMenuScreen* screen;

public:

	NewBattleMenu(BattleMenuScreen* screen, Menu* mainBattleMenu, ALLEGRO_FONT* unselected, ALLEGRO_FONT* selected);

	void update(bool* keys);
	void render() const;

	void resetPlayers();

	~NewBattleMenu();
};



#endif