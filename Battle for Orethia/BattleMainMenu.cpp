#include "BattleMainMenu.h"
#include "game.h"
#include "BattleMenuScreen.h"

BattleMainMenu::BattleMainMenu(BattleMenuScreen* screen,  ALLEGRO_FONT* unselected, ALLEGRO_FONT* selected) : Menu(true, true, BattleMainMenu::BattleMainMenuDefaultSelectedOption)
{
	this->screen = screen;
	this->activeOption = -1;
	
	fonts.push_back(selected);
	fonts.push_back(unselected);

	newBattleMenu = new NewBattleMenu(screen,this, fonts[NORMAL], fonts[ACTIVE]);
	//loadBattleMenu = new LoadBattleMenu(screen);

	options.push_back(new Option(BATTLE_MAIN_MENU_OPTION1, true));
	options.push_back(new Option(BATTLE_MAIN_MENU_OPTION2, false));
}

void BattleMainMenu::update(bool* keys)
{
	if(selectedOption == NEW_BATTLE)
	{
		/*if(loadBattleMenu->isDrawing())
			loadBattleMenu->setDrawing(false);*/

		if(!newBattleMenu->isDrawing())
			newBattleMenu->setDrawing(true);
	}
	else
	{
		if(newBattleMenu->isDrawing())
			newBattleMenu->setDrawing(false);

		/*if(!loadBattleMenu->isDrawing())
			loadBattleMenu->setDrawing(true);*/
	}

	if(active)
	{
		if(keys[UP])
		{
			selectedOption = getNextActive(DIR_UP);

			keys[UP] = false;
		}
		else if (keys[DOWN])
		{
			selectedOption = getNextActive(DIR_DOWN);

			keys[DOWN] = false;
		}
		else if(keys[A] || keys[ENTER])
		{
			if(selectedOption == NEW_BATTLE)
			{
				keys[A] = false;
				keys[B] = false;
				keys[ESC] = false;
				keys[ENTER] = false;
				keys[UP] = false;
				keys[DOWN] = false;
				keys[RIGHT] = false;
				keys[LEFT] = false;

				this->active = false;
				activeOption = selectedOption;
				newBattleMenu->setActive(true);
			}
			else if(selectedOption == LOAD_BATTLE)
			{
				keys[A] = false;
				keys[B] = false;
				keys[ESC] = false;
				keys[ENTER] = false;
				keys[UP] = false;
				keys[DOWN] = false;
				keys[RIGHT] = false;
				keys[LEFT] = false;

				this->active = false;
				activeOption = selectedOption;
				//loadBattleMenu->setActive(true);
			}
		}
		else if(keys[B] || keys[ESC])
		{
			screen->switchToMain();
		}
	}
	else // This was moved here in order to stop crashing, later you can move it up if you stop making the poor screens kill themselves.
	{
		newBattleMenu->update(keys);
		//loadBattleMenu->update(keys);
	}
}

void BattleMainMenu::render() const
{
	if(draw)
	{
		drawContours();

		for(int i = 0; (unsigned)i < options.size(); i++)
		{
			if(options[i]->active)
			{
				if(activeOption == i)
				{
					al_draw_text(fonts[ACTIVE], BATTLE_MAIN_MENU_TEXT_COLOR, TEXT_X, INIT_OPTION_SELECTED_Y + i*OPTION2_SELECTED_SPACING, ALLEGRO_ALIGN_CENTRE, options[i]->Name.c_str());
				}
				else
					al_draw_text(fonts[NORMAL], BATTLE_MAIN_MENU_TEXT_COLOR, TEXT_X, INIT_OPTION_UNSELECTED_Y + i*OPTION2_UNSELECTED_SPACING, ALLEGRO_ALIGN_CENTRE, options[i]->Name.c_str());
			}
			else
				al_draw_text(fonts[NORMAL], BATTLE_MAIN_MENU_BLOCKED_COLOR, TEXT_X, INIT_OPTION_UNSELECTED_Y + i*OPTION2_UNSELECTED_SPACING, ALLEGRO_ALIGN_CENTRE, options[i]->Name.c_str());

			if(i == selectedOption)
			{
				if(i == NEW_BATTLE)
				{
					al_draw_line(LEFT_LIMIT, OPTION1_UPPER_LIMIT, LEFTEST_LIMIT, OPTION1_UPPER_LIMIT, BATTLE_MAIN_MENU_CONTOUR_COLOR, CONTOUR_THICKNESS);
					al_draw_line(LEFTEST_LIMIT, OPTION1_UPPER_LIMIT, LEFTEST_LIMIT, OPTION1_LOWER_LIMIT, BATTLE_MAIN_MENU_CONTOUR_COLOR, CONTOUR_THICKNESS);
					al_draw_line(LEFTEST_LIMIT, OPTION1_LOWER_LIMIT, LEFT_LIMIT, OPTION1_LOWER_LIMIT, BATTLE_MAIN_MENU_CONTOUR_COLOR, CONTOUR_THICKNESS);
				}
				else if(i == LOAD_BATTLE)
				{
					al_draw_line(LEFT_LIMIT, OPTION2_UPPER_LIMIT, LEFTEST_LIMIT, OPTION2_UPPER_LIMIT, BATTLE_MAIN_MENU_CONTOUR_COLOR, CONTOUR_THICKNESS);
					al_draw_line(LEFTEST_LIMIT, OPTION2_UPPER_LIMIT, LEFTEST_LIMIT, OPTION2_LOWER_LIMIT, BATTLE_MAIN_MENU_CONTOUR_COLOR, CONTOUR_THICKNESS);
					al_draw_line(LEFTEST_LIMIT, OPTION2_LOWER_LIMIT, LEFT_LIMIT, OPTION2_LOWER_LIMIT, BATTLE_MAIN_MENU_CONTOUR_COLOR, CONTOUR_THICKNESS);
				}
			}
			else
			{
				if(i == NEW_BATTLE)
				{
					al_draw_line(LEFT_LIMIT, OPTION1_UPPER_LIMIT, LEFT_LIMIT, OPTION1_LOWER_LIMIT, BATTLE_MAIN_MENU_CONTOUR_COLOR, CONTOUR_THICKNESS);
				}
				else if(i == LOAD_BATTLE)
				{
					al_draw_line(LEFT_LIMIT, OPTION2_UPPER_LIMIT, LEFT_LIMIT, OPTION2_LOWER_LIMIT, BATTLE_MAIN_MENU_CONTOUR_COLOR, CONTOUR_THICKNESS);
				}
			}
		}
	}

	newBattleMenu->render();
	//loadBattleMenu->render();
}

void BattleMainMenu::drawContours() const
{
	al_draw_line(LEFT_LIMIT, UPPER_LIMIT, TITLE_LEFT_LIMIT, UPPER_LIMIT, BATTLE_MAIN_MENU_CONTOUR_COLOR, CONTOUR_THICKNESS);
	al_draw_line(LEFT_LIMIT, UPPER_LIMIT, LEFT_LIMIT, OPTION1_UPPER_LIMIT, BATTLE_MAIN_MENU_CONTOUR_COLOR, CONTOUR_THICKNESS);
	al_draw_line(LEFT_LIMIT, OPTION1_LOWER_LIMIT, LEFT_LIMIT, OPTION2_UPPER_LIMIT, BATTLE_MAIN_MENU_CONTOUR_COLOR, CONTOUR_THICKNESS);
	al_draw_line(LEFT_LIMIT, OPTION2_LOWER_LIMIT, LEFT_LIMIT, LOWER_LIMIT, BATTLE_MAIN_MENU_CONTOUR_COLOR, CONTOUR_THICKNESS);
	al_draw_line(LEFT_LIMIT, LOWER_LIMIT, RIGHT_LIMIT, LOWER_LIMIT, BATTLE_MAIN_MENU_CONTOUR_COLOR, CONTOUR_THICKNESS);
	al_draw_line(RIGHT_LIMIT, LOWER_LIMIT, RIGHT_LIMIT, UPPER_LIMIT, BATTLE_MAIN_MENU_CONTOUR_COLOR, CONTOUR_THICKNESS);
	al_draw_line(RIGHT_LIMIT, UPPER_LIMIT, TITLE_RIGHT_LIMIT, UPPER_LIMIT, BATTLE_MAIN_MENU_CONTOUR_COLOR, CONTOUR_THICKNESS);
}

BattleMainMenu::~BattleMainMenu()
{
	delete newBattleMenu;
	//delete loadBattleMenu;
}
