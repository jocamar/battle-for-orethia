/*#include "GrassTile.h"
#include "unit.h"

float const GrassTile::grassDefense = 0.0;
string const GrassTile::grassTitle = "Plains";

GrassTile::GrassTile(int x,int y, ALLEGRO_BITMAP* image) : Tile(x, y)
{
	traversable = true;
	traversableByHeavy = true;
	bonusDefense = GrassTile::grassDefense;
	moveCost = GrassTile::grassMoveCost;
	resourceBonus = GrassTile::grassResourceBonus;
	this->tileName = GrassTile::grassTitle;

	this->sprite = Sprite(image, x, y, GrassTile::grassFrameNum, TILE_WIDTH, TILE_HEIGHT, GrassTile::grassAnimationDelay, GRASS_X*TILE_WIDTH, GRASS_Y*TILE_HEIGHT);
	this->infoSprite = Sprite(image, 0, 0, 1, TILE_WIDTH, TILE_HEIGHT, GrassTile::grassAnimationDelay, GRASS_X*TILE_WIDTH, GRASS_Y*TILE_HEIGHT);
}

void GrassTile::renderPreview(int posX, int posY) const
{
	int actualX = (x * TILE_PREVIEW_WIDTH) + posX;
	int actualY = (y * TILE_PREVIEW_HEIGHT) + posY;

	al_draw_filled_rectangle(actualX,actualY, actualX + TILE_PREVIEW_WIDTH, actualY + TILE_PREVIEW_HEIGHT,GRASS_COLOR);
}

GrassTile* GrassTile::clone() const
{
	return new GrassTile(*this);
}

GrassTile::~GrassTile()
{
}*/