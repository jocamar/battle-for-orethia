/*#include "SandTile.h"
#include "unit.h"

float const SandTile::sandDefense = -0.1;
string const SandTile::sandTypeA = "SandGrassUpperLeftCorner";
string const SandTile::sandTypeB = "SandGrassUpperRightCorner";
string const SandTile::sandTypeC = "SandGrassLowerLeftCorner";
string const SandTile::sandTypeD = "SandGrassLowerRightCorner";
string const SandTile::sandTypeE = "GrassSandUpperLeftCorner";
string const SandTile::sandTypeF = "GrassSandUpperRightCorner";
string const SandTile::sandTypeG = "GrassSandLowerLeftCorner";
string const SandTile::sandTypeH = "GrassSandLowerRightCorner";
string const SandTile::sandTypeI = "GrassSandRight";
string const SandTile::sandTypeJ = "GrassSandLeft";
string const SandTile::sandTypeL = "GrassSandBottom";
string const SandTile::sandTypeM = "GrassSandTop";
string const SandTile::sandTypeN = "Sand";
string const SandTile::sandTitle = "Sand";

SandTile::SandTile(int x,int y, string type, ALLEGRO_BITMAP* image) : Tile(x, y)
{
	traversable = true;
	traversableByHeavy = true;
	bonusDefense = SandTile::sandDefense;
	moveCost = SandTile::sandMoveCost;
	resourceBonus = SandTile::sandResourceBonus;
	this->tileName = SandTile::sandTitle;

	this->type = type;

	if(type == SandTile::sandTypeA)
	{
		this->sprite = Sprite(image, x, y, SandTile::sandFrameNum, TILE_WIDTH, TILE_HEIGHT, SandTile::sandAnimationDelay, SAND_GRASS_UPPER_LEFT_CORNER_X*TILE_WIDTH, SAND_GRASS_UPPER_LEFT_CORNER_Y*TILE_HEIGHT);
	}
	else if (type == SandTile::sandTypeB)
	{
		this->sprite = Sprite(image, x, y, SandTile::sandFrameNum, TILE_WIDTH, TILE_HEIGHT, SandTile::sandAnimationDelay, SAND_GRASS_UPPER_RIGHT_CORNER_X*TILE_WIDTH, SAND_GRASS_UPPER_RIGHT_CORNER_Y*TILE_HEIGHT);
	}
	else if (type == SandTile::sandTypeC)
	{
		this->sprite = Sprite(image, x, y, SandTile::sandFrameNum, TILE_WIDTH, TILE_HEIGHT, SandTile::sandAnimationDelay, SAND_GRASS_LOWER_LEFT_CORNER_X*TILE_WIDTH, SAND_GRASS_LOWER_LEFT_CORNER_Y*TILE_HEIGHT);
	}
	else if (type == SandTile::sandTypeD)
	{
		this->sprite = Sprite(image, x, y, SandTile::sandFrameNum, TILE_WIDTH, TILE_HEIGHT, SandTile::sandAnimationDelay, SAND_GRASS_LOWER_RIGHT_CORNER_X*TILE_WIDTH, SAND_GRASS_LOWER_RIGHT_CORNER_Y*TILE_HEIGHT);
	}
		else if (type == SandTile::sandTypeE)
	{
		this->sprite = Sprite(image, x, y, SandTile::sandFrameNum, TILE_WIDTH, TILE_HEIGHT, SandTile::sandAnimationDelay, GRASS_SAND_UPPER_LEFT_CORNER_X*TILE_WIDTH, GRASS_SAND_UPPER_LEFT_CORNER_Y*TILE_HEIGHT);
	}
	else if (type == SandTile::sandTypeF)
	{
		this->sprite = Sprite(image, x, y, SandTile::sandFrameNum, TILE_WIDTH, TILE_HEIGHT, SandTile::sandAnimationDelay, GRASS_SAND_UPPER_RIGHT_CORNER_X*TILE_WIDTH, GRASS_SAND_UPPER_RIGHT_CORNER_Y*TILE_HEIGHT);
	}
	else if (type == SandTile::sandTypeG)
	{
		this->sprite = Sprite(image, x, y, SandTile::sandFrameNum, TILE_WIDTH, TILE_HEIGHT, SandTile::sandAnimationDelay, GRASS_SAND_LOWER_LEFT_CORNER_X*TILE_WIDTH, GRASS_SAND_LOWER_LEFT_CORNER_Y*TILE_HEIGHT);
	}
		else if (type == SandTile::sandTypeH)
	{
		this->sprite = Sprite(image, x, y, SandTile::sandFrameNum, TILE_WIDTH, TILE_HEIGHT, SandTile::sandAnimationDelay, GRASS_SAND_LOWER_RIGHT_CORNER_X*TILE_WIDTH, GRASS_SAND_LOWER_RIGHT_CORNER_Y*TILE_HEIGHT);
	}
	else if (type == SandTile::sandTypeI)
	{
		this->sprite = Sprite(image, x, y, SandTile::sandFrameNum, TILE_WIDTH, TILE_HEIGHT, SandTile::sandAnimationDelay, GRASS_SAND_RIGHT_X*TILE_WIDTH, GRASS_SAND_RIGHT_Y*TILE_HEIGHT);
	}
	else if (type == SandTile::sandTypeJ)
	{
		this->sprite = Sprite(image, x, y, SandTile::sandFrameNum, TILE_WIDTH, TILE_HEIGHT, SandTile::sandAnimationDelay, GRASS_SAND_LEFT_X*TILE_WIDTH, GRASS_SAND_LEFT_Y*TILE_HEIGHT);
	}
		else if (type == SandTile::sandTypeL)
	{
		this->sprite = Sprite(image, x, y, SandTile::sandFrameNum, TILE_WIDTH, TILE_HEIGHT, SandTile::sandAnimationDelay, GRASS_SAND_BOTTOM_X*TILE_WIDTH, GRASS_SAND_BOTTOM_Y*TILE_HEIGHT);
	}
	else if (type == SandTile::sandTypeM)
	{
		this->sprite = Sprite(image, x, y, SandTile::sandFrameNum, TILE_WIDTH, TILE_HEIGHT, SandTile::sandAnimationDelay, GRASS_SAND_TOP_X*TILE_WIDTH, GRASS_SAND_TOP_Y*TILE_HEIGHT);
	}
	else if (type == SandTile::sandTypeN)
	{
		this->sprite = Sprite(image, x, y, SandTile::sandFrameNum, TILE_WIDTH, TILE_HEIGHT, SandTile::sandAnimationDelay, SAND_X*TILE_WIDTH, SAND_Y*TILE_HEIGHT);
	}

	this->infoSprite = Sprite(image, 0, 0, 1, TILE_WIDTH, TILE_HEIGHT, SandTile::sandAnimationDelay, SAND_X*TILE_WIDTH, SAND_Y*TILE_HEIGHT);
}

void SandTile::renderPreview(int posX, int posY) const
{
	int actualX = (x * TILE_PREVIEW_WIDTH) + posX;
	int actualY = (y * TILE_PREVIEW_HEIGHT) + posY;

	al_draw_filled_rectangle(actualX,actualY, actualX + TILE_PREVIEW_WIDTH, actualY + TILE_PREVIEW_HEIGHT,SAND_COLOR);
}

SandTile* SandTile::clone() const
{
	return new SandTile(*this);
}

SandTile::~SandTile()
{
}*/