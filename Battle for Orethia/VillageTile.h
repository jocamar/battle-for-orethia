#ifndef VILLAGETILE_H
#define VILLAGETILE_H
/*
#include "tile.h"

#define VILLAGE_RED_COLOR			al_map_rgb(255,0,0)
#define VILLAGE_BLUE_COLOR			al_map_rgb(0,0,255)
#define VILLAGE_GREEN_COLOR			al_map_rgb(0,255,0)
#define VILLAGE_YELLOW_COLOR		al_map_rgb(255,255,0)
#define VILLAGE_WHITE_COLOR			al_map_rgb(255,255,255)

class VillageTile : public Tile
{
	static float const villageDefence;
	static int const villageFrameNum = 4;
	static int const villageMoveCost = 2;
	static int const villageAnimationDelay = 15;
	static int const villageResourceBonus = 1000;
	static int const villageLoyalty = 20;
	static string const villageTypeA;
	static string const villageTypeB;
	static string const villageTypeC;
	static string const villageTypeD;
	static string const villageTypeE;
	static string const villageTitle;

	enum TileTypeAnimations {
							VILLAGE_RED_X = 1, VILLAGE_RED_Y = 0,
							VILLAGE_BLUE_X = 1, VILLAGE_BLUE_Y = 1, 
							VILLAGE_GREEN_X = 1, VILLAGE_GREEN_Y = 2,
							VILLAGE_YELLOW_X = 1, VILLAGE_YELLOW_Y = 3,
							VILLAGE_WHITE_X = 1, VILLAGE_WHITE_Y = 4
	};

	string type;
	string lastKnownType[PLAYER_NUM];

public:
	VillageTile(int x,int y,string type, ALLEGRO_BITMAP* image);

	bool isOwnable() const;

	void updateTileFor(int playerNum);
	void switchSpriteFor(int playerNum);

	void renderPreview(int x, int y) const;

	void setOwnership(char color);
	char getOwnership() const;
	char getPrevOwnershipFor(int playerNum) const;
	void resetLoyalty();
	int getDefaultLoyalty() const;
	void capture();

	VillageTile* clone() const;
	~VillageTile();
};*/

#endif