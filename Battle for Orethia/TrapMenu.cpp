#include "TrapMenu.h"
#include "game.h"
#include "scenario.h"

TrapMenu::TrapMenu(Scenario* scenario, Tile* tile, ALLEGRO_FONT* font, ALLEGRO_BITMAP* background) : Menu(false, false, TrapMenu::TrapMenuDefaultSelectedOption)
{
	this->scenario = scenario;
	this->tile = tile;
	this->font = font;
	this->counter = 0;

	x = (tile->getX()*TileInfo::TILE_WIDTH) - (scenario->getActivePlayer()->getCamX() * TileInfo::TILE_WIDTH);
	y = (tile->getY()*TileInfo::TILE_HEIGHT) - (scenario->getActivePlayer()->getCamY() * TileInfo::TILE_HEIGHT);

	if(x < WIDTH/2)
		x += MENUWIDTH;
	else 
		x -= MENUWIDTH;

	this->background = background;
}

void TrapMenu::update(bool* keys)
{
	if(active)
	{
		if(counter >= TRAP_WARNING_TIME || keys[A] || keys[B] || keys[ESC] || keys[ENTER])
		{
			keys[A] = false;
			keys[B] = false;
			keys[ESC] = false;
			keys[ENTER] = false;

			scenario->stopSelectedUnitOnTile(tile);
		}
		else
			counter++;

		scenario->updateTileInfo();
	}
}

void TrapMenu::render() const
{
	if(draw)
	{
		scenario->renderBanner();
		scenario->renderTileInfo();

		al_draw_bitmap(background,x,y,0);
		al_draw_text(font,TRAP_TEXT_COLOR,x+MENUWIDTH/2, y + TEXT_Y_OFFSET,ALLEGRO_ALIGN_CENTRE,TRAP_TEXT);
	}
}

TrapMenu::~TrapMenu()
{
}