#include "BattleMenuScreen.h"


BattleMenuScreen::BattleMenuScreen(Game* game) : Screen(game)
{
	images.push_back(al_load_bitmap("resources/images/Background.png"));

	for(unsigned int i = 0; i < images.size(); i++)
	{
		al_convert_mask_to_alpha(images[i], TRANSPARENT_BATTLEMENU);
	}
	smallFont = al_load_font("resources/fonts/coolvetica rg.ttf", SIZE_SMALL, 0);
	mediumFont = al_load_font("resources/fonts/coolvetica rg.ttf", SIZE_MEDIUM, 0);
	
	try {
		maps = Map::loadMapsFromFolder("\\BattleScenarios\\");
	}
	catch (gameException& e)
	{
		e.getInfo();
	}
	
	mainMenu = new BattleMainMenu(this, smallFont, mediumFont);

	scenarioOptions.push_back(-1);
	scenarioOptions.push_back(5000);
	scenarioOptions.push_back(NO_PLAYER);
	scenarioOptions.push_back(NO_PLAYER);
	scenarioOptions.push_back(NO_PLAYER);
	scenarioOptions.push_back(NO_PLAYER);
}

void BattleMenuScreen::update(bool* keys)
{
	mainMenu->update(keys);
}

void BattleMenuScreen::render() const
{
	drawBackground();
	drawOverlay();
	mainMenu->render();
}

BattleMenuScreen::~BattleMenuScreen() 
{
	al_destroy_font(smallFont);
	al_destroy_font(mediumFont);

	delete mainMenu;

	/*for(unsigned int i = 0; i < maps.size(); i++)
	{
		delete maps[i];
	}*/
}

void BattleMenuScreen::drawBackground() const
{
	al_draw_bitmap(images[MMENU_BACKGROUND],0,0,0);
}

void BattleMenuScreen::switchToMain()
{
	game->changeScreen(new MainMenuScreen(game));
}

void BattleMenuScreen::switchToScenario(Scenario* scenario)
{
	game->changeScreen(new ScenarioScreen(game, scenario));
}

void BattleMenuScreen::drawOverlay() const
{
	al_draw_filled_rectangle(LEFT_LIMIT, UPPER_LIMIT, RIGHT_LIMIT, LOWER_LIMIT, BATTLEMENU_OVERLAY_COLOR);
	al_draw_text(mediumFont, BATTLEMENU_TITLE_TEXT_COLOR, TITLE_TEXT_X, TITLE_TEXT_Y, ALLEGRO_ALIGN_CENTRE, TITLE_TEXT);
}

/*Tile* BattleMenuScreen::loadTile(char tiletype, int x, int y)
{
	Tile* tmpTile = NULL;

	string type;

	if(tiletype == 'a')
	{
		type = "FortressRed";
		tmpTile = new FortressTile(x, y, type, NULL);
	}
	else if (tiletype == 'b')
	{
		type = "FortressBlue";
		tmpTile = new FortressTile(x, y, type, NULL);
	}
	else if (tiletype == 'c')
	{
		type = "FortressGreen";
		tmpTile = new FortressTile(x, y, type, NULL);
	}
	else if (tiletype == 'd')
	{
		type = "FortressYellow";
		tmpTile = new FortressTile(x, y, type, NULL);
	}
	else if (tiletype == 'e')
	{
		type = "FortressWhite";
		tmpTile = new FortressTile(x, y, type, NULL);
	}
	else if (tiletype == 'f')
	{
		type = "VillageRed";
		tmpTile = new VillageTile(x, y, type, NULL);
	}
	else if (tiletype == 'g')
	{
		type = "VillageBlue";
		tmpTile = new VillageTile(x, y, type, NULL);
	}
	else if (tiletype == 'h')
	{
		type = "VillageGreen";
		tmpTile = new VillageTile(x, y, type, NULL);
	}
	else if (tiletype == 'i')
	{
		type = "VillageYellow";
		tmpTile = new VillageTile(x, y, type, NULL);
	}
	else if (tiletype == 'j')
	{
		type = "VillageWhite";
		tmpTile = new VillageTile(x, y, type, NULL);
	}
	else if (tiletype == 'l')
	{
		type = "BarracksRed";
		tmpTile = new BarracksTile(x, y, type, NULL);
	}
	else if (tiletype == 'm')
	{
		type = "BarracksBlue";
		tmpTile = new BarracksTile(x, y, type, NULL);
	}
	else if (tiletype == 'n')
	{
		type = "BarracksGreen";
		tmpTile = new BarracksTile(x, y, type, NULL);
	}
	else if (tiletype == 'o')
	{
		type = "BarracksYellow";
		tmpTile = new BarracksTile(x, y, type, NULL);
	}
	else if (tiletype == 'p')
	{
		type = "BarracksWhite";
		tmpTile = new BarracksTile(x, y, type, NULL);
	}
	else if (tiletype == 'q')
	{
		type = "MountainTall";
		tmpTile = new MountainTile(x, y, type, NULL);
	}
	else if (tiletype == 'r')
	{
		type = "MountainShort";
		tmpTile = new MountainTile(x, y, type, NULL);
	}
	else if (tiletype == 's')
	{
		type = "DirtRoadHorizontal";
		tmpTile = new DirtRoadTile(x, y, type, NULL);
	}
	else if (tiletype == 't')
	{
		type = "DirtRoadVertical";
		tmpTile = new DirtRoadTile(x, y, type, NULL);
	}
	else if (tiletype == 'u')
	{
		type = "DirtRoadCross";
		tmpTile = new DirtRoadTile(x, y, type, NULL);
	}
	else if (tiletype == 'v')
	{
		type = "DirtRoadTSectionDown";
		tmpTile = new DirtRoadTile(x, y, type, NULL);
	}
	else if (tiletype == 'x')
	{
		type = "DirtRoadTSectionLeft";
		tmpTile = new DirtRoadTile(x, y, type, NULL);
	}
	else if (tiletype == 'y')
	{
		type = "DirtRoadTSectionUp";
		tmpTile = new DirtRoadTile(x, y, type, NULL);
	}
	else if (tiletype == 'z')
	{
		type = "DirtRoadTSectionRight";
		tmpTile = new DirtRoadTile(x, y, type, NULL);
	}
	else if (tiletype == '!')
	{
		type = "DirtRoadEndUp";
		tmpTile = new DirtRoadTile(x, y, type, NULL);
	}
	else if (tiletype == '"')
	{
		type = "DirtRoadEndLeft";
		tmpTile = new DirtRoadTile(x, y, type, NULL);
	}
	else if (tiletype == '#')
	{
		type = "DirtRoadEndRight";
		tmpTile = new DirtRoadTile(x, y, type, NULL);
	}
	else if (tiletype == '$')
	{
		type = "DirtRoadEndDown";
		tmpTile = new DirtRoadTile(x, y, type, NULL);
	}
	else if (tiletype == '%')
	{
		type = "DirtRoadCurveUpLeft";
		tmpTile = new DirtRoadTile(x, y, type, NULL);
	}
	else if (tiletype == '&')
	{
		type = "DirtRoadCurveUpRight";
		tmpTile = new DirtRoadTile(x, y, type, NULL);
	}
	else if (tiletype == '/')
	{
		type = "DirtRoadCurveDownLeft";
		tmpTile = new DirtRoadTile(x, y, type, NULL);
	}
	else if (tiletype == '(')
	{
		type = "DirtRoadCurveDownRight";
		tmpTile = new DirtRoadTile(x, y, type, NULL);
	}
	else if (tiletype == ')')
	{
		type = "Forest";
		tmpTile = new ForestTile(x, y, type, NULL);
	}
	else if (tiletype == '=')
	{
		type = "ForestTop";
		tmpTile = new ForestTile(x, y, type, NULL);
	}
	else if (tiletype == '?')
	{
		type = "ForestSide";
		tmpTile = new ForestTile(x, y, type, NULL);
	}
	else if (tiletype == '�')
	{
		type = "ForestTopSide";
		tmpTile = new ForestTile(x, y, type, NULL);
	}
	else if (tiletype == '+')
	{
		type = "SandGrassUpperLeftCorner";
		tmpTile = new SandTile(x, y, type, NULL);
	}
	else if (tiletype == '*')
	{
		type = "SandGrassUpperRightCorner";
		tmpTile = new SandTile(x, y, type, NULL);
	}
	else if (tiletype == '<')
	{
		type = "SandGrassLowerLeftCorner";
		tmpTile = new SandTile(x, y, type, NULL);
	}
	else if (tiletype == '>')
	{
		type = "SandGrassLowerRightCorner";
		tmpTile = new SandTile(x, y, type, NULL);
	}
	else if (tiletype == '.')
	{
		type = "GrassSandUpperLeftCorner";
		tmpTile = new SandTile(x, y, type, NULL);
	}
	else if (tiletype == ',')
	{
		type = "GrassSandUpperRightCorner";
		tmpTile = new SandTile(x, y, type, NULL);
	}
	else if (tiletype == ';')
	{
		type = "GrassSandLowerLeftCorner";
		tmpTile = new SandTile(x, y, type, NULL);
	}
	else if (tiletype == ':')
	{
		type = "GrassSandLowerRightCorner";
		tmpTile = new SandTile(x, y, type, NULL);
	}
	else if (tiletype == '-')
	{
		type = "GrassSandRight";
		tmpTile = new SandTile(x, y, type, NULL);
	}
	else if (tiletype == '_')
	{
		type = "GrassSandLeft";
		tmpTile = new SandTile(x, y, type, NULL);
	}
	else if (tiletype == 'A')
	{
		type = "GrassSandBottom";
		tmpTile = new SandTile(x, y, type, NULL);
	}
	else if (tiletype == '�')
	{
		type = "GrassSandTop";
		tmpTile = new SandTile(x, y, type, NULL);
	}
	else if (tiletype == '}')
	{
		type = "WaterSandRight";
		tmpTile = new BeachTile(x, y, type, NULL);
	}
	else if (tiletype == ']')
	{
		type = "WaterSandLeft";
		tmpTile = new BeachTile(x, y, type, NULL);
	}
	else if (tiletype == '[')
	{
		type = "WaterSandTop";
		tmpTile = new BeachTile(x, y, type, NULL);
	}
	else if (tiletype == '{')
	{
		type = "WaterSandBottom";
		tmpTile = new BeachTile(x, y, type, NULL);
	}
	else if (tiletype == 'B')
	{
		type = "WaterSandUpperLeft";
		tmpTile = new BeachTile(x, y, type, NULL);
	}
	else if (tiletype == '|')
	{
		type = "WaterSandUpperRight";
		tmpTile = new BeachTile(x, y, type, NULL);
	}
	else if (tiletype == '^')
	{
		type = "WaterSandLowerLeft";
		tmpTile = new BeachTile(x, y, type, NULL);
	}
	else if (tiletype == '~')
	{
		type = "WaterSandLowerRight";
		tmpTile = new BeachTile(x, y, type, NULL);
	}
	else if (tiletype == '@')
	{
		type = "SandWaterUpperLeft";
		tmpTile = new BeachTile(x, y, type, NULL);
	}
	else if (tiletype == '�')
	{
		type = "SandWaterUpperRight";
		tmpTile = new BeachTile(x, y, type, NULL);
	}
	else if (tiletype == '�')
	{
		type = "SandWaterLowerLeft";
		tmpTile = new BeachTile(x, y, type, NULL);
	}
	else if (tiletype == '�')
	{
		type = "SandWaterLowerRight";
		tmpTile = new BeachTile(x, y, type, NULL);
	}
	else if (tiletype == '1')
	{
		type = "Sand";
		tmpTile = new SandTile(x, y, type, NULL);
	}
	else if (tiletype == '2')
	{
		tmpTile = new GrassTile(x, y, NULL);
	}
	else if (tiletype == '3')
	{
		tmpTile = new WaterTile(x, y, NULL);
	}

	if(tiletype != '\n')
	{
		return tmpTile;
	}

	return NULL;
}*/

vector <Map> & BattleMenuScreen::getMaps()
{
	return maps;
}

int BattleMenuScreen::getNumOfSelectedPlayers() const
{
	int rtrnval = 0;

	if(scenarioOptions[PLAYER_1] != NO_PLAYER)
		rtrnval++;

	if(scenarioOptions[PLAYER_2] != NO_PLAYER)
		rtrnval++;

	if(scenarioOptions[PLAYER_3] != NO_PLAYER)
		rtrnval++;

	if(scenarioOptions[PLAYER_4] != NO_PLAYER)
		rtrnval++;

	return rtrnval;
}

bool BattleMenuScreen::atLeastOneHuman() const
{
	bool rtrnval = false;

	if(scenarioOptions[PLAYER_1] == PLAYER || scenarioOptions[PLAYER_2] == PLAYER || scenarioOptions[PLAYER_3] == PLAYER || scenarioOptions[PLAYER_4] == PLAYER)
		rtrnval = true;

	return rtrnval;
}

Map* BattleMenuScreen::getChosenMap()
{
	int index = scenarioOptions[CHOSEN_SCENARIO];

	if(index >= 0 && (unsigned)index < maps.size())
		return &maps[index];
	else
		return NULL;
}

int BattleMenuScreen::getChosenMapIndex() const
{
	return scenarioOptions[CHOSEN_SCENARIO];
}

int BattleMenuScreen::getStartingRes() const
{
	return scenarioOptions[STARTING_RES];
}

int BattleMenuScreen::getPlayer1() const
{
	return scenarioOptions[PLAYER_1];
}

int BattleMenuScreen::getPlayer2() const
{
	return scenarioOptions[PLAYER_2];
}

int BattleMenuScreen::getPlayer3() const
{
	return scenarioOptions[PLAYER_3];
}

int BattleMenuScreen::getPlayer4() const
{
	return scenarioOptions[PLAYER_4];
}

void BattleMenuScreen::setChosenMapIndex(int newVal)
{
	scenarioOptions[CHOSEN_SCENARIO] = newVal;
}

void BattleMenuScreen::setStartingRes(int newVal)
{
	scenarioOptions[STARTING_RES] = newVal;
}

void BattleMenuScreen::setPlayer1(int newVal)
{
	scenarioOptions[PLAYER_1] = newVal;
}

void BattleMenuScreen::setPlayer2(int newVal)
{
	scenarioOptions[PLAYER_2] = newVal;
}

void BattleMenuScreen::setPlayer3(int newVal)
{
	scenarioOptions[PLAYER_3] = newVal;
}

void BattleMenuScreen::setPlayer4(int newVal)
{
	scenarioOptions[PLAYER_4] = newVal;
}