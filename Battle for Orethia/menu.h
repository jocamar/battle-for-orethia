#ifndef MENU_H
#define MENU_H

#include "Option.h"
#include "allegro5/allegro5.h"
#include "allegro5/allegro_font.h"
#include <vector>
#include <string>

using namespace std;

#define DIR_UP 1
#define DIR_DOWN 2

class Menu
{
protected:
	bool active;
	bool draw;

	vector <Option *> options;
	vector <ALLEGRO_FONT *> fonts;

	int selectedOption;
	int defaultSelectedOption;

public:
	Menu();
	Menu(bool active, bool draw, int defaultSelectedOption);

	virtual void update(bool* keys) = 0;
	virtual void render() const = 0;

	bool atLeastOneActive() const;
	int getNextActive(int direction) const;
	void setActive(bool newActive, bool resetSelectedOption = true);
	void setDrawing(bool newDraw);
	bool isActive() const;
	bool isDrawing() const;

	virtual ~Menu();
};



#endif