#include "MainMenu.h"
#include "game.h"
#include "MainMenuScreen.h"


MainMenu::MainMenu(MainMenuScreen* screen, ALLEGRO_FONT* unselected, ALLEGRO_FONT* selected) : Menu(true, true, MainMenu::MainMenuDefaultSelectedOption)
{
	this->screen = screen;

	exitMenu = new ExitMenu(screen, this, unselected, selected);

	options.push_back(new Option(MAIN_MENU_OPTION1, false));
	options.push_back(new Option(MAIN_MENU_OPTION2, true));
	options.push_back(new Option(MAIN_MENU_OPTION3, true));

	fonts.push_back(unselected);
	fonts.push_back(selected);
}

void MainMenu::update(bool* keys) //main menu specific update method
{
	exitMenu->update(keys);

	if(active)
	{
		if(keys[DOWN])
		{
			selectedOption = getNextActive(DIR_DOWN);

			keys[DOWN] = false;
		}
		else if(keys[UP])
		{
			selectedOption = getNextActive(DIR_UP);

			keys[UP] = false;
		}
		else if(keys[A] || keys[ENTER])
		{
			if(selectedOption == CAMPAIGN)
			{
				screen->switchToCampaign();
			}
			else if(selectedOption == BATTLE)
			{
				screen->switchToBattle();
			}
			else if(selectedOption == EXIT)
			{
				keys[A] = false;
				keys[B] = false;
				keys[ESC] = false;
				keys[ENTER] = false;
				keys[UP] = false;
				keys[DOWN] = false;
				keys[RIGHT] = false;
				keys[LEFT] = false;

				this->active = false;
				this->draw = false;
				exitMenu->setActive(true);
				exitMenu->setDrawing(true);
			}
		}
	}
}

void MainMenu::render() const
{
	if(draw)
	{
		for(unsigned int i = 0; i < options.size(); i++)
		{
			if(i == selectedOption)
			{
				if(options[i]->active)
					al_draw_text(fonts[SELECTED], MAIN_MENU_TEXT_COLOR, TEXT_X, TEXT_INIT_SELECTED_Y + i*TEXT_SELECTED_SPACING, ALLEGRO_ALIGN_CENTRE, options[i]->Name.c_str());
				else
					al_draw_text(fonts[SELECTED], MAIN_MENU_BLOCKED_COLOR, TEXT_X, TEXT_INIT_SELECTED_Y + i*TEXT_SELECTED_SPACING, ALLEGRO_ALIGN_CENTRE, options[i]->Name.c_str());
			}
			else
			{
				if(options[i]->active)
					al_draw_text(fonts[UNSELECTED], MAIN_MENU_TEXT_COLOR, TEXT_X, TEXT_INIT_UNSELECTED_Y + i*TEXT_UNSELECTED_SPACING, ALLEGRO_ALIGN_CENTRE, options[i]->Name.c_str());
				else
					al_draw_text(fonts[UNSELECTED], MAIN_MENU_BLOCKED_COLOR, TEXT_X, TEXT_INIT_UNSELECTED_Y + i*TEXT_UNSELECTED_SPACING, ALLEGRO_ALIGN_CENTRE, options[i]->Name.c_str());
			}
		}
	}

	exitMenu->render();
}

MainMenu::~MainMenu()
{
	delete exitMenu;
}