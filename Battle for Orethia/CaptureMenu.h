#ifndef CAPTUREMENU_H
#define CAPTUREMENU_H

#include "allegro5/allegro5.h"
#include "allegro5/allegro_font.h"

#include "menu.h"
#include "Sprite.h"

#define CAPTURE_MENU_BACK_COLOR					al_map_rgba(0,0,0,220)  //transparent black
#define CAPTURE_FRAME_NUM 3
#define SALUTE_FRAME_NUM 4
#define FLAG_FRAME_NUM 3
#define FLAG_ANIMATION_DELAY 10
#define CAPTURE_ANIMATION_DELAY 5
#define SALUTE_ANIMATION_DELAY 20
#define FLAG_POLE_PIXEL_NUM 184
#define FLAG_ASCEND_SPEED 1
#define SPRITE_Y 20

class Scenario;
class Tile;

class CaptureMenu : public Menu
{
	static int const CaptureMenuDefaultSelectedOption = 0;

	enum dimensions{MENUWIDTH = 200, MENUHEIGHT = 256, FLAGWIDTH = 31, FLAGHEIGHT = 29, FLAG_STARTING_X = 146, FLAG_STARTING_Y = 201};
	enum State{RAISING_FLAG, SALUTING};
	enum CapAnimations{RED_RAISING, RED_SALUTING, BLUE_RAISING, BLUE_SALUTING, GREEN_RAISING, GREEN_SALUTING, YELLOW_RAISING, YELLOW_SALUTING};
	enum FlagAnimations{RED, BLUE, GREEN, YELLOW};

	Scenario* scenario;
	Tile* tile;
	int finalFlagPosition, currFlagPosition;
	int state;
	Sprite captureAnimation, flagAnimation;
	ALLEGRO_BITMAP* background;
	int x, y;
	bool finished;

public:

	CaptureMenu(Scenario* scenario, Tile* tile, ALLEGRO_BITMAP* captureSprites, ALLEGRO_BITMAP* flag_sprites, ALLEGRO_BITMAP* background);

	void update(bool* keys);
	void render() const;

	~CaptureMenu();
};

#endif