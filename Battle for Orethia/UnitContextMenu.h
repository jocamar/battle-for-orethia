#ifndef UNITCONTEXTMENU_H
#define UNITCONTEXTMENU_H

#include "menu.h"
#include "allegro5/allegro5.h"
#include "allegro5/allegro_font.h"

#define UNIT_CONTEXT_MENU_TEXT_COLOR					al_map_rgb(255,255,255) //white
#define UNIT_CONTEXT_MENU_BLOCKED_COLOR					al_map_rgb(150,150,150)  //grey
#define UNIT_CONTEXT_MENU_BACK_COLOR					al_map_rgba(0,0,0,220)  //transparent black

#define UNIT_CONTEXT_MENU_CAPTURE						"Capture"
#define UNIT_CONTEXT_MENU_WAIT							"Wait"
#define UNIT_CONTEXT_MENU_MELEE_ATTACK					"Melee Attack"
#define UNIT_CONTEXT_MENU_RANGED_ATTACK					"Ranged Attack"

class Scenario;
class Unit;
class Tile;

class UnitContextMenu : public Menu
{
	static int const UnitContextMenuDefaultSelectedOption = 0;

	enum Fonts {UNSELECTED, SELECTED};
	enum textCoords {TEXT_UNSELECTED_SPACING = 100, TEXT_SELECTED_SPACING = 100, TEXT_HEIGHT = 100, TEXT_WIDTH = 300, TEXT_X_SPACING = 150, TEXT_Y_SPACING = 20};
	enum OptionNames {WAIT,CAPTURE,MELEE,RANGE};

	Scenario* scenario;
	Tile* unitTile;
	vector <Unit* > meleeAttackOptions;
	vector <Unit* > rangedAttackOptions;
	int numOptions, x, y;

	int getNumActiveOptions() const;
public:

	UnitContextMenu(Scenario* scenario, Tile* tile, ALLEGRO_FONT* unselected, ALLEGRO_FONT* selected);

	void update(bool* keys);
	void render() const;

	~UnitContextMenu();
};



#endif