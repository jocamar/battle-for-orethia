#include "TileInfoElement.h"
#include "scenario.h"
#include "game.h"

TileInfoElement::TileInfoElement(Scenario* scenario, ALLEGRO_BITMAP* backgroundImage, ALLEGRO_BITMAP* icons, ALLEGRO_FONT* font, ALLEGRO_FONT* smallfont) : UI_element(scenario)
{
	this->font = font;
	this->smallfont = smallfont;
	this->tile = scenario->getSelectedTile();

	background = Sprite(backgroundImage,RIGHT_X,TILE_INFO_Y,1,TILE_INFO_WIDTH,TILE_INFO_HEIGHT,10,0,WITHOUT_UNIT);
	defenseIcon = Sprite(icons,RIGHT_X + (TILE_INFO_WIDTH / 2) + DEFENSE_ICON_OFFSET_X, TILE_INFO_Y + DEFENSE_ICON_OFFSET_Y, 1,DEFENSE_ICON_SIZE, DEFENSE_ICON_SIZE, 10, 0, DEFENSE_ICON*DEFENSE_ICON_SIZE);

	this->textTitle_x = RIGHT_X + TILE_INFO_WIDTH / 2 + TILE_INFO_WIDTH / 4;
	this->textTitle_y = TILE_INFO_Y + TEXT_TITLE_OFFSET_Y;
	this->text_defense_x = RIGHT_X + TILE_INFO_WIDTH / 2 + TEXT_DEFENSE_OFFSET_X;
	this->text_defense_y = TILE_INFO_Y + TEXT_DEFENSE_OFFSET_Y;
	this->textUnitName_x = RIGHT_X + TILE_INFO_WIDTH / 4;
	this->textUnitName_y = TILE_INFO_Y + TEXT_TITLE_OFFSET_Y;
	this->textAmmo_x = RIGHT_X + TEXT_AMMO_OFFSET_X;
	this->textAmmo_y = TILE_INFO_Y + TEXT_AMMO_OFFSET_Y;
	this->textHealth_x = RIGHT_X + TEXT_AMMO_OFFSET_X;
	this->textHealth_y = TILE_INFO_Y + TEXT_HEALTH_OFFSET_Y;

	toTheRight = true;
	hasUnit = false;
	state = STOPPED;
}

void TileInfoElement::update()
{
	tile = scenario->getSelectedTile();
	if(tile->getUnit() != NULL && !hasUnit && tile->isVisible())
	{
		hasUnit = true;

		if(!toTheRight)
		{
			background.setPos(background.getX() + (TILE_INFO_WIDTH/2),background.getY());
		}

		background.setAnimationStartY(WITH_UNIT*TILE_INFO_HEIGHT);
	}
	else if (tile->getUnit() == NULL && hasUnit)
	{
		hasUnit = false;

		if(!toTheRight)
		{
			background.setPos(background.getX() - (TILE_INFO_WIDTH/2),background.getY());
		}

		background.setAnimationStartY(WITHOUT_UNIT);
	}

	if(state == RAISING)
	{
		if(background.getY() >= HEIGHT)
		{
			if(toTheRight)
			{
				if(!hasUnit)
					background.setPos(LEFT_X - (TILE_INFO_WIDTH/2), background.getY());
				else
					background.setPos(LEFT_X,background.getY());

				defenseIcon.setPos(LEFT_X + DEFENSE_ICON_OFFSET_X, defenseIcon.getY());

				textTitle_x = LEFT_X + TILE_INFO_WIDTH / 4;
				text_defense_x = LEFT_X + TEXT_DEFENSE_OFFSET_X;
				textUnitName_x = LEFT_X + TILE_INFO_WIDTH/2 + TILE_INFO_WIDTH/4;
				textAmmo_x = LEFT_X + TILE_INFO_WIDTH/2 + TEXT_AMMO_OFFSET_X;
				textHealth_x = LEFT_X + TILE_INFO_WIDTH/2 + TEXT_AMMO_OFFSET_X;

				toTheRight = false;
			}
			else
			{
				background.setPos(RIGHT_X, background.getY());
				defenseIcon.setPos(RIGHT_X + (TILE_INFO_WIDTH / 2) + DEFENSE_ICON_OFFSET_X, defenseIcon.getY());

				textTitle_x = RIGHT_X + TILE_INFO_WIDTH / 2 + TILE_INFO_WIDTH / 4;
				text_defense_x = RIGHT_X + TILE_INFO_WIDTH / 2 + TEXT_DEFENSE_OFFSET_X;
				textUnitName_x = RIGHT_X + (TILE_INFO_WIDTH / 4);
				textAmmo_x = RIGHT_X + TEXT_AMMO_OFFSET_X;
				textHealth_x = RIGHT_X + TEXT_AMMO_OFFSET_X;

				toTheRight = true;
			}

			state = FALLING;
		}
		else
		{
			background.setPos(background.getX(),background.getY() + BANNER_SPEED);
			defenseIcon.setPos(defenseIcon.getX(), defenseIcon.getY() + BANNER_SPEED);
			textTitle_y += BANNER_SPEED;
			text_defense_y += BANNER_SPEED;
			textUnitName_y += BANNER_SPEED;
			textAmmo_y += BANNER_SPEED;
			textHealth_y += BANNER_SPEED;
		}
	}
	else if (state == FALLING)
	{
		if(background.getY() > TILE_INFO_Y)
		{
			background.setPos(background.getX(),background.getY() - BANNER_SPEED);
			defenseIcon.setPos(defenseIcon.getX(), defenseIcon.getY() - BANNER_SPEED);
			textTitle_y -= BANNER_SPEED;
			text_defense_y -= BANNER_SPEED;
			textUnitName_y -= BANNER_SPEED;
			textAmmo_y -= BANNER_SPEED;
			textHealth_y -= BANNER_SPEED;
		}	
		else
			state = STOPPED;
	}

	if(scenario->getActivePlayer()->getCursorX() < scenario->getActivePlayer()->getCamX() + SWITCH_THRESHOLD && !toTheRight)
	{
		state = RAISING;
	}
	else if (scenario->getActivePlayer()->getCursorX() >= scenario->getActivePlayer()->getCamX() + SWITCH_THRESHOLD && toTheRight)
	{
		state = RAISING;
	}

	if(toTheRight)
	{
		if(hasUnit)
		{
			tile->getUnit()->updateInfoSprite(background.getX() + TILE_SPRITE_OFFSET_X, background.getY() + TILE_SPRITE_OFFSET_Y);
		}

		tile->updateInfoSprite(background.getX() + (TILE_INFO_WIDTH/2) + TILE_SPRITE_OFFSET_X, background.getY() + TILE_SPRITE_OFFSET_Y);
	}
	else
	{
		if(!hasUnit)
			tile->updateInfoSprite(background.getX() + (TILE_INFO_WIDTH/2) + TILE_SPRITE_OFFSET_X, background.getY() + TILE_SPRITE_OFFSET_Y);
		else
		{
			tile->updateInfoSprite(background.getX() + TILE_SPRITE_OFFSET_X, background.getY() + TILE_SPRITE_OFFSET_Y);
			tile->getUnit()->updateInfoSprite(background.getX() + (TILE_INFO_WIDTH/2) + TILE_SPRITE_OFFSET_X, background.getY() + TILE_SPRITE_OFFSET_Y);
		}
	}

}

void TileInfoElement::render() const
{
	stringstream stream;
	int defense = (int)(tile->getDefense()*100);

	if(defense >= 0)
		stream << "+";
	
	stream << defense << "%";

	background.render();
	tile->renderInfoSprite(scenario->getActivePlayerNum());
	defenseIcon.render();

	al_draw_text(font,TILE_INFO_TEXT_COLOR,textTitle_x,textTitle_y,ALLEGRO_ALIGN_CENTRE,tile->getName().c_str());
	al_draw_text(smallfont,TILE_INFO_TEXT_COLOR,text_defense_x,text_defense_y,0,stream.str().c_str());

	if(hasUnit)
	{
		tile->getUnit()->renderInfoSprite();
		al_draw_text(font,TILE_INFO_TEXT_COLOR,textUnitName_x,textUnitName_y,ALLEGRO_ALIGN_CENTRE,tile->getUnit()->getName().c_str());

		stream.str(string());

		stream << "Ammo - " << tile->getUnit()->getAmmo();

		al_draw_text(smallfont,TILE_INFO_TEXT_COLOR,textAmmo_x,textAmmo_y,0,stream.str().c_str());

		stream.str(string());

		stream << "Health - " << tile->getUnit()->getConvertedHP() << "/" << tile->getUnit()->getMaxHp();

		al_draw_text(smallfont,TILE_INFO_TEXT_COLOR,textHealth_x,textHealth_y,0,stream.str().c_str());

	}
}