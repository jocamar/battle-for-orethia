#include "player.h"

Player::Player(bool human, int camX, int camY, unsigned int resources, bool active, bool defeated, char color, ALLEGRO_BITMAP* cursor)
{
	this->defeated = defeated;
	this->human = human;
	this->camX = camX;
	this->camY = camY;
	this->resources = resources;
	this->active = active;
	this->cursorX = 0;
	this->cursorY = 0;
	this->color = color;
	this->cursor = cursor;
}

Player::Player(vector <Unit*> units, bool human, int camX, int camY, unsigned int resources, bool active, bool defeated, char color, ALLEGRO_BITMAP* cursor)
{
	this->defeated = defeated;
	this->human = human;
	this->camX = camX;
	this->camY = camY;
	this->resources = resources;
	this->active = active;
	this->units = units;
	this->cursorX = 0;
	this->cursorY = 0;
	this->color = color;
	this->cursor = cursor;
}

int Player::getCamX() const
{
	return camX;
}

int Player::getCamY() const
{
	return camY;
}

vector <Unit*> Player::getUnits() const
{
 return units;
}

void Player::addUnit(Unit* newUnit)
{
 units.push_back(newUnit);
}

void Player::removeUnit(int index)
{
	delete units[index];
	units.erase(units.begin()+index);
}

unsigned int Player::getResources() const
{
	return resources;
}

bool Player::isHuman() const
{
	return human;
}

bool Player::isActive() const
{
	return active;
}

bool Player::isDefeated() const
{
	return defeated;
}

void Player::setActive(bool newValue)
{
	active = newValue;
}

void Player::setCamX(int newValue)
{
	camX = newValue;
}

void Player::setCamY(int newValue)
{
	camY = newValue;
}

void Player::setCursorImage(ALLEGRO_BITMAP* cursor)
{
	this->cursor = cursor;
}

void Player::addResources(int value)
{
	resources += value;
}

void Player::removeResources(int value)
{
	resources -= value;
}

void Player::update(Scenario* scenario)
{
	for(unsigned int i = 0; i < units.size(); i++)
	{
		units[i]->update(scenario);
	}
}
	
void Player::render() const
{
	if(active)
	{
		al_draw_bitmap(cursor,((cursorX-camX)*TileInfo::TILE_WIDTH)-CURSOR_SCALE,((cursorY-camY)*TileInfo::TILE_HEIGHT)-CURSOR_SCALE,0);
	}
}

Player::~Player()
{
	for(unsigned int i = 0; i < units.size(); i++)
	{
		delete units[i];
	}
}

int Player::getCursorX() const
{
	return cursorX;
}

int Player::getCursorY() const
{
	return cursorY;
}

void Player::moveCursor(char direction, int mapSizeX, int mapSizeY)
{
	if(direction == 'u')
	{
		if(cursorY > 0)
			cursorY--;

		if (cursorY < camY+2 && cursorY > 1)
			camY--;
	}
	else if(direction == 'd')
	{
		if(cursorY < mapSizeY-1)
			cursorY++;

		if(cursorY > camY + 6 && cursorY < mapSizeY-2)
			camY++;
	}
	else if(direction == 'l')
	{
		if(cursorX > 0)
			cursorX--;

		if (cursorX < camX+2 && cursorX > 1)
			camX--;
	}
	else if(direction == 'r')
	{
		if(cursorX < mapSizeX-1)
			cursorX++;

		if(cursorX > camX + 13 && cursorX < mapSizeX-2)
			camX++;
	}
}

char Player::getColor() const
{
	return color;
}

void Player::renderUnits() const
{
	int selected = -1;

	for(unsigned int i = 0; i < units.size(); i++)
	{
		if(units[i]->getTile()->isVisible() && units[i]->getState() != Unit::MOVING)
			units[i]->render();
		else if (units[i]->getState() ==  Unit::MOVING)
			selected = i;
	}

	if(selected >= 0 && (unsigned) selected < units.size())
		units[selected]->render();
}

void Player::restUnits()
{
	for(unsigned int i = 0; i < units.size(); i++)
	{
		units[i]->changeState(Unit::IDLE);
	}
}

void Player::setUnitsTint(ALLEGRO_COLOR tint)
{
	for(unsigned int i = 0; i < units.size(); i++)
	{
		units[i]->setTint(tint);
	}
}

void Player::animateUnits(int camX, int camY)
{
	for(unsigned int i = 0; i < units.size(); i++)
	{
		units[i]->updateSprite(camX, camY);
	}
}