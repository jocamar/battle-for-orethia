#include "UnitAttackMenu.h"
#include "game.h"
#include "scenario.h"
#include "CaptureMenu.h"
#include "ScenarioScreen.h"
#include "UnitContextMenu.h"

#define PI 3.1415926


UnitAttackMenu::UnitAttackMenu(Scenario* scenario, ALLEGRO_FONT* unselected, ALLEGRO_FONT* selected, ALLEGRO_BITMAP* cursor, vector<Unit*> targets, Tile* unitTile, int attackType) : Menu(false, false, UnitAttackMenu::UnitAttackMenuDefaultSelectedOption)
{
	this->scenario = scenario;
	this->attackOptions = targets;
	this->cursorAngle = 0;
	this->cursor = cursor;
	this->attackType = attackType;
	this->state = MOVING_TO_TARGET;
	this->moveCounter = 0;
	this->unitTile = unitTile;

	if(attackOptions.size() > 0)
		selectedTarget = 0;

	for(unsigned int i = 0; i < attackOptions.size(); i++)
	{
		scenario->getMap()->highlightRangeNode(attackOptions[i]->getX(),attackOptions[i]->getY());
	}

	this->cursor_x = attackOptions[selectedTarget]->getX();
	this->cursor_y = attackOptions[selectedTarget]->getY();

	fonts.push_back(unselected);
	fonts.push_back(selected);
}

void UnitAttackMenu::update(bool* keys)
{
	if(active)
	{
		this->cursorAngle += ROTATION_SPEED;

		if(cursorAngle >= 360)
			cursorAngle -= 360;

		if(state == AT_TARGET)
		{
			scenario->updateTileInfo();
			Unit* newTarget;

			if (keys[B])
			{
				this->state = EXITING;
				this->draw = false;
				scenario->getMap()->resetRangeNodes();

				keys[B] = false;
			}
			else 
			{
				if(keys[UP])
				{
					keys[UP] = false;
					selectedTarget = getNextUnit('u', selectedTarget);

					newTarget = attackOptions[selectedTarget];

					cursor_x = newTarget->getX();
					cursor_y = newTarget->getY();

					this->state = MOVING_TO_TARGET;
				}
				else if (keys[DOWN])
				{
					keys[DOWN] = false;
					selectedTarget = getNextUnit('d', selectedTarget);

					newTarget = attackOptions[selectedTarget];

					cursor_x = newTarget->getX();
					cursor_y = newTarget->getY();

					this->state = MOVING_TO_TARGET;
				}
				else if (keys[LEFT])
				{
					keys[LEFT] = false;
					selectedTarget = getNextUnit('l', selectedTarget);

					newTarget = attackOptions[selectedTarget];

					cursor_x = newTarget->getX();
					cursor_y = newTarget->getY();

					this->state = MOVING_TO_TARGET;
				}
				else if (keys[RIGHT])
				{
					keys[RIGHT] = false;
					selectedTarget = getNextUnit('r', selectedTarget);

					newTarget = attackOptions[selectedTarget];

					cursor_x = newTarget->getX();
					cursor_y = newTarget->getY();

					this->state = MOVING_TO_TARGET;
				}
			}
		}
		else if (state == MOVING_TO_TARGET)
		{
			if(moveCounter >= MOVE_DELAY)
			{
				if(cursor_x > scenario->getActivePlayer()->getCursorX())
				{
					scenario->getActivePlayer()->moveCursor('r',scenario->getMap()->getSizeX(),scenario->getMap()->getSizeY());
				}
				else if(cursor_x < scenario->getActivePlayer()->getCursorX())
				{
					scenario->getActivePlayer()->moveCursor('l',scenario->getMap()->getSizeX(),scenario->getMap()->getSizeY());
				}
				else if(cursor_y > scenario->getActivePlayer()->getCursorY())
				{
					scenario->getActivePlayer()->moveCursor('d',scenario->getMap()->getSizeX(),scenario->getMap()->getSizeY());
				}
				else if(cursor_y < scenario->getActivePlayer()->getCursorY())
				{
					scenario->getActivePlayer()->moveCursor('u',scenario->getMap()->getSizeX(),scenario->getMap()->getSizeY());
				}

				moveCounter = 0;
				if(cursor_x == scenario->getActivePlayer()->getCursorX() && cursor_y == scenario->getActivePlayer()->getCursorY())
				{
					this->state = AT_TARGET;
				}
			}
			else
				moveCounter++;
		}
		else
		{
			if(moveCounter >= MOVE_DELAY)
			{
				if(unitTile->getX() > scenario->getActivePlayer()->getCursorX())
				{
					scenario->getActivePlayer()->moveCursor('r',scenario->getMap()->getSizeX(),scenario->getMap()->getSizeY());
				}
				else if(unitTile->getX() < scenario->getActivePlayer()->getCursorX())
				{
					scenario->getActivePlayer()->moveCursor('l',scenario->getMap()->getSizeX(),scenario->getMap()->getSizeY());
				}
				else if(unitTile->getY() > scenario->getActivePlayer()->getCursorY())
				{
					scenario->getActivePlayer()->moveCursor('d',scenario->getMap()->getSizeX(),scenario->getMap()->getSizeY());
				}
				else if(unitTile->getY() < scenario->getActivePlayer()->getCursorY())
				{
					scenario->getActivePlayer()->moveCursor('u',scenario->getMap()->getSizeX(),scenario->getMap()->getSizeY());
				}

				moveCounter = 0;
				if(unitTile->getX() == scenario->getActivePlayer()->getCursorX() && unitTile->getY() == scenario->getActivePlayer()->getCursorY())
				{
					scenario->activateMenu(new UnitContextMenu(scenario,unitTile,scenario->getScreen()->getFontMedium(),scenario->getScreen()->getFontLarge()));
				}
			}
			else
				moveCounter++;
		}
	}
}

int UnitAttackMenu::getNextUnit(char direction, int currUnit) const
{
	int targetUnit = -1;

	if(direction == 'u')
	{
		for(unsigned int i = 0; i < attackOptions.size(); i++)
		{
			if(attackOptions[i]->getX() == cursor_x && attackOptions[i]->getY() < cursor_y)
			{
				if(targetUnit == -1)
					targetUnit = i;
				else if(attackOptions[targetUnit]->getY() < attackOptions[i]->getY())
						targetUnit = i;
			}
		}

		if(targetUnit == -1)
		{
			for(unsigned int i = 0; i < attackOptions.size(); i++)
			{
				if(attackOptions[i]->getY() < cursor_y)
				{
					if(targetUnit == -1)
						targetUnit = i;
					else if(attackOptions[targetUnit]->getY() < attackOptions[i]->getY())
						targetUnit = i;
				}
			}
		}
	}
	else if (direction == 'd')
	{
		for(unsigned int i = 0; i < attackOptions.size(); i++)
		{
			if(attackOptions[i]->getX() == cursor_x && attackOptions[i]->getY() > cursor_y)
			{
				if(targetUnit == -1)
					targetUnit = i;
				else if(attackOptions[targetUnit]->getY() > attackOptions[i]->getY())
						targetUnit = i;
			}
		}

		if(targetUnit == -1)
		{
			for(unsigned int i = 0; i < attackOptions.size(); i++)
			{
				if(attackOptions[i]->getY() > cursor_y)
				{
					if(targetUnit == -1)
						targetUnit = i;
					else if(attackOptions[targetUnit]->getY() > attackOptions[i]->getY())
						targetUnit = i;
				}
			}
		}
	}
	else if(direction == 'l')
	{
		for(unsigned int i = 0; i < attackOptions.size(); i++)
		{
			if(attackOptions[i]->getY() == cursor_y && attackOptions[i]->getX() < cursor_x)
			{
				if(targetUnit == -1)
					targetUnit = i;
				else if(attackOptions[targetUnit]->getX() < attackOptions[i]->getX())
						targetUnit = i;
			}
		}

		if(targetUnit == -1)
		{
			for(unsigned int i = 0; i < attackOptions.size(); i++)
			{
				if(attackOptions[i]->getX() < cursor_x)
				{
					if(targetUnit == -1)
						targetUnit = i;
					else if(attackOptions[targetUnit]->getX() < attackOptions[i]->getX())
						targetUnit = i;
				}
			}
		}
	}
	else if (direction == 'r')
	{
		for(unsigned int i = 0; i < attackOptions.size(); i++)
		{
			if(attackOptions[i]->getY() == cursor_y && attackOptions[i]->getX() > cursor_x)
			{
				if(targetUnit == -1)
					targetUnit = i;
				else if(attackOptions[targetUnit]->getX() > attackOptions[i]->getX())
						targetUnit = i;
			}
		}

		if(targetUnit == -1)
		{
			for(unsigned int i = 0; i < attackOptions.size(); i++)
			{
				if(attackOptions[i]->getX() > cursor_x)
				{
					if(targetUnit == -1)
						targetUnit = i;
					else if(attackOptions[targetUnit]->getX() > attackOptions[i]->getX())
						targetUnit = i;
				}
			}
		}
	}

	if(targetUnit == -1)
	{
		targetUnit = currUnit;

		targetUnit++;

		if(targetUnit >= attackOptions.size())
			targetUnit = 0;
	}

	return targetUnit;
}

void UnitAttackMenu::render() const
{
	if(draw)
	{
		float angleRadians = cursorAngle * (PI/180);
		int cam_x = scenario->getActivePlayer()->getCamX();
		int cam_y = scenario->getActivePlayer()->getCamY();

		int actualX = (cursor_x - cam_x) * TileInfo::TILE_WIDTH;
		int actualY = (cursor_y - cam_y) * TileInfo::TILE_HEIGHT;

		al_draw_rotated_bitmap(cursor,CURSOR_SIZE/2,CURSOR_SIZE/2,actualX-CURSOR_OFFSET+CURSOR_SIZE/2,actualY-CURSOR_OFFSET+CURSOR_SIZE/2,angleRadians,0);

		scenario->renderBanner();
		scenario->renderTileInfo();
	}
}

UnitAttackMenu::~UnitAttackMenu()
{
}
