#include "scenario.h"
#include "game.h"
#include "ScenarioScreen.h"
#include "ScenarioPauseMenu.h"
#include "UnitContextMenu.h"
#include "TrapMenu.h"

Scenario::Scenario(int numRemainingPlayers, vector <Player> players, Map map)
{
	this->numRemainingPlayers = numRemainingPlayers;
	this->turnNum = TURN_NUM;
	this->activePlayer = ACTIVE_PLAYER;
	this->players = players;
	this->players[activePlayer].setActive(true);
	this->map = map;
	//map.print();
	this->scenarioState = STARTING_STATE;
	this->keyHoldCounter = 0;
	this->tileMoveDelayCounter = 0;
	this->scrolling = false;

	this->screen = NULL;
	this->currMenu = NULL;
	this->selectedUnit = NULL;
	
	map.initializeTiles(players);

	for(unsigned int i = 0; i < NUM_KEYS; i++)
		keysHeld[i] = false;

	this->map.print();
}

bool Scenario::getNumRemainingPlayers() const
{
	return numRemainingPlayers;
}

void Scenario::update(bool* keys)
{
	if(scenarioState == PRETURN)
	{
		updatePreturn(keys);
	}
	else if (scenarioState == IDLE)
	{
		updateIdle(keys);
	}
	else if (scenarioState == IN_MENU)
	{
		updateInMenu(keys);
	}
	else if (scenarioState == SELECTING_UNIT)
	{
		updateSelectingUnit(keys);
	}
	else if (scenarioState == UNIT_MOVING)
	{
		updateUnitMoving(keys);
	}
	else if(scenarioState == CHECKING_MAP)
	{
		updateCheckingMap(keys);
	}
	else if(scenarioState == CHECKING_UNIT_RANGE)
	{
		updateCheckingRange(keys);
	}
	else if(scenarioState == CHECKING_UNIT_MOVEMENT)
	{
		updateCheckingMovement(keys);
	}

	for(unsigned int i = 0; i < NUM_KEYS; i++)
		keysHeld[i] = keys[i];
}

void Scenario::render(ALLEGRO_FONT* font) const
{
	if(scenarioState == PRETURN)
	{
		renderPreturn(font);
	}
	else if (scenarioState == IDLE)
	{
		renderIdle();
	}
	else if (scenarioState == SELECTING_UNIT)
	{
		renderIdle();
	}
	else if (scenarioState == IN_MENU)
	{
		renderInMenu();
	}
	else if (scenarioState == UNIT_MOVING)
	{
		renderIdle();
	}
	else if(scenarioState == CHECKING_MAP)
	{
		renderCheckingMap();
	}
	else if(scenarioState == CHECKING_UNIT_RANGE)
	{
		renderIdle();
	}
	else if(scenarioState == CHECKING_UNIT_MOVEMENT)
	{
		renderIdle();
	}
}

int Scenario::getTurnNum() const
{
	return turnNum;
}

int Scenario::getActivePlayerNum() const
{
	return activePlayer;
}

Player* Scenario::getActivePlayer()
{
	return &players[activePlayer];
}

void Scenario::passTurn()
{
	int newActive;

	do
	{
		newActive = activePlayer+1;

		if(newActive >= numRemainingPlayers)
		{
			turnNum++;

			newActive = 0;
		}
	} while (players[newActive].isDefeated());

	setActivePlayer(newActive);
	map.setVisibilityChanged(true);

	if(turnNum >= 2)
		map.gainResources(&players[activePlayer]);

	for(unsigned int i = 0; i < players.size(); i++)
	{
		players[i].restUnits();
		players[i].update(this);
	}
}

void Scenario::setActivePlayer(int newValue)
{
	players[activePlayer].setActive(false);
	activePlayer = newValue;
	players[activePlayer].setActive(true);
}

Scenario::~Scenario()
{
	delete currMenu;
}

void Scenario::updateMapImages(vector<ALLEGRO_BITMAP*> tiles, ALLEGRO_BITMAP* fog)
{
	map.updateMapImages(tiles, fog);
}

void Scenario::updatePlayerImages(ALLEGRO_BITMAP* cursor)
{
	for(unsigned int i = 0; i < players.size(); i++)
	{
		players[i].setCursorImage(cursor);
	}
}

void Scenario::changeState(int newState)
{
	scenarioState = newState;
}

void Scenario::setScreen(ScenarioScreen* screen)
{
	this->screen = screen;
}

void Scenario::activateMenu(Menu* newMenu)
{
	delete currMenu;
	currMenu = newMenu;

	currMenu->setActive(true);
	currMenu->setDrawing(true);
}

void Scenario::buyUnit(Unit* newUnit, Tile* tile)
{
	players[activePlayer].addUnit(newUnit);
	tile->occupy(newUnit);
	players[activePlayer].removeResources(newUnit->getCost());
	map.setVisibilityChanged(true);
}

int Scenario::getState() const
{
	return scenarioState;
}

void Scenario::updatePreturn(bool* keys)
{
	map.update(&players[activePlayer], activePlayer);

	if(keys[A] || keys[B] || keys[ESC] || keys[ENTER]) // key A, key B, key Esc, key Enter
	{
		keys[A] = false;
		keys[B] = false;
		keys[ESC] = false;
		keys[ENTER] = false;
		keys[UP] = false;
		keys[DOWN] = false;
		keys[RIGHT] = false;
		keys[LEFT] = false;

		changeState(IDLE);
	}
}

void Scenario::updateCursor(bool* keys, Player* player)
{
	if(keys[UP] && keysHeld[UP]) //UP
	{
		if(scrolling)
		{
			if(tileMoveDelayCounter > DELAY_TIME)
			{
				player->moveCursor('u',map.getSizeX(), map.getSizeY());
				tileMoveDelayCounter = 0;
			}
			else
				tileMoveDelayCounter++;
		}
		else if (keyHoldCounter > HOLD_TIME)
		{
			scrolling = true;
			keyHoldCounter = 0;
			player->moveCursor('u',map.getSizeX(), map.getSizeY());
			tileMoveDelayCounter = 0;
		}
		else
			keyHoldCounter++;
	}
	else if (!keys[UP] && keysHeld[UP])
	{
		if(scrolling)
		{
			scrolling = false;
			tileMoveDelayCounter = 0;
			keyHoldCounter = 0;
		}
		else
		{
			player->moveCursor('u',map.getSizeX(), map.getSizeY());
			keyHoldCounter = 0;
		}
	}
	else if(keys[DOWN] && keysHeld[DOWN]) //DOWN
	{
		if(scrolling)
		{
			if(tileMoveDelayCounter > DELAY_TIME)
			{
				player->moveCursor('d',map.getSizeX(), map.getSizeY());
				tileMoveDelayCounter = 0;
			}
			else
				tileMoveDelayCounter++;
		}
		else if (keyHoldCounter > HOLD_TIME)
		{
			scrolling = true;
			keyHoldCounter = 0;
			player->moveCursor('d',map.getSizeX(), map.getSizeY());
			tileMoveDelayCounter = 0;
		}
		else
			keyHoldCounter++;
	}
	else if (!keys[DOWN] && keysHeld[DOWN])
	{
		if(scrolling)
		{
			scrolling = false;
			tileMoveDelayCounter = 0;
			keyHoldCounter = 0;
		}
		else
		{
			player->moveCursor('d',map.getSizeX(), map.getSizeY());
			keyHoldCounter = 0;
		}
	}
	if(keys[LEFT] && keysHeld[LEFT]) //LEFT
	{
		if(scrolling)
		{
			if(tileMoveDelayCounter > DELAY_TIME)
			{
				player->moveCursor('l',map.getSizeX(), map.getSizeY());
				tileMoveDelayCounter = 0;
			}
			else
				tileMoveDelayCounter++;
		}
		else if (keyHoldCounter > HOLD_TIME)
		{
			scrolling = true;
			keyHoldCounter = 0;
			player->moveCursor('l',map.getSizeX(), map.getSizeY());
			tileMoveDelayCounter = 0;
		}
		else
			keyHoldCounter++;
	}
	else if (!keys[LEFT] && keysHeld[LEFT])
	{
		if(scrolling)
		{
			scrolling = false;
			tileMoveDelayCounter = 0;
			keyHoldCounter = 0;
		}
		else
		{
			player->moveCursor('l',map.getSizeX(), map.getSizeY());
			keyHoldCounter = 0;
		}
	}
	if(keys[RIGHT] && keysHeld[RIGHT]) //RIGHT
	{
		if(scrolling)
		{
			if(tileMoveDelayCounter > DELAY_TIME)
			{
				player->moveCursor('r',map.getSizeX(), map.getSizeY());
				tileMoveDelayCounter = 0;
			}
			else
				tileMoveDelayCounter++;
		}
		else if (keyHoldCounter > HOLD_TIME)
		{
			scrolling = true;
			keyHoldCounter = 0;
			player->moveCursor('r',map.getSizeX(), map.getSizeY());
			tileMoveDelayCounter = 0;
		}
		else
			keyHoldCounter++;
	}
	else if (!keys[RIGHT] && keysHeld[RIGHT])
	{
		if(scrolling)
		{
			scrolling = false;
			tileMoveDelayCounter = 0;
			keyHoldCounter = 0;
		}
		else
		{
			player->moveCursor('r',map.getSizeX(), map.getSizeY());
			keyHoldCounter = 0;
		}
	}
}

void Scenario::updateTileInfo()
{
	tileInfoBanner.update();
}

void Scenario::updateIdle(bool* keys)
{
	for(unsigned int i = 0; i < players.size(); i++)
	{
		if(players[i].isActive() && players[i].isHuman())
		{
			updateCursor(keys, &players[i]);
			
			if(keys[A]) //A
			{
				int cursorX = players[i].getCursorX();
				int cursorY = players[i].getCursorY();

				keys[A] = false;

				Tile* selectedTile = map.getTile(cursorX,cursorY);

				selectedTile->actionA(this,screen, keys);
			}
			else if(keys[B]) //B
			{
				int cursorX = players[i].getCursorX();
				int cursorY = players[i].getCursorY();

				Tile* selectedTile = map.getTile(cursorX,cursorY);

				selectedTile->actionB(this,screen, keys);
			}
			else if(keys[ESC]) //ESC
			{
				keys[A] = false;
				keys[B] = false;
				keys[ESC] = false;
				keys[ENTER] = false;
				keys[UP] = false;
				keys[DOWN] = false;
				keys[RIGHT] = false;
				keys[LEFT] = false;

				activateMenu(new ScenarioPauseMenu(screen, this, screen->getFontMedium(), screen->getFontLarge()));
				changeState(IN_MENU);
			}
			else if(keys[ENTER]) //ENTER
			{
			}
		}

		players[i].update(this);
	}

	currBanner.update();
	tileInfoBanner.update();
	map.update(&players[activePlayer], activePlayer);
}

void Scenario::setUnitsTint(ALLEGRO_COLOR tint)
{
	for(unsigned int j = 0; j < players.size(); j++)
	{
		players[j].setUnitsTint(tint);
	}
}

void Scenario::updateInMenu(bool* keys)
{
	for(unsigned int i = 0; i < players.size(); i++)
	{
		players[i].animateUnits(players[activePlayer].getCamX(),players[activePlayer].getCamY());
	}
	
	currBanner.update();
	map.update(&players[activePlayer], activePlayer);
	
	currMenu->update(keys);
}

void Scenario::updateCheckingMovement(bool* keys)
{
	currBanner.update();
	tileInfoBanner.update();
	map.update(&players[activePlayer], activePlayer);

	for(unsigned int i = 0; i < players.size(); i++)
	{
		players[i].update(this);
	}

	updateCursor(keys, &players[activePlayer]);

	Tile* cursorTile = map.getTile(players[activePlayer].getCursorX(),players[activePlayer].getCursorY());

	if(cursorTile->isHW())
		selectedUnit->addTileToPath(cursorTile,map.getShortestPath(cursorTile));

	if(!keys[A])
	{
		unselectUnit();
		changeState(IDLE);
	}
}

void Scenario::renderPreturn(ALLEGRO_FONT* font) const
{
	char buffer [33];
	int playerNum = activePlayer+1;

	al_draw_filled_rectangle(0,0,1280,720,al_map_rgb(0,0,0));

	_itoa(turnNum,buffer,10);

	al_draw_text(font,al_map_rgb(255,255,255),630,300,ALLEGRO_ALIGN_CENTRE,"Day");
	al_draw_text(font,al_map_rgb(255,255,255),670,300,ALLEGRO_ALIGN_CENTRE,buffer);

	if(players[activePlayer].getColor() == 'r')
		al_draw_filled_rectangle(630,365,650,385,al_map_rgb(255,0,0));
	else if(players[activePlayer].getColor() == 'b')
		al_draw_filled_rectangle(630,365,650,385,al_map_rgb(0,0,255));
	else if (players[activePlayer].getColor() == 'g')
		al_draw_filled_rectangle(630,365,650,385,al_map_rgb(0,255,0));
	else if (players[activePlayer].getColor() == 'y')
		al_draw_filled_rectangle(630,365,650,385,al_map_rgb(255,255,0));

	_itoa(playerNum,buffer,10);

	al_draw_text(font,al_map_rgb(255,255,255),630,400,ALLEGRO_ALIGN_CENTRE,"Player");
	al_draw_text(font,al_map_rgb(255,255,255),690,400,ALLEGRO_ALIGN_CENTRE,buffer);
}

void Scenario::renderIdle() const
{
	map.render(players[activePlayer].getCamX(), players[activePlayer].getCamY(), activePlayer);

	for(unsigned int i = 0; i < players.size(); i++)
	{
		if(i != activePlayer)
			players[i].renderUnits();
	}

	players[activePlayer].renderUnits();

	if(selectedUnit != NULL && selectedUnit->getState() == Unit::IDLE)
			selectedUnit->renderPath(screen->getImage(ScenarioScreen::ARROWS),players[activePlayer].getCamX(), players[activePlayer].getCamY());
	players[activePlayer].render();
	currBanner.render();
	tileInfoBanner.render();
}

void Scenario::renderInMenu() const
{
	map.render(players[activePlayer].getCamX(), players[activePlayer].getCamY(), activePlayer);
	
	for(unsigned int i = 0; i < players.size(); i++)
	{
		if(i != activePlayer)
			players[i].renderUnits();
	}

	players[activePlayer].renderUnits();

	currMenu->render();
}

void Scenario::renderBanner() const
{
	currBanner.render();
}

void Scenario::renderTileInfo() const
{
	tileInfoBanner.render();
}

void Scenario::renderCheckingMap() const
{
	map.render(players[activePlayer].getCamX(), players[activePlayer].getCamY(), activePlayer);

	for(unsigned int i = 0; i < players.size(); i++)
	{
		if(i != activePlayer)
			players[i].renderUnits();
	}

	players[activePlayer].renderUnits();

	if(selectedUnit != NULL && selectedUnit->getState() == Unit::IDLE)
			selectedUnit->renderPath(screen->getImage(ScenarioScreen::ARROWS),players[activePlayer].getCamX(), players[activePlayer].getCamY());
	players[activePlayer].render();
}

void Scenario::updateSelectingUnit(bool* keys)
{
	for(unsigned int i = 0; i < players.size(); i++)
	{
		if(players[i].isActive() && players[i].isHuman())
		{
			updateCursor(keys, &players[i]);

			Tile* cursorTile = map.getTile(players[i].getCursorX(),players[i].getCursorY());

			if(cursorTile->isHW())
				selectedUnit->addTileToPath(cursorTile,map.getShortestPath(cursorTile));

			if(keys[A]) //A
			{
				int cursorX = players[i].getCursorX();
				int cursorY = players[i].getCursorY();

				Tile* selectedTile = map.getTile(cursorX,cursorY);

				keys[A] = false;
				keys[B] = false;
				keys[ESC] = false;
				keys[ENTER] = false;
				keys[UP] = false;
				keys[DOWN] = false;
				keys[RIGHT] = false;
				keys[LEFT] = false;

				selectedTile->actionA(this,screen, keys);
			}
			else if(keys[B]) //B
			{
				keys[A] = false;
				keys[B] = false;
				keys[ESC] = false;
				keys[ENTER] = false;
				keys[UP] = false;
				keys[DOWN] = false;
				keys[RIGHT] = false;
				keys[LEFT] = false;

				unselectUnit();
			}
			else if(keys[ESC]) //ESC
			{
				keys[A] = false;
				keys[B] = false;
				keys[ESC] = false;
				keys[ENTER] = false;
				keys[UP] = false;
				keys[DOWN] = false;
				keys[RIGHT] = false;
				keys[LEFT] = false;

				unselectUnit();
				activateMenu(new ScenarioPauseMenu(screen, this, screen->getFontMedium(), screen->getFontLarge()));
				changeState(IN_MENU);
			}
			else if(keys[ENTER]) //ENTER
			{
			}
		}

		players[i].update(this);
	}

	map.update(&players[activePlayer], activePlayer);
	currBanner.update();
	tileInfoBanner.update();
}

void Scenario::updateCheckingMap(bool* keys)
{
	currBanner.update();
	tileInfoBanner.update();
	map.update(&players[activePlayer], activePlayer);

	for(unsigned int i = 0; i < players.size(); i++)
	{
		players[i].update(this);
	}

	if(!keys[B])
	{
		for(unsigned int j = 0; j < players.size(); j++)
		{
			players[j].setUnitsTint(al_map_rgba_f(1,1,1,1));
		}
		changeState(IDLE);
	}
}

void Scenario::updateCheckingRange(bool* keys)
{
	currBanner.update();
	tileInfoBanner.update();
	map.update(&players[activePlayer], activePlayer);

	for(unsigned int i = 0; i < players.size(); i++)
	{
		players[i].update(this);
	}

	if(!keys[B])
	{
		map.resetRangeNodes();
		changeState(IDLE);
	}
}

void Scenario::selectUnit(Unit* unitToSelect)
{
	unitToSelect->select();
	selectedUnit = unitToSelect;
	map.highlightMovementNodes(selectedUnit);
	changeState(SELECTING_UNIT);
}

void Scenario::unselectUnit()
{
	selectedUnit->deselect();
	selectedUnit = NULL;
	map.resetMovementNodes();
	changeState(IDLE);
}

void Scenario::setSelectedUnitPath(Tile* dest)
{
}

void Scenario::startUnitMovement()
{
	selectedUnit->changeState(Unit::MOVING);
	map.resetMovementNodes();
}

void Scenario::updateUnitMoving(bool* keys)
{
	for(unsigned int i = 0; i < players.size(); i++)
	{
		if(players[i].isActive() && players[i].isHuman())
		{
			if(keys[A]) //A
			{
				int cursorX = players[i].getCursorX();
				int cursorY = players[i].getCursorY();

				Tile* selectedTile = map.getTile(cursorX,cursorY);
				Tile* toStop = selectedUnit->isPathObstructed();

				if(toStop == NULL)
				{
					toStop = selectedTile;

					selectedUnit->moveSpriteTo(toStop->getX(), toStop->getY());
					selectedUnit->setEndMovementAnimation();
					activateMenu(new UnitContextMenu(this, toStop, screen->getFontMedium(), screen->getFontLarge()));
					changeState(IN_MENU);
				}
				else
					this->trapSelectedUnit(toStop);

				keys[A] = false;
				keys[B] = false;
				keys[ESC] = false;
				keys[ENTER] = false;
				keys[UP] = false;
				keys[DOWN] = false;
				keys[RIGHT] = false;
				keys[LEFT] = false;
			}
			else if(keys[B]) //B
			{
				keys[A] = false;
				keys[B] = false;
				keys[ESC] = false;
				keys[ENTER] = false;
				keys[UP] = false;
				keys[DOWN] = false;
				keys[RIGHT] = false;
				keys[LEFT] = false;

				selectedUnit->changeState(Unit::IDLE);
				selectUnit(selectedUnit);
			}
		}

		players[i].update(this);
	}

	map.update(&players[activePlayer], activePlayer);
	currBanner.update();
	tileInfoBanner.update();
}

void Scenario::trapSelectedUnit(Tile* tile)
{
	this->activateMenu(new TrapMenu(this,tile,screen->getFontMedium(),screen->getImage(ScenarioScreen::TRAP_BACKGROUNDS)));
	this->changeState(IN_MENU);
}

Unit* Scenario::getSelectedUnit() const
{
	return selectedUnit;
}

void Scenario::stopSelectedUnitOnTile(Tile* tile)
{
	if(selectedUnit->getTile() != tile)
	{
		selectedUnit->getTile()->unoccupy();
		tile->occupy(selectedUnit);
	}
	map.setVisibilityChanged(true);
	selectedUnit->changeState(Unit::RESTING);
	unselectUnit();
}

vector <Unit*> Scenario::getAttackableUnitsMelee(Tile* tile)
{
	vector<Unit* > attackableUnits;

	for(unsigned int i = 0; i < players.size(); i++)
	{
		if(i != activePlayer)
		{
			vector <Unit* > units = players[i].getUnits();

			for(unsigned int j = 0; j < units.size(); j++)
			{
				if(selectedUnit->canAttackMelee(units[j],tile) && units[j]->getTile()->isVisible())
					attackableUnits.push_back(units[j]);
			}
		}
	}

	return attackableUnits;
}

vector <Unit*> Scenario::getAttackableUnitsRange(Tile* tile)
{
	vector<Unit* > attackableUnits;

	for(unsigned int i = 0; i < players.size(); i++)
	{
		if(i != activePlayer)
		{
			vector <Unit* > units = players[i].getUnits();

			for(unsigned int j = 0; j < units.size(); j++)
			{
				if(selectedUnit->canAttackRange(units[j],tile) && units[j]->getTile()->isVisible())
					attackableUnits.push_back(units[j]);
			}
		}
	}

	return attackableUnits;
}

ALLEGRO_FONT* Scenario::getFontMedium() const
{
	return screen->getFontMedium();
}

ALLEGRO_FONT* Scenario::getFontLarge() const
{
	return screen->getFontLarge();
}

ScenarioScreen* Scenario::getScreen()
{
	return screen;
}

void Scenario::initializeBanner(ALLEGRO_BITMAP* backgrounds, ALLEGRO_FONT* font, ALLEGRO_FONT* smallfont)
{
	this->currBanner = BannerElement(this, backgrounds, font, smallfont);
}

void Scenario::initializeTileInfo(ALLEGRO_BITMAP* backgrounds, ALLEGRO_BITMAP* icons, ALLEGRO_FONT* font, ALLEGRO_FONT* smallfont)
{
	this->tileInfoBanner = TileInfoElement(this,backgrounds,icons,font,smallfont);
}

Tile* Scenario::getSelectedTile()
{
	return map.getTile(players[activePlayer].getCursorX(), players[activePlayer].getCursorY());
}

void Scenario::checkUnitRange(Unit* unit)
{
	map.highlightRangeNodes(unit);
}

Map* Scenario::getMap()
{
	return &map;
}

void Scenario::printMap() const
{
	map.print();
}
