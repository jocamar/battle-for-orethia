#include "BannerElement.h"
#include "scenario.h"

BannerElement::BannerElement(Scenario* scenario, ALLEGRO_BITMAP* bannerImages, ALLEGRO_FONT* font, ALLEGRO_FONT* smallfont) : UI_element(scenario)
{
	this->currentPlayer = scenario->getActivePlayer()->getColor();

	if(currentPlayer == 'r')
		background = Sprite(bannerImages,RIGHT_X,0,1,BANNER_WIDTH, BANNER_HEIGHT,10,0,RED*BANNER_HEIGHT);
	else if (currentPlayer == 'b')
		background = Sprite(bannerImages,RIGHT_X,0,1,BANNER_WIDTH, BANNER_HEIGHT,10,0,BLUE*BANNER_HEIGHT);
	else if (currentPlayer == 'g')
		background = Sprite(bannerImages,RIGHT_X,0,1,BANNER_WIDTH, BANNER_HEIGHT,10,0,GREEN*BANNER_HEIGHT);
	else
		background = Sprite(bannerImages,RIGHT_X,0,1,BANNER_WIDTH, BANNER_HEIGHT,10,0,YELLOW*BANNER_HEIGHT);

	this->font = font;
	this->smallfont = smallfont;

	toTheRight = true;
	state = STOPPED;
}

void BannerElement::update()
{
	this->currentPlayer = scenario->getActivePlayer()->getColor();

	if(currentPlayer == 'r')
		background.setAnimationStartY(RED*BANNER_HEIGHT);
	else if (currentPlayer == 'b')
		background.setAnimationStartY(BLUE*BANNER_HEIGHT);
	else if (currentPlayer == 'g')
		background.setAnimationStartY(GREEN*BANNER_HEIGHT);
	else
		background.setAnimationStartY(YELLOW*BANNER_HEIGHT);

	if(state == RAISING)
	{
		if(background.getY() <= -BANNER_HEIGHT)
		{
			if(toTheRight)
			{
				background.setPos(LEFT_X, background.getY());
				toTheRight = false;
			}
			else
			{
				background.setPos(RIGHT_X, background.getY());
				toTheRight = true;
			}

			state = FALLING;
		}
		else
			background.setPos(background.getX(),background.getY() - BANNER_SPEED);
	}
	else if (state == FALLING)
	{
		if(background.getY() < 0)
		{
			background.setPos(background.getX(),background.getY() + BANNER_SPEED);
		}	
		else
			state = STOPPED;
	}
	
	if(scenario->getActivePlayer()->getCursorX() < scenario->getActivePlayer()->getCamX() + SWITCH_THRESHOLD && !toTheRight)
	{
		state = RAISING;
	}
	else if (scenario->getActivePlayer()->getCursorX() >= scenario->getActivePlayer()->getCamX() + SWITCH_THRESHOLD && toTheRight)
	{
		state = RAISING;
	}
}

void BannerElement::render() const
{
	char buffer [33];
	_itoa(scenario->getActivePlayerNum()+1,buffer,10);

	background.render();

	al_draw_text(font,al_map_rgb(255,255,255),background.getX()+TEXT_PLAYER_X,background.getY()+TEXT_PLAYER_Y,0,"Player");
	al_draw_text(font,al_map_rgb(255,255,255),background.getX()+PLAYER_NUMBER_X,background.getY()+TEXT_PLAYER_Y,0,buffer);

	_itoa(scenario->getActivePlayer()->getResources(),buffer,10);
	al_draw_text(smallfont,al_map_rgb(255,255,255),background.getX()+PLAYER_RESOURCES_X,background.getY()+RESOURCES_Y,0,"Resources:");

	al_draw_text(smallfont,al_map_rgb(255,255,255),background.getX()+PLAYER_RESOURCES_NUMBER_X,background.getY()+RESOURCES_Y,ALLEGRO_ALIGN_CENTRE,buffer);
}