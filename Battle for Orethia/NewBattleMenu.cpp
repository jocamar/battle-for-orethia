#include "NewBattleMenu.h"
#include "game.h"
#include "BattleMenuScreen.h"

NewBattleMenu::NewBattleMenu(BattleMenuScreen* screen, Menu* mainBattleMenu, ALLEGRO_FONT* unselected, ALLEGRO_FONT* selected) : Menu(false, true, NewBattleMenu::NewBattleMenuDefaultSelectedOption)
{
	this->screen = screen;
	this->mainBattleMenu = mainBattleMenu;
	this->selectScenario = new SelectScenarioMenu(screen, this, unselected, selected);
	this->player1Menu = new BattlePlayerMenu(screen, unselected, PLAYER_1, false);
	this->player2Menu = new BattlePlayerMenu(screen, unselected, PLAYER_2, false);
	this->player3Menu = new BattlePlayerMenu(screen, unselected, PLAYER_3, false);
	this->player4Menu = new BattlePlayerMenu(screen, unselected, PLAYER_4, false);

	options.push_back(new Option(NEW_BATTLE_MENU_OPTION1, true));
	options.push_back(new Option(NEW_BATTLE_MENU_OPTION2, true));
	options.push_back(new Option(NEW_BATTLE_MENU_OPTION3, false));
	options.push_back(new Option(NEW_BATTLE_MENU_OPTION4, false));
	options.push_back(new Option(NEW_BATTLE_MENU_OPTION5, false));
	options.push_back(new Option(NEW_BATTLE_MENU_OPTION6, false));
	options.push_back(new Option(NEW_BATTLE_MENU_OPTION7, true));

	fonts.push_back(unselected);
	fonts.push_back(selected);
}

void NewBattleMenu::update(bool* keys)
{
	selectScenario->update(keys);

	if(active)
	{
		if(selectedOption == P1)
			player1Menu->setActive(true,false);
		else
			player1Menu->setActive(false,false);

		if(selectedOption == P2)
			player2Menu->setActive(true,false);
		else
			player2Menu->setActive(false,false);

		if(selectedOption == P3)
			player3Menu->setActive(true,false);
		else
			player3Menu->setActive(false,false);

		if(selectedOption == P4)
			player4Menu->setActive(true,false);
		else
			player4Menu->setActive(false,false);

		player1Menu->update(keys);
		player2Menu->update(keys);
		player3Menu->update(keys);
		player4Menu->update(keys);
		
		if(keys[UP])
		{
			selectedOption = getNextActive(DIR_UP);

			keys[UP] = false;
		}
		else if (keys[DOWN])
		{
			selectedOption = getNextActive(DIR_DOWN);

			keys[DOWN] = false;
		}
		else if (keys[LEFT])
		{
			if (selectedOption == STARTINGRES)
			{
				int startingRes = screen->getStartingRes();

				if(startingRes > MINIMUM_INCREMENT)
					screen->setStartingRes(startingRes - MINIMUM_INCREMENT);
			}

			keys[LEFT] = false;
		}
		else if (keys[RIGHT])
		{
			if (selectedOption == STARTINGRES)
			{
				int startingRes = screen->getStartingRes();

				if(startingRes < 100000)
					screen->setStartingRes(startingRes + MINIMUM_INCREMENT);
			}

			keys[RIGHT] = false;
		}
		else if(keys[A] || keys[ENTER])
		{	
				Map map;

			if(selectedOption == SELECTSCENARIO && this->screen->getMaps().size() > 0)
			{
				keys[A] = false;
				keys[B] = false;
				keys[ESC] = false;
				keys[ENTER] = false;
				keys[UP] = false;
				keys[DOWN] = false;
				keys[RIGHT] = false;
				keys[LEFT] = false;

				this->active = false;
				this->draw = false;
				selectScenario->setActive(true, false);
				selectScenario->setDrawing(true);
			}
			else if(selectedOption == START && screen->getChosenMap())
			{
				map = *(screen->getChosenMap());
				Map newMap = Map(map);

				int numSelectedPlayers = screen->getNumOfSelectedPlayers();

				if(numSelectedPlayers >= 2 && screen->atLeastOneHuman())
				{
					Scenario* tmpScenario = NULL;
					vector <Player> players;
					int numRemainingPlayers = numSelectedPlayers;
					int resources = screen->getStartingRes();
					int p1 = screen->getPlayer1();
					int p2 = screen->getPlayer2();
					int p3 = screen->getPlayer3();
					int p4 = screen->getPlayer4();

					for(int i = 0; i < PLAYER_NUM; i++)
					{
						bool human;
						char color;
						bool playing = false;

						if(map.getPlayersAllowed()[i])
						{
							if(i == 0)
							{
								if(p1 == PLAYER)
								{
									human = true;
									playing = true;
								}
								else if (p1 == CPU)
								{
									human = false;
									playing = true;
								}

								color = 'r';
							}
							else if(i == 1)
							{
								if(p2 == PLAYER)
								{
									human = true;
									playing = true;
								}
								else if (p2 == CPU)
								{
									human = false;
									playing = true;
								}

								color = 'b';
							}
							else if(i == 2)
							{
								if(p3 == PLAYER)
								{
									human = true;
									playing = true;
								}
								else if (p3 == CPU)
								{
									human = false;
									playing = true;
								}

								color = 'g';
							}
							else if(i == 3)
							{
								if(p4 == PLAYER)
								{
									human = true;
									playing = true;
								}
								else if (p4 == CPU)
								{
									human = false;
									playing = true;
								}

								color = 'y';
							}

							if(playing)
							{
								Player tmpPlayer = Player(human, 0, 0, resources, false, false, color);

								players.push_back(tmpPlayer);
							}
						}
					}
					//newMap.print();
					tmpScenario = new Scenario(numRemainingPlayers, players, newMap);
					tmpScenario->printMap();
					//newMap.print();
					screen->switchToScenario(tmpScenario);
				}
			}
		}
		else if(keys[B] || keys[ESC])
		{
			keys[A] = false;
			keys[B] = false;
			keys[ESC] = false;
			keys[ENTER] = false;
			keys[UP] = false;
			keys[DOWN] = false;
			keys[RIGHT] = false;
			keys[LEFT] = false;
			
			active = false;
			mainBattleMenu->setActive(true, false);
		}

	}
}

void NewBattleMenu::render() const
{
	if(draw && !selectScenario->isDrawing())
	{
		int startingRes = screen->getStartingRes();
		Map* chosenMap = screen->getChosenMap();

		stringstream toString;

		toString << startingRes;


		//Map
		if(selectedOption == SELECTSCENARIO && active)
		{
			al_draw_filled_rectangle( OPTION1_CONTOUR_LEFT_LIMIT, OPTION1_CONTOUR_UPPER_LIMIT, OPTION1_CONTOUR_RIGHT_LIMIT, OPTION1_CONTOUR_LOWER_LIMIT, NEW_BATTLE_MENU_OVERLAY_COLOR);
		}

		if(options[SELECTSCENARIO]->active)
		{
			al_draw_text(fonts[FONT], NEW_BATTLE_MENU_TEXT_COLOR, OPTION1_TEXT_X, OPTION1_TEXT_Y, ALLEGRO_ALIGN_CENTRE, NEW_BATTLE_MENU_OPTION1);
			al_draw_rectangle(OPTION1_CONTOUR_LEFT_LIMIT, OPTION1_CONTOUR_UPPER_LIMIT, OPTION1_CONTOUR_RIGHT_LIMIT, OPTION1_CONTOUR_LOWER_LIMIT, NEW_BATTLE_MENU_CONTOUR_COLOR, CONTOUR_THICKNESS);

			if(chosenMap != NULL)
				al_draw_text(fonts[FONT], NEW_BATTLE_MENU_TEXT_COLOR, OPTION1_MAP_NAME_X, OPTION1_MAP_NAME_Y, 0, chosenMap->getName().c_str());
		}
		else
		{
			al_draw_text(fonts[FONT], NEW_BATTLE_MENU_BLOCKED_COLOR, OPTION1_TEXT_X, OPTION1_TEXT_Y, ALLEGRO_ALIGN_CENTRE, NEW_BATTLE_MENU_OPTION1);
			al_draw_rectangle(OPTION1_CONTOUR_LEFT_LIMIT, OPTION1_CONTOUR_UPPER_LIMIT, OPTION1_CONTOUR_RIGHT_LIMIT, OPTION1_CONTOUR_LOWER_LIMIT, NEW_BATTLE_MENU_BLOCKED_COLOR, CONTOUR_THICKNESS);

			if(chosenMap != NULL)
				al_draw_text(fonts[FONT], NEW_BATTLE_MENU_BLOCKED_COLOR, OPTION1_MAP_NAME_X, OPTION1_MAP_NAME_Y, 0, chosenMap->getName().c_str());
		}

		//Starting resources
		if(selectedOption == STARTINGRES && active)
		{
			al_draw_filled_rectangle( OPTION2_CONTOUR_LEFT_LIMIT, OPTION2_CONTOUR_UPPER_LIMIT, OPTION2_CONTOUR_RIGHT_LIMIT, OPTION2_CONTOUR_LOWER_LIMIT, NEW_BATTLE_MENU_OVERLAY_COLOR);
		}

		if(options[STARTINGRES]->active)
		{
			al_draw_text(fonts[FONT], NEW_BATTLE_MENU_TEXT_COLOR, OPTION2_TEXT_X, OPTION2_TEXT_Y, 0, NEW_BATTLE_MENU_OPTION2);
			al_draw_rectangle(OPTION2_CONTOUR_LEFT_LIMIT, OPTION2_CONTOUR_UPPER_LIMIT, OPTION2_CONTOUR_RIGHT_LIMIT, OPTION2_CONTOUR_LOWER_LIMIT, NEW_BATTLE_MENU_CONTOUR_COLOR, CONTOUR_THICKNESS);
			al_draw_filled_triangle(OPTION2_LEFT_TRIANGLE_TIP_X, OPTION2_TRIANGLE_TIP_Y, OPTION2_LEFT_TRIANGLE_RIGHT_LIMIT, OPTION2_CONTOUR_UPPER_LIMIT, OPTION2_LEFT_TRIANGLE_RIGHT_LIMIT, OPTION2_CONTOUR_LOWER_LIMIT, NEW_BATTLE_MENU_CONTOUR_COLOR);
			al_draw_filled_triangle(OPTION2_RIGHT_TRIANGLE_TIP_X, OPTION2_TRIANGLE_TIP_Y, OPTION2_RIGHT_TRIANGLE_LEFT_LIMIT, OPTION2_CONTOUR_UPPER_LIMIT, OPTION2_RIGHT_TRIANGLE_LEFT_LIMIT, OPTION2_CONTOUR_LOWER_LIMIT, NEW_BATTLE_MENU_CONTOUR_COLOR);
			al_draw_text(fonts[FONT], NEW_BATTLE_MENU_TEXT_COLOR, OPTION2_RESOURCE_AMOUNT_X, OPTION2_RESOURCE_AMOUNT_Y, ALLEGRO_ALIGN_CENTRE, toString.str().c_str());
		}
		else
		{
			al_draw_text(fonts[FONT], NEW_BATTLE_MENU_BLOCKED_COLOR, OPTION2_TEXT_X, OPTION2_TEXT_Y, 0, NEW_BATTLE_MENU_OPTION2);
			al_draw_rectangle(OPTION2_CONTOUR_LEFT_LIMIT, OPTION2_CONTOUR_UPPER_LIMIT, OPTION2_CONTOUR_RIGHT_LIMIT, OPTION2_CONTOUR_LOWER_LIMIT, NEW_BATTLE_MENU_BLOCKED_COLOR, CONTOUR_THICKNESS);
			al_draw_filled_triangle(OPTION2_LEFT_TRIANGLE_TIP_X, OPTION2_TRIANGLE_TIP_Y, OPTION2_LEFT_TRIANGLE_RIGHT_LIMIT, OPTION2_CONTOUR_UPPER_LIMIT, OPTION2_LEFT_TRIANGLE_RIGHT_LIMIT, OPTION2_CONTOUR_LOWER_LIMIT, NEW_BATTLE_MENU_BLOCKED_COLOR);
			al_draw_filled_triangle(OPTION2_RIGHT_TRIANGLE_TIP_X, OPTION2_TRIANGLE_TIP_Y, OPTION2_RIGHT_TRIANGLE_LEFT_LIMIT, OPTION2_CONTOUR_UPPER_LIMIT, OPTION2_RIGHT_TRIANGLE_LEFT_LIMIT, OPTION2_CONTOUR_LOWER_LIMIT, NEW_BATTLE_MENU_BLOCKED_COLOR);
			al_draw_text(fonts[FONT], NEW_BATTLE_MENU_BLOCKED_COLOR, OPTION2_RESOURCE_AMOUNT_X, OPTION2_RESOURCE_AMOUNT_Y, ALLEGRO_ALIGN_CENTRE, toString.str().c_str());
		}

		int order = 0;
		
		if(player1Menu->isDrawing())
		{
			player1Menu->render(order);
			order++;
		}

		if(player2Menu->isDrawing())
		{
			player2Menu->render(order);
			order++;
		}

		if(player3Menu->isDrawing())
		{
			player3Menu->render(order);
			order++;
		}

		if(player4Menu->isDrawing())
		{
			player4Menu->render(order);
		}

		//Start
		if(selectedOption == START && active)
		{
			al_draw_filled_rectangle( OPTION7_CONTOUR_LEFT_LIMIT, OPTION7_CONTOUR_UPPER_LIMIT, OPTION7_CONTOUR_RIGHT_LIMIT, OPTION7_CONTOUR_LOWER_LIMIT, NEW_BATTLE_MENU_OVERLAY_COLOR);
		}

		if(options[START]->active)
		{
			al_draw_text(fonts[FONT], NEW_BATTLE_MENU_TEXT_COLOR, OPTION7_TEXT_X, OPTION7_TEXT_Y, 0, NEW_BATTLE_MENU_OPTION7);
			al_draw_rectangle(OPTION7_CONTOUR_LEFT_LIMIT, OPTION7_CONTOUR_UPPER_LIMIT, OPTION7_CONTOUR_RIGHT_LIMIT, OPTION7_CONTOUR_LOWER_LIMIT, NEW_BATTLE_MENU_CONTOUR_COLOR, CONTOUR_THICKNESS);
		}
		else
		{
			al_draw_text(fonts[FONT],NEW_BATTLE_MENU_BLOCKED_COLOR, OPTION7_TEXT_X, OPTION7_TEXT_Y, 0, NEW_BATTLE_MENU_OPTION7);
			al_draw_rectangle(OPTION7_CONTOUR_LEFT_LIMIT, OPTION7_CONTOUR_UPPER_LIMIT, OPTION7_CONTOUR_RIGHT_LIMIT, OPTION7_CONTOUR_LOWER_LIMIT, NEW_BATTLE_MENU_BLOCKED_COLOR, CONTOUR_THICKNESS);
		}

	}

	selectScenario->render();
}

void NewBattleMenu::resetPlayers()
{
	Map* chosenMap = screen->getChosenMap();

	vector <bool> allowedPlayers = chosenMap->getPlayersAllowed();

	delete player1Menu;
	delete player2Menu;
	delete player3Menu;
	delete player4Menu;

	for(unsigned int i = 0; i < allowedPlayers.size(); i++)
	{
		if(allowedPlayers[i])
		{
			options[P1+i]->active = true;

			if(i == PLAYER_1)
				player1Menu = new BattlePlayerMenu(screen, fonts[FONT], PLAYER_1, true);
			else if (i == PLAYER_2)
				player2Menu = new BattlePlayerMenu(screen, fonts[FONT], PLAYER_2, true);
			else if (i == PLAYER_3)
				player3Menu = new BattlePlayerMenu(screen, fonts[FONT], PLAYER_3, true);
			else if (i == PLAYER_4)
				player4Menu = new BattlePlayerMenu(screen, fonts[FONT], PLAYER_4, true);
		}
		else
		{
			options[P1+i]->active = false;

			if(i == PLAYER_1)
				player1Menu = new BattlePlayerMenu(screen, fonts[FONT], PLAYER_1, false);
			else if (i == PLAYER_2)
				player2Menu = new BattlePlayerMenu(screen, fonts[FONT], PLAYER_2, false);
			else if (i == PLAYER_3)
				player3Menu = new BattlePlayerMenu(screen, fonts[FONT], PLAYER_3, false);
			else if (i == PLAYER_4)
				player4Menu = new BattlePlayerMenu(screen, fonts[FONT], PLAYER_4, false);
		}
	}
}

NewBattleMenu::~NewBattleMenu()
{
	delete selectScenario;
	delete player1Menu;
	delete player2Menu;
	delete player3Menu;
	delete player4Menu;
}