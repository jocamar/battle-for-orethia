#include "UnitContextMenu.h"
#include "game.h"
#include "scenario.h"
#include "CaptureMenu.h"
#include "ScenarioScreen.h"
#include "UnitAttackMenu.h"


UnitContextMenu::UnitContextMenu(Scenario* scenario, Tile* tile, ALLEGRO_FONT* unselected, ALLEGRO_FONT* selected) : Menu(false, false, UnitContextMenu::UnitContextMenuDefaultSelectedOption)
{
	this->scenario = scenario;

	fonts.push_back(unselected);
	fonts.push_back(selected);

	unitTile = tile;

	meleeAttackOptions = scenario->getAttackableUnitsMelee(tile);
	rangedAttackOptions = scenario->getAttackableUnitsRange(tile);

	options.push_back(new Option(UNIT_CONTEXT_MENU_WAIT,true));

	if(unitTile->isOwnable() && unitTile->getOwnership() != scenario->getSelectedUnit()->getColor() && scenario->getSelectedUnit()->canCapture())
		options.push_back(new Option(UNIT_CONTEXT_MENU_CAPTURE, true));
	else
		options.push_back(new Option(UNIT_CONTEXT_MENU_CAPTURE, false));

	if(meleeAttackOptions.size() != 0)
		options.push_back(new Option(UNIT_CONTEXT_MENU_MELEE_ATTACK, true));
	else
		options.push_back(new Option(UNIT_CONTEXT_MENU_MELEE_ATTACK, false));

	if(rangedAttackOptions.size() != 0)
		options.push_back(new Option(UNIT_CONTEXT_MENU_RANGED_ATTACK, true));
	else
		options.push_back(new Option(UNIT_CONTEXT_MENU_RANGED_ATTACK, false));

	numOptions = getNumActiveOptions();

	x = (tile->getX()*TileInfo::TILE_WIDTH) - (scenario->getActivePlayer()->getCamX() * TileInfo::TILE_WIDTH);
	y = (tile->getY()*TileInfo::TILE_HEIGHT) - (scenario->getActivePlayer()->getCamY() * TileInfo::TILE_HEIGHT);

	if(x < WIDTH/2)
		x += TEXT_WIDTH/2;
	else 
		x -= (TEXT_WIDTH + TEXT_WIDTH/4);

	if(y < HEIGHT/2)
		y += TEXT_HEIGHT;
	else 
		y -= TEXT_HEIGHT*numOptions;
}

void UnitContextMenu::update(bool* keys)
{
	if(active)
	{
		scenario->updateTileInfo();

		if(keys[DOWN])
		{
			selectedOption = getNextActive(DIR_DOWN);

			keys[DOWN] = false;
		}
		else if(keys[UP])
		{
			selectedOption = getNextActive(DIR_UP);

			keys[UP] = false;
		}
		else if(keys[A] || keys[ENTER])
		{
			keys[A] = false;
			keys[B] = false;
			keys[ESC] = false;
			keys[ENTER] = false;
			keys[UP] = false;
			keys[DOWN] = false;
			keys[RIGHT] = false;
			keys[LEFT] = false;

			if(selectedOption == WAIT)
			{
				scenario->stopSelectedUnitOnTile(unitTile);
			}
			else if (selectedOption == CAPTURE)
			{
				scenario->activateMenu(new CaptureMenu(scenario,unitTile,scenario->getScreen()->getImage(ScenarioScreen::CAPTURE_ANIM),scenario->getScreen()->getImage(ScenarioScreen::FLAG_ANIM), scenario->getScreen()->getImage(ScenarioScreen::CAPTURE_MENU_BACKGROUND)));
			}
			else if (selectedOption == MELEE)
			{
				scenario->activateMenu(new UnitAttackMenu(scenario,scenario->getScreen()->getFontTiny(),scenario->getScreen()->getFontSmall(),scenario->getScreen()->getImage(ScenarioScreen::ATTACK_MENU_CURSOR),meleeAttackOptions,unitTile,UnitAttackMenu::MELEE));
			}
			else if (selectedOption == RANGE)
			{
				scenario->activateMenu(new UnitAttackMenu(scenario,scenario->getScreen()->getFontTiny(),scenario->getScreen()->getFontSmall(),scenario->getScreen()->getImage(ScenarioScreen::ATTACK_MENU_CURSOR),rangedAttackOptions,unitTile,UnitAttackMenu::RANGED));
			}
		}
		else if (keys[B] || keys[ESC])
		{
			keys[A] = false;
			keys[B] = false;
			keys[ESC] = false;
			keys[ENTER] = false;
			keys[UP] = false;
			keys[DOWN] = false;
			keys[RIGHT] = false;
			keys[LEFT] = false;

			this->active = false;
			this->draw = false;

			scenario->getSelectedUnit()->changeState(Unit::IDLE);
			scenario->selectUnit(scenario->getSelectedUnit());
		}
	}
}

void UnitContextMenu::render() const
{
	if(draw)
	{
		al_draw_filled_rectangle(x,y,x+TEXT_WIDTH,y+numOptions*TEXT_HEIGHT,UNIT_CONTEXT_MENU_BACK_COLOR);

		int count = 0;

		for(unsigned int i = 0; i < options.size(); i++)
		{
			if(i == selectedOption)
			{
				if(options[i]->active)
				{
					al_draw_text(fonts[SELECTED], UNIT_CONTEXT_MENU_TEXT_COLOR, x+TEXT_X_SPACING, y+TEXT_Y_SPACING + count*TEXT_SELECTED_SPACING, ALLEGRO_ALIGN_CENTRE, options[i]->Name.c_str());
					count++;
				}
			}
			else
			{
				if(options[i]->active)
				{
					al_draw_text(fonts[UNSELECTED], UNIT_CONTEXT_MENU_TEXT_COLOR, x+TEXT_X_SPACING, y+TEXT_Y_SPACING + count*TEXT_UNSELECTED_SPACING, ALLEGRO_ALIGN_CENTRE, options[i]->Name.c_str());
					count++;
				}
			}
		}

		scenario->renderBanner();
		scenario->renderTileInfo();
	}
}

UnitContextMenu::~UnitContextMenu()
{
}

int UnitContextMenu::getNumActiveOptions() const
{
	int count = 0;

	for(unsigned int i = 0; i < options.size(); i++)
	{
		if(options[i]->active)
			count++;
	}

	return count;
}