#ifndef BANNERELEMENT_H
#define BANNERELEMENT_H

#include "UIelement.h"
#include "Sprite.h"
#include "allegro5/allegro5.h"
#include "allegro5/allegro_font.h"

#define SWITCH_THRESHOLD 8
#define BANNER_SPEED 40

class BannerElement : public UI_element
{
	Sprite background;
	ALLEGRO_FONT* font;
	ALLEGRO_FONT* smallfont;
	char currentPlayer;
	bool toTheRight;
	int state;

	enum Coords{
				RIGHT_X = 1065, 
				LEFT_X = 15, 
				BANNER_WIDTH = 200, 
				BANNER_HEIGHT = 322, 
				TEXT_PLAYER_X = 45, 
				PLAYER_NUMBER_X = 145, 
				PLAYER_RESOURCES_X = 20, 
				PLAYER_RESOURCES_NUMBER_X = 150,
				TEXT_PLAYER_Y = 35,
				RESOURCES_Y = 115
	};

	enum Banners{RED, BLUE, GREEN, YELLOW};
	enum States{RAISING, FALLING, STOPPED};

public:
	BannerElement() {};
	BannerElement(Scenario* scenario, ALLEGRO_BITMAP* bannerImages = NULL, ALLEGRO_FONT* font = NULL, ALLEGRO_FONT* smallfont = NULL);
	void update();
	void render() const;
};

#endif