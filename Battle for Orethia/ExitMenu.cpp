#include "ExitMenu.h"
#include "game.h"
#include "MainMenuScreen.h"

ExitMenu::ExitMenu(MainMenuScreen* screen, Menu* mainMenu,  ALLEGRO_FONT* unselected, ALLEGRO_FONT* selected) : Menu(false, false, ExitMenu::ExitMenuDefaultSelectedOption)
{
	this->screen = screen;
	this->mainMenu = mainMenu;

	options.push_back(new Option(EXIT_MENU_OPTION1, true));
	options.push_back(new Option(EXIT_MENU_OPTION2, true));

	fonts.push_back(unselected);
	fonts.push_back(selected);
}

void ExitMenu::update(bool* keys) //exitMenu specific update
{
	if(active)
	{

		if(keys[DOWN])
		{
			selectedOption = getNextActive(DIR_DOWN);

			keys[DOWN] = false;
		}
		else if(keys[UP])
		{
			selectedOption = getNextActive(DIR_UP);

			keys[UP] = false;
		}
		else if(keys[A] || keys[ENTER])
		{
			if(selectedOption == YES)
			{
				screen->exit();
			}
			else if(selectedOption == NO)
			{
				keys[A] = false;
				keys[B] = false;
				keys[ESC] = false;
				keys[ENTER] = false;
				keys[UP] = false;
				keys[DOWN] = false;
				keys[RIGHT] = false;
				keys[LEFT] = false;

				this->active = false;
				this->draw = false;
				mainMenu->setActive(true);
				mainMenu->setDrawing(true);
			}
		}
		else if (keys[B] || keys[ESC])
		{
			keys[A] = false;
			keys[B] = false;
			keys[ESC] = false;
			keys[ENTER] = false;
			keys[UP] = false;
			keys[DOWN] = false;
			keys[RIGHT] = false;
			keys[LEFT] = false;

			this->active = false;
			this->draw = false;
			mainMenu->setActive(true);
			mainMenu->setDrawing(true);
		}
	}
}

void ExitMenu::render() const
{
	if(draw)
	{
		al_draw_text(fonts[UNSELECTED],EXIT_MENU_TEXT_COLOR,TEXT_X,TEXT_WARNING_Y,ALLEGRO_ALIGN_CENTRE,"Are you sure you");
		al_draw_text(fonts[UNSELECTED],EXIT_MENU_TEXT_COLOR,TEXT_X,TEXT_WARNING_Y + TEXT_WARNING_SPACING,ALLEGRO_ALIGN_CENTRE,"wish to exit the game?");

		for(unsigned int i = 0; i < options.size(); i++)
		{
			if(i == selectedOption)
			{
				if(options[i]->active)
					al_draw_text(fonts[SELECTED], EXIT_MENU_TEXT_COLOR, TEXT_X, TEXT_INIT_SELECTED_Y + i*TEXT_SELECTED_SPACING, ALLEGRO_ALIGN_CENTRE, options[i]->Name.c_str());
				else
					al_draw_text(fonts[SELECTED], EXIT_MENU_BLOCKED_COLOR, TEXT_X, TEXT_INIT_SELECTED_Y + i*TEXT_SELECTED_SPACING, ALLEGRO_ALIGN_CENTRE, options[i]->Name.c_str());
			}
			else
			{
				if(options[i]->active)
					al_draw_text(fonts[UNSELECTED], EXIT_MENU_TEXT_COLOR, TEXT_X, TEXT_INIT_UNSELECTED_Y + i*TEXT_UNSELECTED_SPACING, ALLEGRO_ALIGN_CENTRE, options[i]->Name.c_str());
				else
					al_draw_text(fonts[UNSELECTED], EXIT_MENU_BLOCKED_COLOR, TEXT_X, TEXT_INIT_UNSELECTED_Y + i*TEXT_UNSELECTED_SPACING, ALLEGRO_ALIGN_CENTRE, options[i]->Name.c_str());
			}
		}
	}
}

ExitMenu::~ExitMenu()
{
}