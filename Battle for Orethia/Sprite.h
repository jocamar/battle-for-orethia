#ifndef SPRITE_H
#define SPRITE_H

#include <allegro5/allegro.h>           //   Includes allegro
#include <allegro5/allegro_image.h>

class Sprite
{
	ALLEGRO_BITMAP* image;
	ALLEGRO_COLOR tint;
	int frameNum;
	int frameHeight, frameWidth;
	int frameDelay, counter;
	int currFrame;
	int animationStartX, animationStartY;
	int x, y;

public:
	Sprite (ALLEGRO_BITMAP* image = NULL, int x = 0, int y = 0, int frameNum = 0, int frameWidth = 0, int frameHeight = 0, int frameDelay = 0, int animationStartX = 0, int animationStartY = 0, ALLEGRO_COLOR tint = al_map_rgba_f(1,1,1,1));
	
	void render(int draw_flags = 0) const;
	void update();

	void setAnimationStartX(int newX);
	void setAnimationStartY(int newY);
	void restartAnimation();
	void setImage(ALLEGRO_BITMAP* newImage);
	void setAnimationDelay(int newDelay);
	void setFrameNum(int newFrameNum);
	void setTint(ALLEGRO_COLOR newTint);
	
	int getAnimationStartX() const;
	int getAnimationStartY() const;
	int getCurrFrame() const;
	int getX() const;
	int getY() const;
	ALLEGRO_BITMAP* getImage() const;
	bool finishedAnimation() const;
	void setPos(int x, int y);
	~Sprite();
};

#endif