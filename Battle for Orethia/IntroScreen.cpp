#include "IntroScreen.h"

IntroScreen::IntroScreen(Game* game) : Screen(game)
{
	images.push_back(al_load_bitmap("resources/images/intro.png"));
	startCounter = 0;
	counter = 0;
}

void IntroScreen::update(bool* keys)
{
	counter++;

	if(keys[ESC] || keys[A] || keys[B] || keys[ENTER] || counter - startCounter >= INTRO_TIME)
	{
		game->changeScreen(new MainMenuScreen(game));
	}
}

void IntroScreen::render() const
{
	al_draw_bitmap(images[INTRO_BCKGROUND],0,0,0);
}

IntroScreen::~IntroScreen()
{
}