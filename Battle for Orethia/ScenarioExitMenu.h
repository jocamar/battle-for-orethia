#ifndef SCENARIOEXITMENU_H
#define SCENARIOEXITMENU_H

#include "menu.h"
#include "allegro5/allegro5.h"
#include "allegro5/allegro_font.h"

#define SCENARIO_EXIT_MENU_TEXT_COLOR					al_map_rgb(255,255,255) //white
#define SCENARIO_EXIT_MENU_BLOCKED_COLOR				al_map_rgb(150,150,150)  //grey
#define SCENARIO_EXIT_MENU_BACK_COLOR					al_map_rgba(0,0,0,220)  //transparent black

#define SCENARIO_EXIT_MENU_OPTION1					"Yes"
#define SCENARIO_EXIT_MENU_OPTION2					"No"

class ScenarioScreen;

class ScenarioExitMenu : public Menu
{
	static int const ScenarioExitMenuDefaultSelectedOption = 0;

	enum Fonts {UNSELECTED, SELECTED};
	enum textCoords {TEXT_X = 640, TEXT_INIT_UNSELECTED_Y = 400, TEXT_INIT_SELECTED_Y = 390, TEXT_UNSELECTED_SPACING = 100, TEXT_SELECTED_SPACING = 100, TEXT_WARNING_Y = 250, TEXT_WARNING_SPACING = 50};
	enum Coords {MENU_LEFT_LIMIT = 420, MENU_RIGHT_LIMIT = 860, MENU_UPPER_LIMIT = 150, MENU_LOWER_LIMIT = 600};
	enum OptionNames {YES,NO};

	ScenarioScreen* screen;
	Menu* mainMenu;

public:

	ScenarioExitMenu(ScenarioScreen* screen, Menu* mainMenu, ALLEGRO_FONT* unselected, ALLEGRO_FONT* selected);

	void update(bool* keys);
	void render() const;

	~ScenarioExitMenu();
};



#endif