#ifndef SELECTSCENARIOMENU_H
#define SELECTSCENARIOMENU_H

#include "menu.h"
#include "allegro5/allegro5.h"
#include "allegro5/allegro_primitives.h"
#include "allegro5/allegro_font.h"

#define SELECT_SCENARIO_MENU_TEXT_COLOR				al_map_rgb(255,255,255) //white
#define SELECT_SCENARIO_MENU_BLOCKED_COLOR			al_map_rgb(150,150,150)  //grey
#define SELECT_SCENARIO_MENU_CONTOUR_COLOR			al_map_rgb(255,255,255) //white

class BattleMenuScreen;
class NewBattleMenu;

class SelectScenarioMenu : public Menu
{
	static int const SelectScenarioMenuDefaultSelectedOption = 0;

	enum Fonts {UNSELECTED, SELECTED};
	enum Coords {
				  MAP_TITLE_X = 500,
				  MAP_TITLE_UNSELECTED_INIT_Y = 200,
				  MAP_TITLE_SELECTED_INIT_Y = 195,
				  MAP_TITLE_SPACING = 100,
				  MAP_PREVIEW_X = 800,
				  MAP_PREVIEW_Y = 250
				};

	BattleMenuScreen* screen;
	NewBattleMenu* newBattleMenu;

	int previouslySelectedOption;

public:

	SelectScenarioMenu(BattleMenuScreen* screen, NewBattleMenu* newBattleMenu, ALLEGRO_FONT* unselected, ALLEGRO_FONT* selected);

	void update(bool* keys);
	void render() const;

	~SelectScenarioMenu();
};



#endif