#include "screen.h"

Screen::Screen(Game* game)
{
	this->game = game;
}

Screen::~Screen()
{
	for(int i = 0; i < images.size(); i++)
	{
		al_destroy_bitmap(images[i]);
	}
}

ALLEGRO_BITMAP* Screen::getImage(int imageId)
{
	return images[imageId];
}