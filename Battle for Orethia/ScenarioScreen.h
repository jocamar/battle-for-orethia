#ifndef SCENARIOSCREEN_H
#define SCENARIOSCREEN_H

#include "screen.h"
#include "allegro5/allegro5.h"
#include "allegro5/allegro_primitives.h"
#include "allegro5/allegro_font.h"
#include "game.h"
#include "scenario.h"

#define SCENARIO_TEXT_COLOR						al_map_rgb(255,255,255) //white
#define SCENARIO_OVERLAY_COLOR					al_map_rgba(0,0,0,220)  //transparent black
#define SCENARIO_CONTOUR_COLOR					al_map_rgb(255,255,255) //white
#define TRANSPARENT_SCENARIO					al_map_rgb(255,0,255) //magenta


class Game;

class ScenarioScreen : public Screen
{
	enum FontSizes {SIZE_LARGE = 45, SIZE_SMALL = 20, SIZE_MEDIUM = 35, SIZE_TINY = 14};

	Scenario* currScenario;

	ALLEGRO_FONT* medium_font;
	ALLEGRO_FONT* small_font;
	ALLEGRO_FONT* tiny_font;
	ALLEGRO_FONT* large_font;

public:
	enum ImageID {TILES, FOOTTROOPER, ARROWS, CAPTURE_ANIM, FLAG_ANIM, UNIT_ICONS, CURSOR, FOG_OF_WAR, BANNERS, CAPTURE_MENU_BACKGROUND, TILE_INFO_BACKGROUND, ATTACK_MENU_CURSOR, TRAP_BACKGROUNDS};

	ScenarioScreen(Game* game, Scenario* scenario);

	ALLEGRO_FONT* getFontSmall() const;
	ALLEGRO_FONT* getFontMedium() const;
	ALLEGRO_FONT* getFontLarge() const;
	ALLEGRO_FONT* getFontTiny() const;

	void update(bool* keys);
	void render() const;

	void switchToMain();

	~ScenarioScreen();
};

#endif