#ifndef MAINMENU_H
#define MAINMENU_H

#include "allegro5/allegro5.h"
#include "allegro5/allegro_font.h"

#include "menu.h"
#include "ExitMenu.h"

#define MAIN_MENU_TEXT_COLOR				al_map_rgb(255,255,255) //white
#define MAIN_MENU_BLOCKED_COLOR				al_map_rgb(150,150,150)  //grey

#define MAIN_MENU_OPTION1					"Campaign"
#define MAIN_MENU_OPTION2					"Battle"
#define MAIN_MENU_OPTION3					"Exit"

class MainMenuScreen;

class MainMenu : public Menu
{
	static int const MainMenuDefaultSelectedOption = 1;

	enum Fonts {UNSELECTED, SELECTED};
	enum textCoords {TEXT_X = 200, TEXT_INIT_UNSELECTED_Y = 300, TEXT_INIT_SELECTED_Y = 290, TEXT_UNSELECTED_SPACING = 100, TEXT_SELECTED_SPACING = 100};
	enum OptionNames {CAMPAIGN,BATTLE,EXIT};

	MainMenuScreen* screen;

	ExitMenu* exitMenu;

public:

	MainMenu(MainMenuScreen* screen, ALLEGRO_FONT* unselected, ALLEGRO_FONT* selected);

	void update(bool* keys);
	void render() const;

	~MainMenu();
};



#endif