#include "gameMap.h"
#include "game.h"

Map::Map(string name, map<int,TileInfo> tileInfo)
{
	//this->playersAllowed = playersAllowed;
	this->tileInfo = tileInfo;
	//this->tiles = tiles;
	this->sizeY = 0;
	this->sizeX = 0;
	playersAllowed = vector<bool> (PLAYER_NUM,false);
	this->name = name;
	this->fog_of_war = Sprite(NULL,0,0,1,TileInfo::TILE_WIDTH,TileInfo::TILE_HEIGHT,10,0,0);

	this->visibilityChanged = true;
}

Map::Map(const Map & other)
{
	this->name = other.name;
	this->playersAllowed = other.playersAllowed;
	this->sizeX = other.sizeX;
	this->sizeY = other.sizeY;
	this->tileInfo = other.tileInfo;
	this->visibilityChanged = other.visibilityChanged;
	this->fog_of_war = other.fog_of_war;

	for(unsigned int i = 0; i < other.tiles.size(); i++)
	{
		vector<Tile> line;

		for(unsigned int j = 0; j < other.tiles[i].size(); j++)
		{
			Tile tmpTile = Tile(other.tiles[i][j],tileInfo);
			line.push_back(tmpTile);
		}

		this->tiles.push_back(line);
	}
	cout << "copy constructor" << endl;
}

const vector <bool> & Map::getPlayersAllowed() const
{
	return playersAllowed;
}

int Map::getSizeX() const
{
	return sizeX;
}

int Map::getSizeY() const
{
	return sizeY;
}

Tile* Map::getTile(int x, int y)
{
	return &tiles[y][x];
}

Map::~Map()
{
	/*for(unsigned int i = 0; i < tiles.size(); i++)
	{
		for(unsigned int j = 0; j < tiles[i].size(); j++)
		{
			delete tiles[i][j];
		}
	}*/
}

void Map::render(int camX, int camY, int playerNum) const
{
	for(int y = 0; y < tiles.size(); y++)
	{
		for(int x = 0; x < tiles[y].size(); x++)
		{
			tiles[y][x].render(camX, camY, playerNum);
		}
	}

	renderFoW(camX, camY);
	renderHW(camX, camY);
	renderHR(camX, camY);
}

void Map::renderMinimap(int posx, int posy, int playerNum) const
{
	for(int y = 0; y < tiles.size(); y++)
	{
		for(int x = 0; x < tiles[y].size(); x++)
		{
			tiles[y][x].renderMinimapTile(posx,posy, playerNum);
		}
	}
}

string Map::getName() const
{
	return name;
}

void Map::update(Player* player, int playerNum)
{
	for(unsigned int i = 0; i < tiles.size(); i++)
	{
		for(unsigned int j = 0; j < tiles[i].size(); j++)
		{
			tiles[i][j].update(player->getCamX(), player->getCamY());
		}
	}

	if(visibilityChanged)
	{
		resetVisibility();
		setPlayerTilesVis(player->getColor());
		

		for(unsigned int i = 0; i < player->getUnits().size(); i++)
		{
			updateVisibility(player->getUnits()[i]);
		}

		for(unsigned int i = 0; i < tiles.size(); i++)
		{
			for(unsigned int j = 0; j < tiles[i].size(); j++)
			{
				if(tiles[i][j].isVisible() || tiles[i][j].getPrevOwnershipFor(playerNum) == player->getColor())
					tiles[i][j].updateTileFor(playerNum);

				tiles[i][j].showSpriteFor(playerNum);
			}
		}

		visibilityChanged = false;
	}
}

void Map::updateVisibility(Unit* unit)
{

	int unitX = unit->getX();
	int unitY = unit->getY();
	int visRange = unit->getVis();
	
	switchVisibility(unitX, unitY, visRange);
	
}

void Map::setVisibilityChanged(bool newVal)
{
	this->visibilityChanged = newVal;
}

void Map::switchVisibility(int x, int y, int VisRange)
{
	for(unsigned int i = 0; i < tiles.size(); i++)
	{
		for(unsigned int j = 0; j < tiles[i].size(); j++)
		{
			int difX = abs(x-(signed)j);
			int difY = abs(y-(signed)i);
			int total = difX + difY;

			if(total <= VisRange)
			{
				if(total <= 1)
					tiles[i][j].setVisible(true);
				else if(!tiles[i][j].isHideout())
					tiles[i][j].setVisible(true);
			}
		}
	}
}

void Map::resetVisibility()
{
	for(unsigned int i = 0; i < tiles.size(); i++)
	{
		for(unsigned int j = 0; j < tiles[i].size(); j++)
		{
			tiles[i][j].setVisible(false);
		}
	}
}

void Map::setPlayerTilesVis(char color)
{
	//print();
	cout << "test2" << endl;
	for(unsigned int i = 0; i < tiles.size(); i++)
	{
		for(unsigned int j = 0; j < tiles[i].size(); j++)
		{
			if(tiles[i][j].isOwnable() && tiles[i][j].getOwnership() == color)
				tiles[i][j].setVisible(true);
		}
	}
}

void Map::renderFoW(int camX, int camY) const
{
	Sprite fow = fog_of_war;

	for(unsigned int i = 0; i < tiles.size(); i++)
	{
		for(unsigned int j = 0; j < tiles[i].size(); j++)
		{
			int actualX = (j - camX) * TileInfo::TILE_WIDTH;
			int actualY = (i - camY) * TileInfo::TILE_HEIGHT;

			if(!tiles[i][j].isVisible())
			{
				fow.setPos(actualX,actualY);
				fow.setAnimationStartY(FOG*TileInfo::TILE_HEIGHT);
				fow.render();
			}
			else
			{
				int neighborX, neighborY;
				int count = 0;
				bool left = false;
				bool right = false;
				bool up = false;
				bool down = false;

				neighborX = j-1;
				neighborY = i;

				if(neighborX >= 0 && !tiles[neighborY][neighborX].isVisible())
				{
					count++;
					left = true;
				}

				neighborX = j+1;

				if(neighborX < tiles[i].size() && !tiles[neighborY][neighborX].isVisible())
				{
					count++;
					right = true;
				}

				neighborX = j;
				neighborY = i-1;

				if(neighborY >= 0 && !tiles[neighborY][neighborX].isVisible())
				{
					count++;
					up = true;
				}

				neighborY = i+1;

				if(neighborY < tiles.size() && !tiles[neighborY][neighborX].isVisible())
				{
					count++;
					down = true;
				}

				if(count == 4)
				{
					fow.setPos(actualX,actualY);
					fow.setAnimationStartY(FOG_ALL_SIDES*TileInfo::TILE_HEIGHT);
					fow.render();
				}
				else if (count == 3)
				{
					if(!left)
					{
						fow.setPos(actualX,actualY);
						fow.setAnimationStartY(FOG_UP_RIGHT_DOWN*TileInfo::TILE_HEIGHT);
						fow.render();
					}
					else if (!right)
					{
						fow.setPos(actualX,actualY);
						fow.setAnimationStartY(FOG_DOWN_LEFT_UP*TileInfo::TILE_HEIGHT);
						fow.render();
					}
					else if(!up)
					{
						fow.setPos(actualX,actualY);
						fow.setAnimationStartY(FOG_LEFT_DOWN_RIGHT*TileInfo::TILE_HEIGHT);
						fow.render();
					}
					else if(!down)
					{
						fow.setPos(actualX,actualY);
						fow.setAnimationStartY(FOG_LEFT_UP_RIGHT*TileInfo::TILE_HEIGHT);
						fow.render();
					}
				}
				else if (count == 2)
				{
					if(up && right)
					{
						fow.setPos(actualX,actualY);
						fow.setAnimationStartY(FOG_UP_RIGHT*TileInfo::TILE_HEIGHT);
						fow.render();

						neighborX = j-1;
						neighborY = i+1;

						if(neighborY < tiles.size() && neighborX >= 0 && !tiles[neighborY][neighborX].isVisible())
						{
							fow.setPos(actualX,actualY);
							fow.setAnimationStartY(FOG_CORNER_DOWN_LEFT*TileInfo::TILE_HEIGHT);
							fow.render();
						}
					}
					else if(up && left)
					{
						fow.setPos(actualX,actualY);
						fow.setAnimationStartY(FOG_UP_LEFT*TileInfo::TILE_HEIGHT);
						fow.render();

						neighborX = j+1;
						neighborY = i+1;

						if(neighborY < tiles.size() && neighborX < tiles[i].size() && !tiles[neighborY][neighborX].isVisible())
						{
							fow.setPos(actualX,actualY);
							fow.setAnimationStartY(FOG_CORNER_DOWN_RIGHT*TileInfo::TILE_HEIGHT);
							fow.render();
						}
					}
					else if(down && right)
					{
						fow.setPos(actualX,actualY);
						fow.setAnimationStartY(FOG_DOWN_RIGHT*TileInfo::TILE_HEIGHT);
						fow.render();

						neighborX = j-1;
						neighborY = i-1;

						if(neighborY >= 0 && neighborX >= 0 && !tiles[neighborY][neighborX].isVisible())
						{
							fow.setPos(actualX,actualY);
							fow.setAnimationStartY(FOG_CORNER_UP_LEFT*TileInfo::TILE_HEIGHT);
							fow.render();
						}
					}
					else if(down && left)
					{
						fow.setPos(actualX,actualY);
						fow.setAnimationStartY(FOG_DOWN_LEFT*TileInfo::TILE_HEIGHT);
						fow.render();

						neighborX = j+1;
						neighborY = i-1;

						if(neighborY >= 0 && neighborX < tiles[i].size() && !tiles[neighborY][neighborX].isVisible())
						{
							fow.setPos(actualX,actualY);
							fow.setAnimationStartY(FOG_CORNER_UP_RIGHT*TileInfo::TILE_HEIGHT);
							fow.render();
						}
					}
					else if(up && down)
					{
						fow.setPos(actualX,actualY);
						fow.setAnimationStartY(FOG_UP*TileInfo::TILE_HEIGHT);
						fow.render();

						fow.setPos(actualX,actualY);
						fow.setAnimationStartY(FOG_DOWN*TileInfo::TILE_HEIGHT);
						fow.render();
					}
					else if (left && right)
					{
						fow.setPos(actualX,actualY);
						fow.setAnimationStartY(FOG_LEFT*TileInfo::TILE_HEIGHT);
						fow.render();

						fow.setPos(actualX,actualY);
						fow.setAnimationStartY(FOG_RIGHT*TileInfo::TILE_HEIGHT);
						fow.render();
					}
				}
				else if (count == 1)
				{
					if(left)
					{
						fow.setPos(actualX,actualY);
						fow.setAnimationStartY(FOG_LEFT*TileInfo::TILE_HEIGHT);
						fow.render();

						neighborX = j+1;
						neighborY = i-1;

						if(neighborY >= 0 && neighborX < tiles[i].size() && !tiles[neighborY][neighborX].isVisible())
						{
							fow.setPos(actualX,actualY);
							fow.setAnimationStartY(FOG_CORNER_UP_RIGHT*TileInfo::TILE_HEIGHT);
							fow.render();
						}
						neighborX = j+1;
						neighborY = i+1;

						if(neighborY < tiles.size() && neighborX < tiles[i].size() && !tiles[neighborY][neighborX].isVisible())
						{
							fow.setPos(actualX,actualY);
							fow.setAnimationStartY(FOG_CORNER_DOWN_RIGHT*TileInfo::TILE_HEIGHT);
							fow.render();
						}
					}
					else if (up)
					{
						fow.setPos(actualX,actualY);
						fow.setAnimationStartY(FOG_UP*TileInfo::TILE_HEIGHT);
						fow.render();

						neighborX = j+1;
						neighborY = i+1;

						if(neighborY < tiles.size() && neighborX < tiles[i].size() && !tiles[neighborY][neighborX].isVisible())
						{
							fow.setPos(actualX,actualY);
							fow.setAnimationStartY(FOG_CORNER_DOWN_RIGHT*TileInfo::TILE_HEIGHT);
							fow.render();
						}

						neighborX = j-1;
						neighborY = i+1;

						if(neighborY < tiles.size() && neighborX >= 0 && !tiles[neighborY][neighborX].isVisible())
						{
							fow.setPos(actualX,actualY);
							fow.setAnimationStartY(FOG_CORNER_DOWN_LEFT*TileInfo::TILE_HEIGHT);
							fow.render();
						}

					}
					else if(down)
					{
						fow.setPos(actualX,actualY);
						fow.setAnimationStartY(FOG_DOWN*TileInfo::TILE_HEIGHT);
						fow.render();

						neighborX = j+1;
						neighborY = i-1;

						if(neighborY >= 0 && neighborX < tiles[i].size() && !tiles[neighborY][neighborX].isVisible())
						{
							fow.setPos(actualX,actualY);
							fow.setAnimationStartY(FOG_CORNER_UP_RIGHT*TileInfo::TILE_HEIGHT);
							fow.render();
						}

						neighborX = j-1;
						neighborY = i-1;

						if(neighborY >= 0 && neighborX >= 0 && !tiles[neighborY][neighborX].isVisible())
						{
							fow.setPos(actualX,actualY);
							fow.setAnimationStartY(FOG_CORNER_UP_LEFT*TileInfo::TILE_HEIGHT);
							fow.render();
						}

					}
					else if (right)
					{
						fow.setPos(actualX,actualY);
						fow.setAnimationStartY(FOG_RIGHT*TileInfo::TILE_HEIGHT);
						fow.render();

						neighborX = j-1;
						neighborY = i-1;

						if(neighborY >= 0 && neighborX >= 0 && !tiles[neighborY][neighborX].isVisible())
						{
							fow.setPos(actualX,actualY);
							fow.setAnimationStartY(FOG_CORNER_UP_LEFT*TileInfo::TILE_HEIGHT);
							fow.render();
						}

						neighborX = j-1;
						neighborY = i+1;

						if(neighborY < tiles.size() && neighborX >= 0 && !tiles[neighborY][neighborX].isVisible())
						{
							fow.setPos(actualX,actualY);
							fow.setAnimationStartY(FOG_CORNER_DOWN_LEFT*TileInfo::TILE_HEIGHT);
							fow.render();
						}
					}
				}
				else
				{
					neighborX = j-1;
					neighborY = i+1;

					if(neighborY < tiles.size() && neighborX >= 0 && !tiles[neighborY][neighborX].isVisible())
					{
						fow.setPos(actualX,actualY);
						fow.setAnimationStartY(FOG_CORNER_DOWN_LEFT*TileInfo::TILE_HEIGHT);
						fow.render();
					}

					neighborX = j-1;
					neighborY = i-1;

					if(neighborY >= 0 && neighborX >= 0 && !tiles[neighborY][neighborX].isVisible())
					{
						fow.setPos(actualX,actualY);
						fow.setAnimationStartY(FOG_CORNER_UP_LEFT*TileInfo::TILE_HEIGHT);
						fow.render();
					}

					neighborX = j+1;
					neighborY = i-1;

					if(neighborY >= 0 && neighborX < tiles[i].size() && !tiles[neighborY][neighborX].isVisible())
					{
						fow.setPos(actualX,actualY);
						fow.setAnimationStartY(FOG_CORNER_UP_RIGHT*TileInfo::TILE_HEIGHT);
						fow.render();
					}

					neighborX = j+1;
					neighborY = i+1;

					if(neighborY < tiles.size() && neighborX < tiles[i].size() && !tiles[neighborY][neighborX].isVisible())
					{
						fow.setPos(actualX,actualY);
						fow.setAnimationStartY(FOG_CORNER_DOWN_RIGHT*TileInfo::TILE_HEIGHT);
						fow.render();
					}
				}
			}
		}
	}
}

void Map::updateMapImages(vector<ALLEGRO_BITMAP*> newtiles, ALLEGRO_BITMAP* fog)
{
	/*for(unsigned int i = 0; i < tiles.size(); i++)
	{
		for(unsigned int j = 0; j < tiles[i].size(); j++)
		{
			tiles[i][j].setSprite(newtiles);
		}
	}`*/
	for(unsigned int i = 0; i < tileInfo.size(); i++)
	{
		tileInfo[i].setSprites(newtiles);
	}

	fog_of_war.setImage(fog);
}

void Map::initializeTiles(vector <Player> playersInGame)
{
	for(unsigned int y = 0; y < tiles.size(); y++)
	{
		for(unsigned int x = 0; x < tiles[y].size(); x++)
		{
			if(tiles[y][x].isOwnable())
			{
				bool playerExists = false;

				for(unsigned int i = 0; i < playersInGame.size(); i++)
				{
					if(playersInGame[i].getColor() == tiles[y][x].getOwnership())
						playerExists = true;
				}

				if(!playerExists)
					tiles[y][x].setOwnership('w');
			}
		}
	}
}

void Map::gainResources(Player* activePlayer)
{
	for(unsigned int y = 0; y < tiles.size(); y++)
	{
		for(unsigned int x = 0; x < tiles[y].size(); x++)
		{
			if(tiles[y][x].isOwnable() && tiles[y][x].getOwnership() == activePlayer->getColor())
			{
				activePlayer->addResources(tiles[y][x].getResourceBonus());
			}
		}
	}
}

void Map::highlightMovementNodes(Unit* unit)
{
	for(unsigned int y = 0; y < tiles.size(); y++)
	{
		for(unsigned int x = 0; x < tiles[y].size(); x++)
		{
			tiles[y][x].setDist(INT_MAX);
			tiles[y][x].setParent(NULL);
		}
	}

	Tile* source = unit->getTile();

	source->setDist(0);
	
	vector <Tile*> q;

	for(unsigned int y = 0; y < tiles.size(); y++)
	{
		for(unsigned int x = 0; x < tiles[y].size(); x++)
			q.push_back(&tiles[x][y]);
		//q.insert(q.end(), tiles[y].begin(), tiles[y].end());
	}

	int count = 0;
	while(!q.empty())
	{
		count++;
		Tile* u = NULL;
		int index;

		for(unsigned int i = 0; i < q.size(); i++)
		{
			if(u == NULL)
			{
				u = q[i];
				index = i;
			}
			else if (q[i]->getDist() < u->getDist())
			{
				u = q[i];
				index = i;
			}
		}

		q.erase(q.begin()+index);

		if(u->getDist() == INT_MAX)
			break;


		//checks node on top
		int neighborX = u->getX();
		int neighborY = u->getY()-1;
		Tile* neighbor;

		if(neighborY >= 0 && (unsigned) neighborY < tiles.size())
		{	
			neighbor =  &tiles[neighborY][neighborX];

			int alt = u->getDist() + neighbor->getMoveCost(unit->getType());

			if(neighbor->isTraversableBy(unit) && (!neighbor->isVisible() ||  !neighbor->isOccupiedByEnemy(unit)))
			{
				if(alt < neighbor->getDist())
				{
					neighbor->setDist(alt);
					neighbor->setParent(u);
				}
			}
		}

		//checks node on bottom
		neighborX = u->getX();
		neighborY = u->getY()+1;
		neighbor;

		if(neighborY >= 0 && (unsigned)neighborY < tiles.size())
		{	
			neighbor =  &tiles[neighborY][neighborX];

			int alt = u->getDist() + neighbor->getMoveCost(unit->getType());

			if(neighbor->isTraversableBy(unit) && (!neighbor->isVisible() ||  !neighbor->isOccupiedByEnemy(unit)))
			{
				if(alt < neighbor->getDist())
				{
					neighbor->setDist(alt);
					neighbor->setParent(u);
				}
			}
		}

		//checks node on left
		neighborX = u->getX()-1;
		neighborY = u->getY();
		neighbor;

		if(neighborX >= 0 && (unsigned) neighborX < tiles[neighborY].size())
		{	
			neighbor =  &tiles[neighborY][neighborX];

			int alt = u->getDist() + neighbor->getMoveCost(unit->getType());

			if(neighbor->isTraversableBy(unit) && (!neighbor->isVisible() ||  !neighbor->isOccupiedByEnemy(unit)))
			{
				if(alt < neighbor->getDist())
				{
					neighbor->setDist(alt);
					neighbor->setParent(u);
				}
			}
		}

		//checks node on right
		neighborX = u->getX()+1;
		neighborY = u->getY();
		neighbor;

		if(neighborX >= 0 && (unsigned) neighborX < tiles[neighborY].size())
		{	
			neighbor =  &tiles[neighborY][neighborX];

			int alt = u->getDist() + neighbor->getMoveCost(unit->getType());

			if(neighbor->isTraversableBy(unit) && (!neighbor->isVisible() ||  !neighbor->isOccupiedByEnemy(unit)))
			{
				if(alt < neighbor->getDist())
				{
					neighbor->setDist(alt);
					neighbor->setParent(u);
				}
			}
		}
	}

	for(unsigned int y = 0; y < tiles.size(); y++)
	{
		for(unsigned int x = 0; x < tiles[y].size(); x++)
		{
			if(tiles[y][x].getDist() <= unit->getMovePoints())
			{
				
				tiles[y][x].setHW(true);
			}
		}
	}
}

void Map::highlightRangeNodes(Unit* unit)
{

	for(unsigned int y = 0; y < tiles.size(); y++)
	{
		for(unsigned int x = 0; x < tiles[y].size(); x++)
		{
			if(unit->isInMeleeRange(unit->getTile(),&tiles[y][x]) || unit->isInRange(unit->getTile(),&tiles[y][x]))
			{
				tiles[y][x].setHR(true);
			}
		}
	}

	unit->getTile()->setHR(false);
}

void Map::resetRangeNodes()
{
	for(unsigned int y = 0; y < tiles.size(); y++)
	{
		for(unsigned int x = 0; x < tiles[y].size(); x++)
		{
			tiles[y][x].setHR(false);
		}
	}
}

void Map::resetMovementNodes()
{
	for(unsigned int y = 0; y < tiles.size(); y++)
	{
		for(unsigned int x = 0; x < tiles[y].size(); x++)
		{
			tiles[y][x].setHW(false);
		}
	}
}

void Map::renderHW(int camX, int camY) const
{
	for(unsigned int y = 0; y < tiles.size(); y++)
	{
		for(unsigned int x = 0; x < tiles[y].size(); x++)
		{
			if(tiles[y][x].isHW())
			{
				int actualX = (x - camX) * TileInfo::TILE_WIDTH;
				int actualY = (y - camY) * TileInfo::TILE_HEIGHT;

				al_draw_filled_rectangle(actualX,actualY,actualX+TileInfo::TILE_WIDTH, actualY+TileInfo::TILE_HEIGHT,TILE_HIGHLIGHT_WHITE);
			}
		}
	}
}

void Map::renderHR(int camX, int camY) const
{
	for(unsigned int y = 0; y < tiles.size(); y++)
	{
		for(unsigned int x = 0; x < tiles[y].size(); x++)
		{
			if(tiles[y][x].isHR())
			{
				int actualX = (x - camX) * TileInfo::TILE_WIDTH;
				int actualY = (y - camY) * TileInfo::TILE_HEIGHT;

				al_draw_filled_rectangle(actualX,actualY,actualX+TileInfo::TILE_WIDTH, actualY+TileInfo::TILE_HEIGHT,TILE_HIGHLIGHT_RED);
			}
		}
	}
}

vector <Tile*> Map::getShortestPath(Tile* dest)
{
	int destY = dest->getY();
	int destX = dest->getX();

	vector<Tile*> shortestPath;

	if(destY < 0 || (unsigned) destY >= tiles.size())
		return shortestPath;

	if(destX < 0 || (unsigned) destY >= tiles[destY].size())
		return shortestPath;

	Tile* tile = dest;
	shortestPath.push_back(tile);

	while(tile->getParent() != NULL)
	{
		tile = tile->getParent();
		shortestPath.push_back(tile);
	}

	reverse(shortestPath.begin(), shortestPath.end());

	return shortestPath;
}

void Map::highlightRangeNode(int x, int y)
{
	if(x >= 0 && y >= 0)
	{
		if(x < sizeX && y < sizeY)
		{
			tiles[y][x].setHR(true);
		}
	}
}

/*"\\BattleScenarios\\"*/
//TODO Change this to be platform independent
vector<Map> Map::loadMapsFromFolder(string folder)
{
	vector<Map> maps;
	map<int,TileInfo> tileInfo;

	//char* currDir = al_get_current_directory();

	//if(currDir)
	//{
		//string path = currDir;
		//al_free(currDir);

		ALLEGRO_FS_ENTRY * dataDir = al_create_fs_entry("data");
		bool success = al_open_directory(dataDir);

		if (!success)
		{
			throw errorOpeningDirectory(al_get_fs_entry_name(dataDir));
		}
		
		ALLEGRO_FS_ENTRY * tileInfoFile;
		tileInfoFile = al_read_directory(dataDir);

		while(tileInfoFile != NULL)
		{
			ALLEGRO_PATH * filePath = al_create_path(al_get_fs_entry_name(tileInfoFile));
			if (al_get_fs_entry_mode(tileInfoFile) & ALLEGRO_FILEMODE_ISFILE)
				if	(strcmp(al_get_path_filename(filePath),"tiles.xml") == 0)
				{
					try 
					{
						tileInfo = loadTileInfo(al_get_fs_entry_name(tileInfoFile));
					} 
					catch (gameException & e)
					{
						e.getInfo();
					}
				}

			al_destroy_fs_entry(tileInfoFile);
			tileInfoFile = al_read_directory(dataDir);
		}
		al_close_directory(dataDir);
		al_destroy_fs_entry(dataDir);

		string mapDir = "data\\BattleScenarios\\";
		//path.append(folder);
		ALLEGRO_FS_ENTRY * dir = al_create_fs_entry(mapDir.c_str());
		success = al_open_directory(dir);

		if (!success)
		{
			throw errorOpeningDirectory(mapDir);
		}
		
		ALLEGRO_FS_ENTRY * file;
		file = al_read_directory(dir);

		while(file != NULL)
		{
			ALLEGRO_PATH * filePath = al_create_path(al_get_fs_entry_name(file));
			if (al_get_fs_entry_mode(file)&ALLEGRO_FILEMODE_ISFILE)
				if	(strcmp(al_get_path_extension(filePath),".scen") == 0)
				{
					try 
					{
						maps.push_back(Map::loadMap(al_get_fs_entry_name(file), tileInfo));
					} 
					catch (gameException & e)
					{
						e.getInfo();
					}
				}

			al_destroy_fs_entry(file);
			file = al_read_directory(dir);
		}
		al_close_directory(dir);
		al_destroy_fs_entry(dir);

		/*WIN32_FIND_DATA FindData;
		HANDLE hFind;

		hFind = FindFirstFile( fullSearchPath.c_str(), &FindData );

		if( hFind == INVALID_HANDLE_VALUE )
		{
		return empty;
		}

		do
		{
		string filePath = path + FindData.cFileName;

		try 
		{
		maps.push_back(Map::loadMap(filePath));
		} 
		catch (gameException e)
		{
		e.getInfo();
		}
		}
		while( FindNextFile(hFind, &FindData) > 0 );

		if( GetLastError() != ERROR_NO_MORE_FILES )
		{
		throw errorLoadingMaps();
		}*/
	//}

	return maps;
}

Map Map::loadMap(string filename, map<int,TileInfo> tileInfo)
{
	ifstream in( filename.c_str() );
	if( in )
	{
		Map tmp;
		string name;

		pugi::xml_document mapDoc;
		pugi::xml_parse_result result = mapDoc.load_file(filename.c_str());

		if(result)
		{
			pugi::xml_node info = mapDoc.child("scenario").first_child();

			name = info.text().as_string();
			info = info.next_sibling();
			info = info.next_sibling();
			string terrainInfoSrc = info.attribute("terrain").value();

			tmp = Map(name, tileInfo);

			tmp.loadTerrainInfo(terrainInfoSrc);

			return tmp;
		} else throw errorOpeningMap(filename);

		/*while(getline(in, line))
		{
			if(!receivingTiles)
			{
				string tag = line.substr(0,line.find_first_of('=',0));
				string info = line.substr(line.find_first_of('=',0)+1,line.npos);

				if(tag == "name")
				{
					name = info;
				}
				else if (tag == "map")
				{
					receivingTiles = true;
					y = 0;
				}
			}
			else
			{
				vector <Tile*> tileLine;

				for(unsigned int i = 0; i < line.size(); i++)
				{
					Tile tmpTile = loadTile(line[i],i,y);

					if(tmpTile != NULL)
					{
						if(tmpTile->getOwnership() == 'r')
							playersAllowed[0] = true;
						else if (tmpTile->getOwnership() == 'b')
							playersAllowed[1] = true;
						else if (tmpTile->getOwnership() == 'g')
							playersAllowed[2] = true;
						else if (tmpTile->getOwnership() == 'y')
							playersAllowed[3] = true;
					}

					tileLine.push_back(tmpTile);
				}

				tiles.push_back(tileLine);

				tileLine.clear();

				y++;
			}
		}*/
	}
	else throw errorOpeningMap(filename);
}

Tile Map::loadTile( ALLEGRO_COLOR color, int x, int y )
{
	
	int tileID = color.r*255;
	int tileTypeID = color.g*255;
	TileInfo* info;
	
	map<int,TileInfo>::iterator it = tileInfo.find(tileID);
	if( it!=tileInfo.end())
		info = &(it->second);
	else
		throw errorLoadingTile();

	return Tile(x,y,info,tileTypeID);
}

void Map::loadTerrainInfo( string terrainInfoSrc)
{
	ALLEGRO_BITMAP* terrainInfo = al_load_bitmap(terrainInfoSrc.c_str());
	int bmapWidth = al_get_bitmap_width(terrainInfo);
	int bmapHeight = al_get_bitmap_height(terrainInfo);

	for(unsigned int x = 0; x < bmapWidth; x++)
	{
		vector <Tile> tileLine;
		for(unsigned int y = 0; y < bmapHeight; y++)
		{
			try {

				Tile tmpTile = loadTile(al_get_pixel(terrainInfo,x,y),x,y);
				 tileLine.push_back(tmpTile);

				if(tmpTile.getOwnership() == 'r')
				{
					playersAllowed[0] = true;
				}
				else if (tmpTile.getOwnership() == 'b')
					playersAllowed[1] = true;
				else if (tmpTile.getOwnership() == 'g')
					playersAllowed[2] = true;
				else if (tmpTile.getOwnership() == 'y')
					playersAllowed[3] = true;
			}
			catch (gameException & e)
			{
				throw errorLoadingTerrain(terrainInfoSrc);
			}
		}

		tiles.push_back(tileLine);
	}

	sizeX = tiles.size();
	sizeY = tiles[0].size();
}

map<int, TileInfo> Map::loadTileInfo( string filename )
{
	pugi::xml_document mapDoc;
	pugi::xml_parse_result result = mapDoc.load_file(filename.c_str());

	if(result)
	{
		pugi::xml_node tile = mapDoc.child("tiles").child("tileInfo");

		map <int, TileInfo> rtrnVal;

		while(tile)
		{
			map<int,TileType> tileTypes;
			map<int,vector<Sprite>> infoRepresentations;

			int id = tile.attribute("id").as_int();
			float defense = tile.attribute("defense").as_float();
			string name = tile.attribute("name").value();
			int spriteIndex = tile.attribute("imgSrc").as_int();
			int bgIndex = tile.attribute("battleBackground").as_int();
			bool captureable = tile.attribute("captureable").as_bool();
			bool hideout = tile.attribute("hideout").as_bool();
			int moveCostLandLight = tile.attribute("moveCostLandLight").as_int();
			int moveCostLandHeavy = tile.attribute("moveCostLandHeavy").as_int();
			int moveCostWater = tile.attribute("moveCostWater").as_int();

			int maxLoyalty;
			if(tile.attribute("loyalty"))
				maxLoyalty = tile.attribute("loyalty").as_int();
			else
				maxLoyalty = 0;

			int resBonus;
			if(tile.attribute("resBonus"))
				resBonus = tile.attribute("resBonus").as_int();
			else
				resBonus = 0;

			Sprite battleBG = Sprite(NULL, 0, 0, 1, WIDTH, HEIGHT);

			pugi::xml_node info = tile.child("info");

			if(!info)
				throw errorLoadingTileInfo();

			pugi::xml_node representation = info.child("representation");
			if(!representation)
				throw errorLoadingTileInfo();

			while(representation)
			{
				vector<Sprite> sprites;
				int infoRepID = representation.attribute("id").as_int();
				
				pugi::xml_node spriteNode = representation.child("sprite");
				if(!spriteNode)
					throw errorLoadingTileInfo();

				while(spriteNode)
				{
					int sx = spriteNode.attribute("sx").as_int();
					int sy = spriteNode.attribute("sy").as_int();
					int dx = spriteNode.attribute("dx").as_int();
					int dy = spriteNode.attribute("dy").as_int();
					int frameNum = spriteNode.attribute("frameNum").as_int();
					int animDelay = spriteNode.attribute("animDelay").as_int();

					Sprite tmpSprite = Sprite(NULL, dx, dy, frameNum, TileInfo::TILE_WIDTH, TileInfo::TILE_HEIGHT, animDelay, sx, sy);
					sprites.push_back(tmpSprite);

					spriteNode = spriteNode.next_sibling();
				}

				infoRepresentations.insert(pair<int,vector<Sprite>>(infoRepID, sprites));
				representation = representation.next_sibling();
			}

			pugi::xml_node type = tile.child("type");
			if(!type)
				throw errorLoadingTileInfo();
			
			while(type)
			{
				vector<Sprite> sprites;
				ALLEGRO_COLOR minimapColor;

				int typeID = type.attribute("id").as_int();
				int infoID = type.attribute("infoID").as_int();
				
				char color;
				
				if(type.attribute("player"))
					color = type.attribute("player").as_string()[0];
				else
					color = 'w';
				
				string colorVals;
				colorVals = type.attribute("minimapColor").value();
				int r, g, b;
				if(sscanf(colorVals.c_str(),"%d %d %d",&r,&g,&b) == 3)
					minimapColor = al_map_rgb(r,g,b);
				else
					throw errorLoadingTileInfo();

				pugi::xml_node spriteNode = type.child("sprite");
				if(!spriteNode)
					throw errorLoadingTileInfo();

				while(spriteNode)
				{
					int sx = spriteNode.attribute("sx").as_int();
					int sy = spriteNode.attribute("sy").as_int();
					int dx = spriteNode.attribute("dx").as_int();
					int dy = spriteNode.attribute("dy").as_int();
					int frameNum = spriteNode.attribute("frameNum").as_int();
					int animDelay = spriteNode.attribute("animDelay").as_int();

					Sprite tmpSprite = Sprite(NULL, dx, dy, frameNum, TileInfo::TILE_WIDTH, TileInfo::TILE_HEIGHT, animDelay, sx, sy);
					sprites.push_back(tmpSprite);

					spriteNode = spriteNode.next_sibling();
				}

				TileType tmpType;
				tmpType.color = color;
				tmpType.minimapColor = minimapColor;
				tmpType.id = typeID;
				tmpType.infoSprites = infoRepresentations[infoID];
				tmpType.sprites = sprites;

				tileTypes.insert(pair<int,TileType>(typeID,tmpType));
				type = type.next_sibling("type");
			}

			rtrnVal.insert(pair<int,TileInfo>(id,TileInfo(id,name,moveCostLandLight,moveCostLandHeavy,moveCostWater,resBonus,maxLoyalty,defense,captureable,hideout,tileTypes,spriteIndex,bgIndex)));
			tile = tile.next_sibling();
		}

		return rtrnVal;

	} else throw errorLoadingTileInfo();
}

void Map::print() const
{
	for(unsigned int i = 0; i < 1; i++)
	{
		for(unsigned int j = 0; j < tiles[i].size(); j++)
		{
			cout << "Tile at " << i << ", " << j << " is a "; cout << tiles[i][j].getName() << endl;
		}
	}

	cout << "Number of tiles is " << tileInfo.size() << endl;
	for(map<int,TileInfo>::const_iterator i = tileInfo.begin(); i != tileInfo.end(); i++)
		cout << "Tile is " << i->second.getName() << endl;
}
