#include "ScenarioPauseMenu.h"
#include "game.h"
#include "ScenarioScreen.h"
#include "scenario.h"


ScenarioPauseMenu::ScenarioPauseMenu(ScenarioScreen* screen, Scenario* scenario, ALLEGRO_FONT* unselected, ALLEGRO_FONT* selected) : Menu(false, false, ScenarioPauseMenu::ScenarioPauseMenuDefaultSelectedOption)
{
	this->screen = screen;
	this->scenario = scenario;

	exitMenu = new ScenarioExitMenu(screen, this, unselected, selected);

	options.push_back(new Option(SCENARIO_PAUSE_MENU_OPTION1, false));
	options.push_back(new Option(SCENARIO_PAUSE_MENU_OPTION2, false));
	options.push_back(new Option(SCENARIO_PAUSE_MENU_OPTION3, true));
	options.push_back(new Option(SCENARIO_PAUSE_MENU_OPTION4, true));

	fonts.push_back(unselected);
	fonts.push_back(selected);
}

void ScenarioPauseMenu::update(bool* keys) 
{
	scenario->updateTileInfo();
	exitMenu->update(keys);
	
	if(active)
	{
		if(keys[DOWN])
		{
			selectedOption = getNextActive(DIR_DOWN);

			keys[DOWN] = false;
		}
		else if(keys[UP])
		{
			selectedOption = getNextActive(DIR_UP);

			keys[UP] = false;
		}
		else if(keys[A] || keys[ENTER])
		{
			if(selectedOption == EXIT)
			{
				keys[A] = false;
				keys[B] = false;
				keys[ESC] = false;
				keys[ENTER] = false;
				keys[UP] = false;
				keys[DOWN] = false;
				keys[RIGHT] = false;
				keys[LEFT] = false;

				this->active = false;
				this->draw = false;
				exitMenu->setActive(true);
				exitMenu->setDrawing(true);
			}
			else if (selectedOption == NEXT_TURN)
			{
				keys[A] = false;
				keys[B] = false;
				keys[ESC] = false;
				keys[ENTER] = false;
				keys[UP] = false;
				keys[DOWN] = false;
				keys[RIGHT] = false;
				keys[LEFT] = false;

				this->active = false;
				this->draw = false;
				scenario->passTurn();
				scenario->changeState(Scenario::PRETURN);
			}
		}
		else if (keys[B] || keys[ESC])
		{
			keys[A] = false;
			keys[B] = false;
			keys[ESC] = false;
			keys[ENTER] = false;
			keys[UP] = false;
			keys[DOWN] = false;
			keys[RIGHT] = false;
			keys[LEFT] = false;

			this->active = false;
			this->draw = false;
			scenario->changeState(Scenario::IDLE);
		}
	}
}

void ScenarioPauseMenu::render() const
{
	if(draw)
	{
		al_draw_filled_rectangle(MENU_LEFT_LIMIT,MENU_UPPER_LIMIT, MENU_RIGHT_LIMIT, MENU_LOWER_LIMIT, SCENARIO_PAUSE_MENU_BACK_COLOR);

		for(unsigned int i = 0; i < options.size(); i++)
		{
			if(i == selectedOption)
			{
				if(options[i]->active)
					al_draw_text(fonts[SELECTED], SCENARIO_PAUSE_MENU_TEXT_COLOR, TEXT_X, TEXT_INIT_SELECTED_Y + i*TEXT_SELECTED_SPACING, ALLEGRO_ALIGN_CENTRE, options[i]->Name.c_str());
				else
					al_draw_text(fonts[SELECTED], SCENARIO_PAUSE_MENU_BLOCKED_COLOR, TEXT_X, TEXT_INIT_SELECTED_Y + i*TEXT_SELECTED_SPACING, ALLEGRO_ALIGN_CENTRE, options[i]->Name.c_str());
			}
			else
			{
				if(options[i]->active)
					al_draw_text(fonts[UNSELECTED], SCENARIO_PAUSE_MENU_TEXT_COLOR, TEXT_X, TEXT_INIT_UNSELECTED_Y + i*TEXT_UNSELECTED_SPACING, ALLEGRO_ALIGN_CENTRE, options[i]->Name.c_str());
				else
					al_draw_text(fonts[UNSELECTED], SCENARIO_PAUSE_MENU_BLOCKED_COLOR, TEXT_X, TEXT_INIT_UNSELECTED_Y + i*TEXT_UNSELECTED_SPACING, ALLEGRO_ALIGN_CENTRE, options[i]->Name.c_str());
			}
		}
	}

	exitMenu->render();
}

ScenarioPauseMenu::~ScenarioPauseMenu()
{
	delete exitMenu;
}