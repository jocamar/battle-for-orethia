#include "Sprite.h"
#include <iostream>

Sprite::Sprite (ALLEGRO_BITMAP* image, int x, int y, int frameNum, int frameWidth, int frameHeight, int frameDelay, int animationStartX, int animationStartY, ALLEGRO_COLOR tint)
{
	this->tint = tint;
	this->x = x;
	this->y = y;
	this->image = image;
	this->frameNum = frameNum;
	this->frameWidth = frameWidth;
	this->frameHeight = frameHeight;
	this->frameDelay = frameDelay;
	this->currFrame = 0;
	this->counter = 0;
	this->animationStartX = animationStartX;
	this->animationStartY = animationStartY;
}

void Sprite::render(int draw_flags) const
{
	al_draw_tinted_bitmap_region(image,tint,animationStartX+(currFrame*frameWidth),animationStartY,frameWidth,frameHeight,x,y,draw_flags);
}

void Sprite::update()
{
	if(counter >= frameDelay)
	{
		counter = 0;

		currFrame++;

		if(currFrame >= frameNum)
			currFrame = 0;
	}
	else
		counter++;
}

void Sprite::setAnimationStartX(int newX)
{
	this->animationStartX = newX;
	this->currFrame = 0;
}

void Sprite::setAnimationStartY(int newY)
{
	this->animationStartY = newY;
	this->currFrame = 0;
}

void Sprite::setTint(ALLEGRO_COLOR newTint)
{
	this->tint = newTint;
}

int Sprite::getAnimationStartX() const
{
	return animationStartX;
}

int Sprite::getAnimationStartY() const
{
	return animationStartY;
}


int Sprite::getCurrFrame() const
{
	return currFrame;
}

void Sprite::restartAnimation()
{
	currFrame = 0;
	counter = 0;
}

bool Sprite::finishedAnimation() const
{
	if(currFrame >= frameNum-1)
		return true;
	else
		return false;
}

void Sprite::setPos(int x, int y)
{
	this->x = x;
	this->y = y;
}

void Sprite::setImage(ALLEGRO_BITMAP* newImage)
{
	this->image = newImage;
}

ALLEGRO_BITMAP* Sprite::getImage() const
{
	return image;
}

Sprite::~Sprite()
{
}

void Sprite::setAnimationDelay(int newDelay)
{
	frameDelay = newDelay;
}

int Sprite::getX() const
{
	return x;
}

int Sprite::getY() const
{
	return y;
}

void Sprite::setFrameNum(int newFrameNum)
{
	this->frameNum = newFrameNum;
}