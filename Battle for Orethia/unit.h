#ifndef UNIT_H
#define UNIT_H

#include <vector>
#include "tile.h"
#include "allegro5/allegro5.h"
#include "allegro5/allegro_font.h"

using namespace std;

#define UNIT_MOVE_SPEED 10
#define UNIT_ANIMATION_FRAMES 4
#define CAP_ICON_WIDTH 21
#define HP_TEXT_COLOR			al_map_rgb(255,255,255)
#define MELEE_RANGE 1

class Unit
{
	enum Animations {RED_X = 0, IDLE_RED_Y = 0, BLUE_X = 4, IDLE_BLUE_Y = 0, GREEN_X = 8, IDLE_GREEN_Y = 0, YELLOW_X = 12, IDLE_YELLOW_Y = 0,
					 MOVING_SIDE_RED_Y = 1, MOVING_SIDE_BLUE_Y = 1, MOVING_SIDE_GREEN_Y = 1, MOVING_SIDE_YELLOW_Y = 1,
					 MOVING_UP_RED_Y = 2, MOVING_UP_BLUE_Y = 2, MOVING_UP_GREEN_Y = 2, MOVING_UP_YELLOW_Y = 2,
					 MOVING_DOWN_RED_Y = 3, MOVING_DOWN_BLUE_Y = 3, MOVING_DOWN_GREEN_Y = 3, MOVING_DOWN_YELLOW_Y = 3,
					 AIMING_RED_Y = 4, AIMING_BLUE_Y = 4, AIMING_GREEN_Y = 4, AIMING_YELLOW_Y = 4,
					 FIRING_RED_Y = 5, FIRING_BLUE_Y = 5, FIRING_GREEN_Y = 5, FIRING_YELLOW_Y = 5,
					 STABBING_RED_Y = 6, STABBING_BLUE_Y = 6, STABBING_GREEN_Y = 6, STABBING_YELLOW_Y = 6,
					 DYING_RED_Y = 7, DYING_BLUE_Y = 7, DYING_GREEN_Y = 7, DYING_YELLOW_Y = 7,
					 SHADOW_IDLE_X = 0, SHADOW_IDLE_Y = 8};
	enum Icons {CAP_RED, CAP_BLUE, CAP_GREEN, CAP_YELLOW, LIFE_ICON_RED, LIFE_ICON_BLUE, LIFE_ICON_GREEN, LIFE_ICON_YELLOW};
	enum IconCoords { CAP_X = 15, CAP_Y = 50, HP_X = 50, HP_Y = 50, HP_TEXT_X = 10, HP_TEXT_Y = 1};

	double hp;
	int maxHP;
	int ammo, maxAmmo;
	int cost;
	int attack;
	int x, y;
	int spriteX, spriteY;
	int screenX, screenY;
	int movePoints, maxRange, minRange, visibility;
	int state;
	int type, moveType;
	int currPathTile;
	char color;
	bool canCap, isCapturing;
	bool turnedLeft;

	string name;
	Tile* currTile;
	Sprite sprite, shadow, captureIcon, lifeIcon, infoSprite;
	ALLEGRO_FONT* hp_font;
	vector <bool> unitsAlive;
	vector <Tile* > path;

public:
	enum UnitMoveTypes {UNIT_LAND_LIGHT, UNIT_LAND_HEAVY, UNIT_WATER};
	enum States{IDLE, MOVING, AT_DESTINATION, RESTING};
	enum Costs{INFANTRY_COST = 1000};

	Unit(int x, int y, Tile* tile, ALLEGRO_BITMAP* sprite, char color, ALLEGRO_BITMAP* icons, ALLEGRO_FONT* hp_font);

	void changeState(int newState);

	//Getters
	Tile* getTile() const;
	int getX() const;
	int getY() const;
	int getVis() const;
	int getState() const;
	int getCost() const;
	int getMaxAmmo() const;
	int getMaxHp() const;
	int getType() const;
	char getColor() const;
	int getAmmo() const;
	double getHP() const;
	int getConvertedHP() const;
	string getName() const;
	int getUnitMoveType() const;

	//Related to selecting
	void select();
	void deselect();

	//Related to capturing
	void startCapturing();
	void stopCapturing();
	bool canCapture() const;

	//Related to movement
	Tile* isPathObstructed();
	void addTileToPath(Tile* tile, vector<Tile*> shortestPath);
	bool isTileOnPath(Tile* tile) const;
	bool isTileAdjacentToPath(Tile* tile) const;
	bool canTraverse(Tile* tile);
	void setEndMovementAnimation();
	void moveSpriteTo(int x, int y);
	void startMovingRight();
	void startMovingLeft();
	void startMovingUp();
	void startMovingDown();
	void setPath(vector <Tile*> newPath);
	void setPos(int x, int y, Tile* tile);
	int getMovePoints() const;

	//Related to combat
	bool isInRange(Tile* tile, Tile* target) const;
	bool isInMeleeRange(Tile* tile, Tile* target) const;
	bool canAttackRange(Unit* unit, Tile* tile) const;
	bool canAttackMelee(Unit* unit, Tile* tile) const;
	bool isEnemy(Unit* unit) const;

	//Rendering functions
	void renderHP();
	void renderPath(ALLEGRO_BITMAP* arrows, int camX, int camY) const;
	void render();
	void renderInfoSprite() const;
	void setTint(ALLEGRO_COLOR tint);

	//Update Functions
	void update(Scenario* scenario);
	void updateSprite(int camX, int camY);
	void updateInfoSprite(int x, int y);
};


#endif