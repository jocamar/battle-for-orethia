#ifndef BATTLEPLAYERMENU_H
#define BATTLEPLAYERMENU_H

#include "menu.h"
#include "allegro5/allegro5.h"
#include "allegro5/allegro_primitives.h"
#include "allegro5/allegro_font.h"

#define BATTLE_PLAYER_MENU_TEXT_COLOR				al_map_rgb(255,255,255) //white
#define BATTLE_PLAYER_MENU_BLOCKED_COLOR			al_map_rgb(150,150,150)  //grey
#define BATTLE_PLAYER_MENU_CONTOUR_COLOR			al_map_rgb(255,255,255) //white
#define BATTLE_PLAYER_MENU_OVERLAY_COLOR			al_map_rgba( 255*0.1f, 255*0.1f, 255*0.1f, 0.1f ) //transparent white

#define PLAYER_1_COLOR								al_map_rgb(255,0,0)
#define PLAYER_2_COLOR								al_map_rgb(0,0,255)
#define PLAYER_3_COLOR								al_map_rgb(0,255,0)
#define PLAYER_4_COLOR								al_map_rgb(255,255,0)

#define BATTLE_PLAYER_MENU_P1					"Player 1"
#define BATTLE_PLAYER_MENU_P2					"Player 2"
#define BATTLE_PLAYER_MENU_P3					"Player 3"
#define BATTLE_PLAYER_MENU_P4					"Player 4"

#define BATTLE_PLAYER_MENU_OPTION1				"NONE"
#define BATTLE_PLAYER_MENU_OPTION2				"HUMAN"
#define BATTLE_PLAYER_MENU_OPTION3				"CPU"

class BattleMenuScreen;

class BattlePlayerMenu : public Menu
{
	static int const BattlePlayerMenuDefaultSelectedOption = 0;

	enum Fonts {FONT};
	enum Coords {PLAYER_INIT_UPPER_Y = 350,
				 PLAYER_INIT_LOWER_Y = 400, 
				 PLAYER_INIT_MIDDLE_Y = 375, 
				 PLAYER_SPACING = 70,
				 PLAYER_LEFT_ARROY_TIP_X = 400,
				 PLAYER_LEFT_ARROW_RIGHT_LIMIT = 420,
				 PLAYER_RIGHT_ARROW_TIP_X = 930,
				 PLAYER_RIGHT_ARROW_LEFT_LIMIT = 910,
				 PLAYER_COLOR_SQUARE_LEFT_LIMIT = 430,
				 PLAYER_COLOR_SQUARE_RIGHT_LIMIT = 480,
				 PLAYER_NAME_CONTOUR_RIGHT_LIMIT = 613,
				 PLAYER_STATUS_CONTOUR_RIGHT_LIMIT = 900,
				 PLAYER_NAME_TEXT_X = 493,
				 PLAYER_STATUS_TEXT_X = 756,
				 CONTOUR_THICKNESS = 4
				};
	enum Players {PLAYER_1, PLAYER_2, PLAYER_3, PLAYER_4};
	enum PlayerChoices {NO_PLAYER, PLAYER, CPU};

	BattleMenuScreen* screen;

	int playerNum;

public:

	BattlePlayerMenu(BattleMenuScreen* screen, ALLEGRO_FONT* font, int playerNum, bool draw);

	void update(bool* keys);
	void render() const;
	void render(int order) const;

	~BattlePlayerMenu();
};



#endif