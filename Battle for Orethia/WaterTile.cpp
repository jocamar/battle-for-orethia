/*#include "WaterTile.h"
#include "unit.h"

float const WaterTile::waterDefense = 0.0;
string const WaterTile::waterTitle = "Water";

WaterTile::WaterTile(int x,int y, ALLEGRO_BITMAP* image) : Tile(x, y)
{
	traversable = false;
	traversableByHeavy = false;
	bonusDefense = WaterTile::waterDefense;
	moveCost = WaterTile::waterMoveCost;
	resourceBonus = WaterTile::waterResourceBonus;
	this->tileName = WaterTile::waterTitle;

	this->sprite = Sprite(image, x, y, WaterTile::waterFrameNum, TILE_WIDTH, TILE_HEIGHT, WaterTile::waterAnimationDelay, WATER_X*TILE_WIDTH, WATER_Y*TILE_HEIGHT);
	this->infoSprite = Sprite(image, 0, 0, 1, TILE_WIDTH, TILE_HEIGHT, WaterTile::waterAnimationDelay, WATER_X*TILE_WIDTH, WATER_Y*TILE_HEIGHT);
}

void WaterTile::renderPreview(int posX, int posY) const
{
	int actualX = (x * TILE_PREVIEW_WIDTH) + posX;
	int actualY = (y * TILE_PREVIEW_HEIGHT) + posY;

	al_draw_filled_rectangle(actualX,actualY, actualX + TILE_PREVIEW_WIDTH, actualY + TILE_PREVIEW_HEIGHT,WATER_COLOR);
}

WaterTile* WaterTile::clone() const
{
	return new WaterTile(*this);
}

WaterTile::~WaterTile()
{
}*/