#include "ScenarioScreen.h"	
#include "ScenarioPauseMenu.h"

ScenarioScreen::ScenarioScreen(Game* game, Scenario* scenario) : Screen(game)
{
	this->currScenario = scenario;
	currScenario->setScreen(this);
	images.push_back(al_load_bitmap("resources/images/Tiles.png"));
	images.push_back(al_load_bitmap("resources/images/FootTrooper.png"));
	images.push_back(al_load_bitmap("resources/images/Arrows.png"));
	images.push_back(al_load_bitmap("resources/images/CaptureAnimation.png"));
	images.push_back(al_load_bitmap("resources/images/Flags.png"));
	images.push_back(al_load_bitmap("resources/images/Icons.png"));
	images.push_back(al_load_bitmap("resources/images/Cursor.png"));
	images.push_back(al_load_bitmap("resources/images/fog.png"));
	images.push_back(al_load_bitmap("resources/images/Banners.png"));
	images.push_back(al_load_bitmap("resources/images/MenuBG.png"));
	images.push_back(al_load_bitmap("resources/images/TileInfoBG.png"));
	images.push_back(al_load_bitmap("resources/images/UnitSelectCursor.png"));
	images.push_back(al_load_bitmap("resources/images/TrapBG.png"));

	currScenario->updateMapImages(images, images[FOG_OF_WAR]);
	currScenario->updatePlayerImages(images[CURSOR]);
	currScenario->setActivePlayer(0);

	small_font = al_load_font("resources/fonts/coolvetica rg.ttf",SIZE_SMALL,0);
	medium_font = al_load_font("resources/fonts/coolvetica rg.ttf",SIZE_MEDIUM,0);
	large_font = al_load_font("resources/fonts/coolvetica rg.ttf",SIZE_LARGE,0);
	tiny_font = al_load_font("resources/fonts/coolvetica rg.ttf",SIZE_TINY,0);

	currScenario->initializeBanner(images[BANNERS], medium_font, small_font);
	currScenario->initializeTileInfo(images[TILE_INFO_BACKGROUND],images[ScenarioScreen::UNIT_ICONS], medium_font, small_font);

	for(unsigned int i = 0; i < images.size(); i++)
	{
		al_convert_mask_to_alpha(images[i], TRANSPARENT_SCENARIO);
	}
}

void ScenarioScreen::update(bool* keys)
{
	currScenario->update(keys);
}

void ScenarioScreen::render() const
{
	currScenario->render(medium_font);
}

void ScenarioScreen::switchToMain()
{
	game->changeScreen(new MainMenuScreen(game));
}

ALLEGRO_FONT* ScenarioScreen::getFontSmall() const
{
	return small_font;
}

ALLEGRO_FONT* ScenarioScreen::getFontMedium() const
{
	return medium_font;
}

ALLEGRO_FONT* ScenarioScreen::getFontLarge() const
{
	return large_font;
}

ALLEGRO_FONT* ScenarioScreen::getFontTiny() const
{
	return tiny_font;
}

ScenarioScreen::~ScenarioScreen()
{
	delete currScenario;

	al_destroy_font(medium_font);
	al_destroy_font(small_font);
	al_destroy_font(large_font);
}