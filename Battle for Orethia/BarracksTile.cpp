/*#include "BarracksTile.h"
#include "unit.h"
#include "scenario.h"
#include "ScenarioPauseMenu.h"
#include "ScenarioScreen.h"
#include "ShopMenu.h"

float const BarracksTile::barracksDefence = 0.4;
string const BarracksTile::barracksTypeA = "BarracksRed";
string const BarracksTile::barracksTypeB = "BarracksBlue";
string const BarracksTile::barracksTypeC = "BarracksGreen";
string const BarracksTile::barracksTypeD = "BarracksYellow";
string const BarracksTile::barracksTypeE = "BarracksWhite";
string const BarracksTile::barracksTitle = "Barracks";

BarracksTile::BarracksTile(int x,int y, string type, ALLEGRO_BITMAP* image) : Tile(x, y)
{
	traversable = true;
	traversableByHeavy = true;
	bonusDefense = BarracksTile::barracksDefence;
	moveCost = BarracksTile::barracksMoveCost;
	resourceBonus = BarracksTile::barracksResourceBonus;
	loyalty = BarracksTile::barracksLoyalty;
	this->tileName = BarracksTile::barracksTitle;

	this->type = type;

	for(unsigned int i = 0; i < PLAYER_NUM; i++)
	{
		this->lastKnownType[i] = this->type;
	}

	if(type == BarracksTile::barracksTypeA)
	{
		this->sprite = Sprite(image, x, y, BarracksTile::barracksFrameNum, TILE_WIDTH, TILE_HEIGHT, BarracksTile::barracksAnimationDelay, BARRACKS_RED_X*TILE_WIDTH, BARRACKS_RED_Y*TILE_HEIGHT);
		this->infoSprite = Sprite(image,0,0,1,TILE_WIDTH,TILE_HEIGHT,10,BARRACKS_RED_X*TILE_WIDTH, BARRACKS_RED_Y*TILE_HEIGHT);
	}
	else if (type == BarracksTile::barracksTypeB)
	{
		this->sprite = Sprite(image, x, y, BarracksTile::barracksFrameNum, TILE_WIDTH, TILE_HEIGHT, BarracksTile::barracksAnimationDelay, BARRACKS_BLUE_X*TILE_WIDTH, BARRACKS_BLUE_Y*TILE_HEIGHT);
		this->infoSprite = Sprite(image,0,0,1,TILE_WIDTH,TILE_HEIGHT,10,BARRACKS_BLUE_X*TILE_WIDTH, BARRACKS_BLUE_Y*TILE_HEIGHT);
	}
	else if (type == BarracksTile::barracksTypeC)
	{
		this->sprite = Sprite(image, x, y, BarracksTile::barracksFrameNum, TILE_WIDTH, TILE_HEIGHT, BarracksTile::barracksAnimationDelay, BARRACKS_GREEN_X*TILE_WIDTH, BARRACKS_GREEN_Y*TILE_HEIGHT);
		this->infoSprite = Sprite(image,0,0,1,TILE_WIDTH,TILE_HEIGHT,10,BARRACKS_GREEN_X*TILE_WIDTH, BARRACKS_GREEN_Y*TILE_HEIGHT);
	}
	else if (type == BarracksTile::barracksTypeD)
	{
		this->sprite = Sprite(image, x, y, BarracksTile::barracksFrameNum, TILE_WIDTH, TILE_HEIGHT, BarracksTile::barracksAnimationDelay, BARRACKS_YELLOW_X*TILE_WIDTH, BARRACKS_YELLOW_Y*TILE_HEIGHT);
		this->infoSprite = Sprite(image,0,0,1,TILE_WIDTH,TILE_HEIGHT,10,BARRACKS_YELLOW_X*TILE_WIDTH, BARRACKS_YELLOW_Y*TILE_HEIGHT);
	}
	else if (type == BarracksTile::barracksTypeE)
	{
		this->sprite = Sprite(image, x, y, BarracksTile::barracksFrameNum, TILE_WIDTH, TILE_HEIGHT, BarracksTile::barracksAnimationDelay, BARRACKS_WHITE_X*TILE_WIDTH, BARRACKS_WHITE_Y*TILE_HEIGHT);
		this->infoSprite = Sprite(image,0,0,1,TILE_WIDTH,TILE_HEIGHT,10,BARRACKS_WHITE_X*TILE_WIDTH, BARRACKS_WHITE_Y*TILE_HEIGHT);
	}
}

bool BarracksTile::isOwnable() const
{
	return true;
}

void BarracksTile::renderPreview(int posX, int posY) const
{
	int actualX = (x * TILE_PREVIEW_WIDTH) + posX;
	int actualY = (y * TILE_PREVIEW_HEIGHT) + posY;

	if(type == BarracksTile::barracksTypeA)
	{
		al_draw_filled_rectangle(actualX,actualY, actualX + TILE_PREVIEW_WIDTH, actualY + TILE_PREVIEW_HEIGHT,BARRACKS_RED_COLOR);
	}
	else if (type == BarracksTile::barracksTypeB)
	{
		al_draw_filled_rectangle(actualX, actualY, actualX + TILE_PREVIEW_WIDTH, actualY + TILE_PREVIEW_HEIGHT,BARRACKS_BLUE_COLOR);
	}
	else if (type == BarracksTile::barracksTypeC)
	{
		al_draw_filled_rectangle(actualX, actualY, actualX + TILE_PREVIEW_WIDTH, actualY + TILE_PREVIEW_HEIGHT,BARRACKS_GREEN_COLOR);
	}
	else if (type == BarracksTile::barracksTypeD)
	{
		al_draw_filled_rectangle(actualX, actualY, actualX + TILE_PREVIEW_WIDTH, actualY + TILE_PREVIEW_HEIGHT,BARRACKS_YELLOW_COLOR);
	}
	else if (type == BarracksTile::barracksTypeE)
	{
		al_draw_filled_rectangle(actualX, actualY, actualX + TILE_PREVIEW_WIDTH, actualY + TILE_PREVIEW_HEIGHT,BARRACKS_WHITE_COLOR);
	}
}

void BarracksTile::setOwnership(char color)
{
	if(color == 'r')
	{
		this->type = BarracksTile::barracksTypeA;
	}
	else if (color == 'b')
	{
		this->type = BarracksTile::barracksTypeB;
	}
	else if (color == 'g')
	{
		this->type = BarracksTile::barracksTypeC;
	}
	else if (color == 'y')
	{
		this->type = BarracksTile::barracksTypeD;
	}
	else if (color == 'w')
	{
		this->type = BarracksTile::barracksTypeE;
	}
}

char BarracksTile::getOwnership() const
{
	if(type == BarracksTile::barracksTypeA)
	{
		return 'r';
	}
	else if (type == BarracksTile::barracksTypeB)
	{
		return 'b';
	}
	else if (type == BarracksTile::barracksTypeC)
	{
		return 'g';
	}
	else if (type == BarracksTile::barracksTypeD)
	{
		return 'y';
	}
	else if (type == BarracksTile::barracksTypeE)
	{
		return 'w';
	}

	return '\0';
}

char BarracksTile::getPrevOwnershipFor(int playerNum) const
{
	if(lastKnownType[playerNum] == BarracksTile::barracksTypeA)
	{
		return 'r';
	}
	else if (lastKnownType[playerNum] == BarracksTile::barracksTypeB)
	{
		return 'b';
	}
	else if (lastKnownType[playerNum] == BarracksTile::barracksTypeC)
	{
		return 'g';
	}
	else if (lastKnownType[playerNum] == BarracksTile::barracksTypeD)
	{
		return 'y';
	}
	else if (lastKnownType[playerNum] == BarracksTile::barracksTypeE)
	{
		return 'w';
	}

	return '\0';
}

BarracksTile* BarracksTile::clone() const
{
	return new BarracksTile(*this);
}

void BarracksTile::doActionA(Scenario* scenario, ScenarioScreen* screen, bool* keys)
{
	if(scenario->getState() == Scenario::IDLE)
	{
		if(occupied)
		{
			if(unitOnTile->getState() == Unit::RESTING || !visible)
			{
				scenario->activateMenu(new ScenarioPauseMenu(screen, scenario, screen->getFontMedium(), screen->getFontLarge()));
				scenario->changeState(Scenario::IN_MENU);
			}
			else if (unitOnTile->getState() == Unit::IDLE && unitOnTile->getColor() == scenario->getActivePlayer()->getColor())
			{
				scenario->selectUnit(unitOnTile);
			}
			else if (unitOnTile->getState() == Unit::IDLE)
			{
				scenario->selectUnit(unitOnTile);
				keys[A] = true;
				scenario->changeState(Scenario::CHECKING_UNIT_MOVEMENT);
			}
		}
		else if (getOwnership() != scenario->getActivePlayer()->getColor())
		{
			scenario->activateMenu(new ScenarioPauseMenu(screen, scenario, screen->getFontMedium(), screen->getFontLarge()));
			scenario->changeState(Scenario::IN_MENU);
		}
		else
		{
			scenario->activateMenu(new ShopMenu(screen, scenario, screen->getFontMedium(), screen->getFontLarge(), this));
			scenario->changeState(Scenario::IN_MENU);
		}
	}
	else if(scenario->getState() == Scenario::SELECTING_UNIT)
	{
		if(( !occupied || isOccupiedBy(scenario->getSelectedUnit()) ) && highlightedWhite)
		{
			scenario->startUnitMovement();
			scenario->changeState(Scenario::UNIT_MOVING);
		}
	}
}

BarracksTile::~BarracksTile()
{
}

void BarracksTile::updateTileFor(int playerNum)
{
	this->lastKnownType[playerNum] = this->type;
}

void BarracksTile::switchSpriteFor(int playerNum)
{
	if(this->lastKnownType[playerNum] == BarracksTile::barracksTypeA)
	{
		this->sprite.setAnimationStartX(BARRACKS_RED_X*TILE_WIDTH);
		this->sprite.setAnimationStartY(BARRACKS_RED_Y*TILE_HEIGHT);
		this->infoSprite.setAnimationStartX(BARRACKS_RED_X*TILE_WIDTH);
		this->infoSprite.setAnimationStartY(BARRACKS_RED_Y*TILE_HEIGHT);
	}
	else if (this->lastKnownType[playerNum] == BarracksTile::barracksTypeB)
	{
		this->sprite.setAnimationStartX(BARRACKS_BLUE_X*TILE_WIDTH);
		this->sprite.setAnimationStartY(BARRACKS_BLUE_Y*TILE_HEIGHT);
		this->infoSprite.setAnimationStartX(BARRACKS_BLUE_X*TILE_WIDTH);
		this->infoSprite.setAnimationStartY(BARRACKS_BLUE_Y*TILE_HEIGHT);
	}
	else if (this->lastKnownType[playerNum] == BarracksTile::barracksTypeC)
	{
		this->sprite.setAnimationStartX(BARRACKS_GREEN_X*TILE_WIDTH);
		this->sprite.setAnimationStartY(BARRACKS_GREEN_Y*TILE_HEIGHT);
		this->infoSprite.setAnimationStartX(BARRACKS_GREEN_X*TILE_WIDTH);
		this->infoSprite.setAnimationStartY(BARRACKS_GREEN_Y*TILE_HEIGHT);
	}
	else if (this->lastKnownType[playerNum] == BarracksTile::barracksTypeD)
	{
		this->sprite.setAnimationStartX(BARRACKS_YELLOW_X*TILE_WIDTH);
		this->sprite.setAnimationStartY(BARRACKS_YELLOW_Y*TILE_HEIGHT);
		this->infoSprite.setAnimationStartX(BARRACKS_YELLOW_X*TILE_WIDTH);
		this->infoSprite.setAnimationStartY(BARRACKS_YELLOW_Y*TILE_HEIGHT);
	}
	else if (this->lastKnownType[playerNum] == BarracksTile::barracksTypeE)
	{
		this->sprite.setAnimationStartX(BARRACKS_WHITE_X*TILE_WIDTH);
		this->sprite.setAnimationStartY(BARRACKS_WHITE_Y*TILE_HEIGHT);
		this->infoSprite.setAnimationStartX(BARRACKS_WHITE_X*TILE_WIDTH);
		this->infoSprite.setAnimationStartY(BARRACKS_WHITE_Y*TILE_HEIGHT);
	}
}

void BarracksTile::resetLoyalty()
{
	loyalty = BarracksTile::barracksLoyalty;
}

void BarracksTile::capture()
{
	unitOnTile->startCapturing();
	loyalty -= unitOnTile->getConvertedHP();

	if(loyalty <= 0)
	{
		setOwnership(unitOnTile->getColor());
		resetLoyalty();
		unitOnTile->stopCapturing();
	}
}

int BarracksTile::getDefaultLoyalty() const
{
	return BarracksTile::barracksLoyalty;
}*/