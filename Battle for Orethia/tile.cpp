#include "tile.h"
#include "unit.h"
#include "scenario.h"
#include "ScenarioPauseMenu.h"
#include "ScenarioScreen.h"

Tile::Tile (int x, int y, TileInfo* tileInfo, int tileType, bool visible, bool occupied, bool highlightedWhite, bool highlightedRed)
{
	this->visible = visible;
	this->occupied = occupied;
	this->highlightedWhite = highlightedWhite;
	this->highlightedRed = highlightedRed;
	this->x = x;
	this->y = y;
	this->unitOnTile = NULL;
	this->info = tileInfo;
	this->tileTypeID = tileType;
	
	for(unsigned int i = 0; i < PLAYER_NUM; i++)
	{
		this->lastKnownType.push_back(this->tileTypeID);
		this->lastSeenByPlayer.push_back(info);
	}
}

Tile::Tile( const Tile & other, map<int,TileInfo> & tileInfo )
{
	this->visible = other.visible;
	this->occupied = other.occupied;
	this->highlightedWhite = other.highlightedWhite;
	this->highlightedRed = other.highlightedRed;
	this->x = other.x;
	this->y = other.y;
	this->unitOnTile = other.unitOnTile;

	map<int,TileInfo>::iterator it = tileInfo.find(other.info->getID());
	if(it == tileInfo.end())
		this->info = NULL;
	else
		this->info = &(it->second);

	this->tileTypeID = other.tileTypeID;

	for(unsigned int i = 0; i < PLAYER_NUM; i++)
	{
		this->lastKnownType.push_back(this->tileTypeID);
		this->lastSeenByPlayer.push_back(info);
	}
}

void Tile::setHW (bool newValue)
{
	this->highlightedWhite = newValue;
}

void Tile::setHR (bool newValue)
{
	this->highlightedRed = newValue;
}

void Tile::setVisible (bool newValue)
{
	this->visible = newValue;
}

void Tile::occupy (Unit* unit)
{
	this->occupied = true;
	this->unitOnTile = unit;
	unitOnTile->setPos(x,y, this);
}

void Tile::unoccupy()
{
	this->occupied = false;
	resetLoyalty();
	unitOnTile->stopCapturing();
	this->unitOnTile = NULL;
}

void Tile::updateTileFor(int playerNum)
{
	this->lastKnownType[playerNum] = this->tileTypeID;
	this->lastSeenByPlayer[playerNum] = info;
}

bool Tile::isHW() const
{
	return highlightedWhite;
}

bool Tile::isHR() const
{
	return highlightedRed;
}

bool Tile::isOccupied() const
{
	return occupied;
}

void Tile::actionA(Scenario* scenario, ScenarioScreen* screen, bool* keys)
{
	if(scenario->getState() == Scenario::IDLE)
	{
		if(occupied)
		{
			if(unitOnTile->getState() == Unit::RESTING || !visible)
			{
				scenario->activateMenu(new ScenarioPauseMenu(screen, scenario, screen->getFontMedium(), screen->getFontLarge()));
				scenario->changeState(Scenario::IN_MENU);
			}
			else if (unitOnTile->getState() == Unit::IDLE && unitOnTile->getColor() == scenario->getActivePlayer()->getColor())
			{
				scenario->selectUnit(unitOnTile);
			}
			else if (unitOnTile->getState() == Unit::IDLE)
			{
				scenario->selectUnit(unitOnTile);
				keys[A] = true;
				scenario->changeState(Scenario::CHECKING_UNIT_MOVEMENT);
			}
		}
		else
		{
			scenario->activateMenu(new ScenarioPauseMenu(screen, scenario, screen->getFontMedium(), screen->getFontLarge()));
			scenario->changeState(Scenario::IN_MENU);
		}
	}
	else if(scenario->getState() == Scenario::SELECTING_UNIT)
	{
		if((!occupied || isOccupiedBy(scenario->getSelectedUnit()) || !visible ) && highlightedWhite)
		{
			scenario->startUnitMovement();
			scenario->changeState(Scenario::UNIT_MOVING);
		}
	}

}

void Tile::actionB(Scenario* scenario, ScenarioScreen* screen, bool* keys)
{
	if(this->unitOnTile == NULL || !visible)
	{
		scenario->setUnitsTint(al_map_rgba_f(0.2,0.2,0.2,0.2));
		scenario->changeState(Scenario::CHECKING_MAP);
	}
	else
	{
		scenario->checkUnitRange(unitOnTile);
		scenario->changeState(Scenario::CHECKING_UNIT_RANGE);
	}
}

/*void Tile::setSprite(ALLEGRO_BITMAP* newSprite)
{
	sprite.setImage(newSprite);
	infoSprite.setImage(newSprite);
}*/

bool Tile::isVisible() const
{
	return visible;
}

int Tile::getResourceBonus() const
{
	return info->getResourceBonus();
}

Tile::~Tile()
{
}

int Tile::getX() const
{
	return x;
}

int Tile::getY() const
{
	return y;
}

int Tile::getLoyalty() const
{
	return loyalty;
}

void Tile::setParent (Tile* parent)
{
	this->parent = parent;
}

Tile* Tile::getParent()
{
	return parent;
}

void Tile::setDist(int newDist)
{
	dist = newDist;
}

int Tile::getDist() const
{
	return dist;
}

int Tile::getMoveCost(int unitType) const
{
	return info->getMoveCost(unitType);
}

/*bool Tile::isTraversableByLight() const
{
	return traversable;
}*/

/*bool Tile::isTraversableByHeavy() const
{
	return traversableByHeavy;
}*/

bool Tile::isOccupiedBy(Unit* unit) const
{
	if(unitOnTile == unit)
		return true;
	else
		return false;
}

bool Tile::isTraversableBy(Unit* unit) const
{
	if(info->getMoveCost(unit->getUnitMoveType()) < unit->getMovePoints())
		return false;
	/*if(unit->isLight() && isTraversableByLight())
		return true;
	else if (isTraversableByHeavy())
		return true;*/

	return true;
}

bool Tile::isOccupiedByEnemy(Unit* unit)
{
	if(unitOnTile != NULL && unit->isEnemy(unitOnTile))
		return true;
	else
		return false;
}

Unit* Tile::getUnit()
{
	return unitOnTile;
}

void Tile::showSpriteFor(int playerNum)
{
}

void Tile::update(int camX, int camY)
{
	//sprite.update();

	/*int actualX, actualY;

	actualX = (x - camX) * TileInfo::TILE_WIDTH;
	actualY = (y - camY) * TileInfo::TILE_HEIGHT;

	//sprite.setPos(actualX,actualY);
	info->setSpritePos(actualX, actualY, tileTypeID);*/
}

void Tile::render(int camX, int camY, int playerNum) const
{
	int actualX = (x - camX) * TileInfo::TILE_WIDTH;
	int actualY = (y - camY) * TileInfo::TILE_HEIGHT;

	lastSeenByPlayer[playerNum]->render(lastKnownType[playerNum], actualX, actualY);
	
	/*if (sprite.getImage() != NULL)
	{
		sprite.render();
	}*/
}

void Tile::resetLoyalty()
{
	loyalty = info->getMaxLoyalty();
}

void Tile::capture()
{
	if(info->isOwnable())
	{
		unitOnTile->startCapturing();
		loyalty -= unitOnTile->getConvertedHP();

		if(loyalty <= 0)
		{
			setOwnership(unitOnTile->getColor());
			resetLoyalty();
			unitOnTile->stopCapturing();
		}
	}
}

void Tile::setOwnership(char color)
{
	int typeID = info->getTypeByOwner(color);
	if(typeID >= 0)
	{
		tileTypeID = typeID;
	}
}

char Tile::getOwnership() const
{
	return info->getOwnership(tileTypeID);
}

char Tile::getPrevOwnershipFor(int playerNum) const
{
	return lastSeenByPlayer[playerNum]->getOwnership(lastKnownType[playerNum]);
}

bool Tile::isHideout() const
{
	return info->isHideout();
}

bool Tile::isOwnable() const
{
	return info->isOwnable();
}

int Tile::getDefaultLoyalty() const
{
	return 1;
}

void Tile::renderInfoSprite(int playerNum) const
{
	lastSeenByPlayer[playerNum]->renderInfoSprite(lastKnownType[playerNum]); //infoSprite.render();
}

void Tile::updateInfoSprite(int x, int y)
{
	info->updateInfoSprite(x, y); //infoSprite.setPos(x,y);
}

string Tile::getName() const
{
	return info->getName();
}

float Tile::getDefense() const
{
	return info->getDefense();
}

void Tile::renderMinimapTile( int mapx, int mapy, int playerNum ) const
{
	lastSeenByPlayer[playerNum]->renderMinimapTile(lastKnownType[playerNum],mapx+x*TileInfo::TILE_PREVIEW_WIDTH, mapy+y*TileInfo::TILE_PREVIEW_HEIGHT);
}

void TileInfo::setSprites( vector<ALLEGRO_BITMAP*> newSprites )
{
	for(unsigned int i = 0; i < tileTypes.size(); i++)
	{
		for(unsigned int j = 0; j < tileTypes[i].sprites.size(); j++)
			tileTypes[i].sprites[j].setImage(newSprites[spriteIndex]);

		for(unsigned int j = 0; j < tileTypes[i].infoSprites.size(); j++)
			tileTypes[i].infoSprites[j].setImage(newSprites[spriteIndex]);

		battleBG.setImage(newSprites[bgIndex]);
	}
}

TileInfo::TileInfo( int id, string name, int moveCostLandLight, int moveCostLandHeavy, int moveCostWater, int resBonus, int maxLoyalty, float defense, bool captureable, bool hideout, map<int,TileType> tileTypes, int spriteIndex /*= 0*/, int bgIndex /*= 13*/ )
{
	this->id = id;
	this->name = name;
	this->moveCostLandLight = moveCostLandLight;
	this->moveCostLandHeavy = moveCostLandHeavy;
	this->moveCostWater = moveCostWater;
	this->resBonus = resBonus;
	this->maxLoyalty = maxLoyalty;
	this->defense = defense;
	this->captureable = captureable;
	this->hideout = hideout;
	this->tileTypes = tileTypes;
	this->spriteIndex = spriteIndex;
	this->bgIndex = bgIndex;
}

void TileInfo::renderMinimapTile( int tileType, int x, int y ) const
{
	al_draw_filled_rectangle(x,y, x + TILE_PREVIEW_WIDTH, y + TILE_PREVIEW_HEIGHT,tileTypes.find(tileType)->second.minimapColor);
}

void TileInfo::render( int tileType, int x, int y ) const
{
	vector<Sprite> sprites = tileTypes.find(tileType)->second.sprites;

	for(unsigned int i = 0; i < sprites.size(); i++)
	{
		int offsetX = sprites[i].getX();
		int offsetY = sprites[i].getY();
		sprites[i].setPos(x+offsetX,y+offsetY);

		sprites[i].render();
	}
}

void TileInfo::renderInfoSprite( int tileType ) const
{
	vector<Sprite> sprites = tileTypes.find(tileType)->second.infoSprites;

	for(unsigned int i = 0; i < sprites.size(); i++)
	{
		sprites[i].render();
	}
}

void TileInfo::updateInfoSprite( int x, int y )
{
	map<int,TileType>::iterator it = tileTypes.begin();

	for(; it != tileTypes.end(); it++)
	{
		for(unsigned int i = 0; i < it->second.infoSprites.size(); i++)
		{
			it->second.infoSprites[i].update();
		}
	}
}

std::string TileInfo::getName() const
{
	return name;
}

float TileInfo::getDefense() const
{
	return defense;
}

int TileInfo::getResourceBonus() const
{
	return resBonus;
}

int TileInfo::getMoveCost( int unitType ) const
{
	if(unitType == Unit::UNIT_LAND_LIGHT)
		return moveCostLandLight;
	else if (unitType == Unit::UNIT_LAND_HEAVY)
		return moveCostLandHeavy;
	else return moveCostWater;
}

int TileInfo::getID() const
{
	return id;
}

char TileInfo::getOwnership( int tileType ) const
{
	return tileTypes.find(tileType)->second.color;
}

bool TileInfo::isOwnable() const
{
	return captureable;
}

bool TileInfo::isHideout() const
{
	return hideout;
}

int TileInfo::getTypeByOwner( char color ) const
{
	map<int,TileType>::const_iterator it = tileTypes.begin();

	for(; it != tileTypes.end(); it++)
		if(it->second.color == color)
			return it->first;

	return -1;
}

int TileInfo::getMaxLoyalty() const
{
	return maxLoyalty;
}
