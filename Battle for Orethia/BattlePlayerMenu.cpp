#include "BattlePlayerMenu.h"
#include "game.h"
#include "BattleMenuScreen.h"

BattlePlayerMenu::BattlePlayerMenu(BattleMenuScreen* screen, ALLEGRO_FONT* font, int playerNum, bool draw) : Menu(false, draw, BattlePlayerMenu::BattlePlayerMenuDefaultSelectedOption)
{
	this->screen = screen;
	this->playerNum = playerNum;

	options.push_back(new Option(BATTLE_PLAYER_MENU_OPTION1, true));
	options.push_back(new Option(BATTLE_PLAYER_MENU_OPTION2, true));
	options.push_back(new Option(BATTLE_PLAYER_MENU_OPTION3, true));

	fonts.push_back(font);
}

void BattlePlayerMenu::update(bool* keys)
{
	if(active)
	{
		if (keys[LEFT])
		{
			selectedOption = this->getNextActive(DIR_UP);

			switch(playerNum)
			{
			case PLAYER_1:
				screen->setPlayer1(screen->getPlayer1() - 1);
				
				if(screen->getPlayer1() < NO_PLAYER)
					screen->setPlayer1(CPU);

				break;
			case PLAYER_2:
				screen->setPlayer2(screen->getPlayer2() - 1);
				
				if(screen->getPlayer2() < NO_PLAYER)
					screen->setPlayer2(CPU);

				break;
			case PLAYER_3:
				screen->setPlayer3(screen->getPlayer3() - 1);
				
				if(screen->getPlayer3() < NO_PLAYER)
					screen->setPlayer3(CPU);

				break;
			case PLAYER_4:
				screen->setPlayer4(screen->getPlayer4() - 1);
				
				if(screen->getPlayer4() < NO_PLAYER)
					screen->setPlayer4(CPU);

				break;
			}

			keys[LEFT] = false;
		}
		else if (keys[RIGHT])
		{
			selectedOption = this->getNextActive(DIR_DOWN);

			switch(playerNum)
			{
			case PLAYER_1:
				screen->setPlayer1(screen->getPlayer1() + 1);
				
				if(screen->getPlayer1() > CPU)
					screen->setPlayer1(NO_PLAYER);

				break;
			case PLAYER_2:
				screen->setPlayer2(screen->getPlayer2() + 1);
				
				if(screen->getPlayer2() > CPU)
					screen->setPlayer2(NO_PLAYER);

				break;
			case PLAYER_3:
				screen->setPlayer3(screen->getPlayer3() + 1);
				
				if(screen->getPlayer3() > CPU)
					screen->setPlayer3(NO_PLAYER);

				break;
			case PLAYER_4:
				screen->setPlayer4(screen->getPlayer4() + 1);
				
				if(screen->getPlayer4() > CPU)
					screen->setPlayer4(NO_PLAYER);

				break;
			}

			keys[RIGHT] = false;
		}
	}
}

//does nothing
void BattlePlayerMenu::render() const
{

}

void BattlePlayerMenu::render(int order) const
{
	if(draw)
	{
		int playerUpperY = PLAYER_INIT_UPPER_Y + order*PLAYER_SPACING;
		int playerLowerY = PLAYER_INIT_LOWER_Y + order*PLAYER_SPACING;
		int playerMiddleY = PLAYER_INIT_MIDDLE_Y + order*PLAYER_SPACING;

		if(active)
			al_draw_filled_rectangle(PLAYER_NAME_CONTOUR_RIGHT_LIMIT, playerUpperY, PLAYER_STATUS_CONTOUR_RIGHT_LIMIT, playerLowerY, BATTLE_PLAYER_MENU_OVERLAY_COLOR);

		if(playerNum == PLAYER_1)
			al_draw_text(fonts[FONT], BATTLE_PLAYER_MENU_TEXT_COLOR,  PLAYER_NAME_TEXT_X,  playerUpperY,  0, BATTLE_PLAYER_MENU_P1);
		else if(playerNum == PLAYER_2)
			al_draw_text(fonts[FONT], BATTLE_PLAYER_MENU_TEXT_COLOR,  PLAYER_NAME_TEXT_X,  playerUpperY,  0, BATTLE_PLAYER_MENU_P2);
		else if(playerNum == PLAYER_3)
			al_draw_text(fonts[FONT], BATTLE_PLAYER_MENU_TEXT_COLOR,  PLAYER_NAME_TEXT_X,  playerUpperY,  0, BATTLE_PLAYER_MENU_P3);
		else if(playerNum == PLAYER_4)
			al_draw_text(fonts[FONT], BATTLE_PLAYER_MENU_TEXT_COLOR,  PLAYER_NAME_TEXT_X,  playerUpperY,  0, BATTLE_PLAYER_MENU_P4);

		al_draw_rectangle(PLAYER_COLOR_SQUARE_RIGHT_LIMIT, playerUpperY, PLAYER_NAME_CONTOUR_RIGHT_LIMIT, playerLowerY, BATTLE_PLAYER_MENU_CONTOUR_COLOR, CONTOUR_THICKNESS);
		al_draw_rectangle(PLAYER_NAME_CONTOUR_RIGHT_LIMIT, playerUpperY, PLAYER_STATUS_CONTOUR_RIGHT_LIMIT, playerLowerY, BATTLE_PLAYER_MENU_CONTOUR_COLOR, CONTOUR_THICKNESS);
		al_draw_filled_triangle(PLAYER_LEFT_ARROY_TIP_X, playerMiddleY, PLAYER_LEFT_ARROW_RIGHT_LIMIT, playerUpperY, PLAYER_LEFT_ARROW_RIGHT_LIMIT, playerLowerY, BATTLE_PLAYER_MENU_CONTOUR_COLOR);
		al_draw_filled_triangle(PLAYER_RIGHT_ARROW_TIP_X, playerMiddleY, PLAYER_RIGHT_ARROW_LEFT_LIMIT, playerUpperY, PLAYER_RIGHT_ARROW_LEFT_LIMIT, playerLowerY, BATTLE_PLAYER_MENU_CONTOUR_COLOR);


		if(selectedOption == NO_PLAYER)
			al_draw_text(fonts[FONT], BATTLE_PLAYER_MENU_TEXT_COLOR, PLAYER_STATUS_TEXT_X,  playerUpperY+2,  ALLEGRO_ALIGN_CENTRE, BATTLE_PLAYER_MENU_OPTION1);
		else if(selectedOption == PLAYER)
				al_draw_text(fonts[FONT], BATTLE_PLAYER_MENU_TEXT_COLOR, PLAYER_STATUS_TEXT_X, playerUpperY+2, ALLEGRO_ALIGN_CENTRE, BATTLE_PLAYER_MENU_OPTION2);
		else if (selectedOption == CPU)
				al_draw_text(fonts[FONT], BATTLE_PLAYER_MENU_TEXT_COLOR, PLAYER_STATUS_TEXT_X, playerUpperY+2, ALLEGRO_ALIGN_CENTRE, BATTLE_PLAYER_MENU_OPTION3);

		if(playerNum == PLAYER_1)
			al_draw_filled_rectangle(PLAYER_COLOR_SQUARE_LEFT_LIMIT, playerUpperY, PLAYER_COLOR_SQUARE_RIGHT_LIMIT,  playerLowerY, PLAYER_1_COLOR);
		else if(playerNum == PLAYER_2)
			al_draw_filled_rectangle(PLAYER_COLOR_SQUARE_LEFT_LIMIT, playerUpperY, PLAYER_COLOR_SQUARE_RIGHT_LIMIT,  playerLowerY, PLAYER_2_COLOR);
		else if(playerNum == PLAYER_3)
			al_draw_filled_rectangle(PLAYER_COLOR_SQUARE_LEFT_LIMIT, playerUpperY, PLAYER_COLOR_SQUARE_RIGHT_LIMIT,  playerLowerY, PLAYER_3_COLOR);
		else if(playerNum == PLAYER_4)
			al_draw_filled_rectangle(PLAYER_COLOR_SQUARE_LEFT_LIMIT, playerUpperY, PLAYER_COLOR_SQUARE_RIGHT_LIMIT,  playerLowerY, PLAYER_4_COLOR);

		al_draw_rectangle(PLAYER_COLOR_SQUARE_LEFT_LIMIT, playerUpperY, PLAYER_COLOR_SQUARE_RIGHT_LIMIT, playerLowerY, BATTLE_PLAYER_MENU_CONTOUR_COLOR, CONTOUR_THICKNESS);
	}
}

BattlePlayerMenu::~BattlePlayerMenu()
{
}