/*#include "ForestTile.h"
#include "unit.h"

float const ForestTile::forestDefence = 0.2;
string const ForestTile::forestTypeA = "Forest";
string const ForestTile::forestTypeB = "ForestTop";
string const ForestTile::forestTypeC = "ForestSide";
string const ForestTile::forestTypeD = "ForestTopSide";
string const ForestTile::forestTitle = "Forest";

ForestTile::ForestTile(int x,int y, string type, ALLEGRO_BITMAP* image) : Tile(x, y)
{
	traversable = true;
	traversableByHeavy = true;
	bonusDefense = ForestTile::forestDefence;
	moveCost = ForestTile::forestMoveCost;
	resourceBonus = ForestTile::forestResourceBonus;
	this->tileName = ForestTile::forestTitle;

	this->type = type;

	if(type == ForestTile::forestTypeA)
	{
		this->sprite = Sprite(image, x, y, ForestTile::forestFrameNum, TILE_WIDTH, TILE_HEIGHT, ForestTile::forestAnimationDelay, FOREST_ISOLATED_BOTTOM_X*TILE_WIDTH, FOREST_ISOLATED_BOTTOM_Y*TILE_HEIGHT);
		this->forestTop = Sprite(image,x,y-TILE_HEIGHT,ForestTile::forestFrameNum, TILE_WIDTH, TILE_HEIGHT, ForestTile::forestAnimationDelay, FOREST_ISOLATED_TOP_X*TILE_WIDTH, FOREST_ISOLATED_TOP_Y*TILE_HEIGHT);
	}
	else if (type == ForestTile::forestTypeB)
	{
		this->sprite = Sprite(image, x, y, ForestTile::forestFrameNum, TILE_WIDTH, TILE_HEIGHT, ForestTile::forestAnimationDelay, FOREST_TOP_BOTTOM_X*TILE_WIDTH, FOREST_TOP_BOTTOM_Y*TILE_HEIGHT);
		this->forestTop = Sprite(image,x,y-TILE_HEIGHT,ForestTile::forestFrameNum, TILE_WIDTH, TILE_HEIGHT, ForestTile::forestAnimationDelay, FOREST_TOP_TOP_X*TILE_WIDTH, FOREST_TOP_TOP_Y*TILE_HEIGHT);
	}
	else if (type == ForestTile::forestTypeC)
	{
		this->sprite = Sprite(image, x, y, ForestTile::forestFrameNum, TILE_WIDTH, TILE_HEIGHT, ForestTile::forestAnimationDelay, FOREST_SIDE_BOTTOM_RIGHT_X*TILE_WIDTH, FOREST_SIDE_BOTTOM_RIGHT_Y*TILE_HEIGHT);
		this->forestTop = Sprite(image,x,y-TILE_HEIGHT,ForestTile::forestFrameNum, TILE_WIDTH, TILE_HEIGHT, ForestTile::forestAnimationDelay, FOREST_ISOLATED_TOP_X*TILE_WIDTH, FOREST_ISOLATED_TOP_Y*TILE_HEIGHT);
		this->forestSide = Sprite(image,x-TILE_WIDTH,y,ForestTile::forestFrameNum, TILE_WIDTH, TILE_HEIGHT, ForestTile::forestAnimationDelay,FOREST_SIDE_LEFT_X*TILE_WIDTH, FOREST_SIDE_LEFT_Y*TILE_HEIGHT);
	}
	else if (type == ForestTile::forestTypeD)
	{
		this->sprite = Sprite(image, x, y, ForestTile::forestFrameNum, TILE_WIDTH, TILE_HEIGHT, ForestTile::forestAnimationDelay, FOREST_TOP_SIDE_BOTTOM_RIGHT_X*TILE_WIDTH, FOREST_TOP_SIDE_BOTTOM_RIGHT_Y*TILE_HEIGHT);
		this->forestTop = Sprite(image,x,y-TILE_HEIGHT,ForestTile::forestFrameNum, TILE_WIDTH, TILE_HEIGHT, ForestTile::forestAnimationDelay, FOREST_TOP_TOP_X*TILE_WIDTH, FOREST_TOP_TOP_Y*TILE_HEIGHT);
		this->forestSide = Sprite(image,x-TILE_WIDTH,y,ForestTile::forestFrameNum, TILE_WIDTH, TILE_HEIGHT, ForestTile::forestAnimationDelay,FOREST_SIDE_LEFT_X*TILE_WIDTH, FOREST_SIDE_LEFT_Y*TILE_HEIGHT);
	}

	this->infoSprite = Sprite(image, 0, 0, 1, TILE_WIDTH, TILE_HEIGHT, ForestTile::forestAnimationDelay, FOREST_ISOLATED_BOTTOM_X*TILE_WIDTH, FOREST_ISOLATED_BOTTOM_Y*TILE_HEIGHT);
	this->infoTop = Sprite(image,0,0-TILE_HEIGHT,1, TILE_WIDTH, TILE_HEIGHT, ForestTile::forestAnimationDelay, FOREST_ISOLATED_TOP_X*TILE_WIDTH, FOREST_ISOLATED_TOP_Y*TILE_HEIGHT);
}

void ForestTile::render() const
{
	if(sprite.getImage() != NULL)
	{
		sprite.render();

		if(type == ForestTile::forestTypeA)
		{
			forestTop.render();
		}
		else if (type == ForestTile::forestTypeB)
		{
			forestTop.render();
		}
		else if (type == ForestTile::forestTypeC)
		{
			forestTop.render();
			forestSide.render();
		}
		else if (type == ForestTile::forestTypeD)
		{
			forestTop.render();
			forestSide.render();
		}
	}
}

void ForestTile::renderInfoSprite() const
{
	if(infoSprite.getImage() != NULL)
	{
		infoSprite.render();
		infoTop.render();
	}
}

void ForestTile::updateInfoSprite(int x, int y)
{
	infoSprite.setPos(x,y);
	infoTop.setPos(x,y-TILE_HEIGHT);
}

void ForestTile::renderPreview(int posX, int posY) const
{
	int actualX = (x * TILE_PREVIEW_WIDTH) + posX;
	int actualY = (y * TILE_PREVIEW_HEIGHT) + posY;

	al_draw_filled_rectangle(actualX,actualY, actualX + TILE_PREVIEW_WIDTH, actualY + TILE_PREVIEW_HEIGHT,FOREST_COLOR);
}

void ForestTile::update(int camX, int camY)
{
	sprite.update();

	int actualX = (x - camX) * TILE_WIDTH;
	int actualY = (y - camY) * TILE_HEIGHT;

	sprite.setPos(actualX,actualY);

	if(type == ForestTile::forestTypeA)
	{
		this->forestTop.update();
		forestTop.setPos(actualX,actualY-TILE_HEIGHT);
	}
	else if (type == ForestTile::forestTypeB)
	{
		this->forestTop.update();
		forestTop.setPos(actualX,actualY-TILE_HEIGHT);
	}
	else if (type == ForestTile::forestTypeC)
	{
		this->forestTop.update();
		this->forestSide.update();
		forestTop.setPos(actualX,actualY-TILE_HEIGHT);
		forestSide.setPos(actualX-TILE_WIDTH,actualY);
	}
	else if (type == ForestTile::forestTypeD)
	{
		this->forestTop.update();
		this->forestSide.update();
		forestTop.setPos(actualX,actualY-TILE_HEIGHT);
		forestSide.setPos(actualX-TILE_WIDTH,actualY);
	}
}

bool ForestTile::isHideout() const
{
	return true;
}

ForestTile* ForestTile::clone() const
{
	return new ForestTile(*this);
}

ForestTile::~ForestTile()
{
}

void ForestTile::setSprite(ALLEGRO_BITMAP* newSprite)
{
	sprite.setImage(newSprite);
	forestTop.setImage(newSprite);
	forestSide.setImage(newSprite);
	infoSprite.setImage(newSprite);
	infoTop.setImage(newSprite);
}*/