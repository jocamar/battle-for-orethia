/*#include "DirtRoadTile.h"
#include "unit.h"

float const DirtRoadTile::dirtRoadDefence = -0.1;
string const DirtRoadTile::dirtRoadTypeA = "DirtRoadHorizontal";
string const DirtRoadTile::dirtRoadTypeB = "DirtRoadVertical";
string const DirtRoadTile::dirtRoadTypeC = "DirtRoadCross";
string const DirtRoadTile::dirtRoadTypeD = "DirtRoadTSectionDown";
string const DirtRoadTile::dirtRoadTypeE = "DirtRoadTSectionLeft";
string const DirtRoadTile::dirtRoadTypeF = "DirtRoadTSectionUp";
string const DirtRoadTile::dirtRoadTypeG = "DirtRoadTSectionRight";
string const DirtRoadTile::dirtRoadTypeH = "DirtRoadEndUp";
string const DirtRoadTile::dirtRoadTypeI = "DirtRoadEndLeft";
string const DirtRoadTile::dirtRoadTypeJ = "DirtRoadEndRight";
string const DirtRoadTile::dirtRoadTypeL = "DirtRoadEndDown";
string const DirtRoadTile::dirtRoadTypeM = "DirtRoadCurveUpLeft";
string const DirtRoadTile::dirtRoadTypeN = "DirtRoadCurveUpRight";
string const DirtRoadTile::dirtRoadTypeO = "DirtRoadCurveDownLeft";
string const DirtRoadTile::dirtRoadTypeP = "DirtRoadCurveDownRight";
string const DirtRoadTileInfo::dirtRoadTitle = "Dirt Road";

DirtRoadTile::DirtRoadTile(int x,int y, string type, ALLEGRO_BITMAP* image) : Tile(x, y)
{
	traversable = true;
	traversableByHeavy = true;
	bonusDefense = DirtRoadTile::dirtRoadDefence;
	moveCost = DirtRoadTile::dirtRoadMoveCost;
	resourceBonus = DirtRoadTile::dirtRoadResourceBonus;
	this->tileName = DirtRoadTile::dirtRoadTitle;

	this->type = type;

	if(type == DirtRoadTile::dirtRoadTypeA)
	{
		this->sprite = Sprite(image, x, y, DirtRoadTile::dirtRoadFrameNum, TILE_WIDTH, TILE_HEIGHT, DirtRoadTile::dirtRoadAnimationDelay, DIRT_ROAD_HORIZONTAL_X*TILE_WIDTH, DIRT_ROAD_HORIZONTAL_Y*TILE_HEIGHT);
	}
	else if (type == DirtRoadTile::dirtRoadTypeB)
	{
		this->sprite = Sprite(image, x, y, DirtRoadTile::dirtRoadFrameNum, TILE_WIDTH, TILE_HEIGHT, DirtRoadTile::dirtRoadAnimationDelay, DIRT_ROAD_VERTICAL_X*TILE_WIDTH, DIRT_ROAD_VERTICAL_Y*TILE_HEIGHT);
	}
	else if (type == DirtRoadTile::dirtRoadTypeC)
	{
		this->sprite = Sprite(image, x, y, DirtRoadTile::dirtRoadFrameNum, TILE_WIDTH, TILE_HEIGHT, DirtRoadTile::dirtRoadAnimationDelay, DIRT_ROAD_CROSS_X*TILE_WIDTH, DIRT_ROAD_CROSS_Y*TILE_HEIGHT);
	}
	else if (type == DirtRoadTile::dirtRoadTypeD)
	{
		this->sprite = Sprite(image, x, y, DirtRoadTile::dirtRoadFrameNum, TILE_WIDTH, TILE_HEIGHT, DirtRoadTile::dirtRoadAnimationDelay, DIRT_ROAD_T_SECTION_DOWN_X*TILE_WIDTH, DIRT_ROAD_T_SECTION_DOWN_Y*TILE_HEIGHT);
	}
	else if (type == DirtRoadTile::dirtRoadTypeE)
	{
		this->sprite = Sprite(image, x, y, DirtRoadTile::dirtRoadFrameNum, TILE_WIDTH, TILE_HEIGHT, DirtRoadTile::dirtRoadAnimationDelay, DIRT_ROAD_T_SECTION_LEFT_X*TILE_WIDTH, DIRT_ROAD_T_SECTION_LEFT_Y*TILE_HEIGHT);
	}
	else if (type == DirtRoadTile::dirtRoadTypeF)
	{
		this->sprite = Sprite(image, x, y, DirtRoadTile::dirtRoadFrameNum, TILE_WIDTH, TILE_HEIGHT, DirtRoadTile::dirtRoadAnimationDelay, DIRT_ROAD_T_SECTION_UP_X*TILE_WIDTH, DIRT_ROAD_T_SECTION_UP_Y*TILE_HEIGHT);
	}
	else if (type == DirtRoadTile::dirtRoadTypeG)
	{
		this->sprite = Sprite(image, x, y, DirtRoadTile::dirtRoadFrameNum, TILE_WIDTH, TILE_HEIGHT, DirtRoadTile::dirtRoadAnimationDelay, DIRT_ROAD_T_SECTION_RIGHT_X*TILE_WIDTH, DIRT_ROAD_T_SECTION_RIGHT_Y*TILE_HEIGHT);
	}
	else if (type == DirtRoadTile::dirtRoadTypeH)
	{
		this->sprite = Sprite(image, x, y, DirtRoadTile::dirtRoadFrameNum, TILE_WIDTH, TILE_HEIGHT, DirtRoadTile::dirtRoadAnimationDelay, DIRT_ROAD_END_UP_X*TILE_WIDTH, DIRT_ROAD_END_UP_Y*TILE_HEIGHT);
	}
	else if (type == DirtRoadTile::dirtRoadTypeI)
	{
		this->sprite = Sprite(image, x, y, DirtRoadTile::dirtRoadFrameNum, TILE_WIDTH, TILE_HEIGHT, DirtRoadTile::dirtRoadAnimationDelay, DIRT_ROAD_END_LEFT_X*TILE_WIDTH, DIRT_ROAD_END_LEFT_Y*TILE_HEIGHT);
	}
	else if (type == DirtRoadTile::dirtRoadTypeJ)
	{
		this->sprite = Sprite(image, x, y, DirtRoadTile::dirtRoadFrameNum, TILE_WIDTH, TILE_HEIGHT, DirtRoadTile::dirtRoadAnimationDelay, DIRT_ROAD_END_RIGHT_X*TILE_WIDTH, DIRT_ROAD_END_RIGHT_Y*TILE_HEIGHT);
	}
	else if (type == DirtRoadTile::dirtRoadTypeL)
	{
		this->sprite = Sprite(image, x, y, DirtRoadTile::dirtRoadFrameNum, TILE_WIDTH, TILE_HEIGHT, DirtRoadTile::dirtRoadAnimationDelay, DIRT_ROAD_END_DOWN_X*TILE_WIDTH, DIRT_ROAD_END_DOWN_Y*TILE_WIDTH);
	}
	else if (type == DirtRoadTile::dirtRoadTypeM)
	{
		this->sprite = Sprite(image, x, y, DirtRoadTile::dirtRoadFrameNum, TILE_WIDTH, TILE_HEIGHT, DirtRoadTile::dirtRoadAnimationDelay, DIRT_ROAD_CURVE_UP_LEFT_X*TILE_WIDTH, DIRT_ROAD_CURVE_UP_LEFT_Y*TILE_HEIGHT);
	}
	else if (type == DirtRoadTile::dirtRoadTypeN)
	{
		this->sprite = Sprite(image, x, y, DirtRoadTile::dirtRoadFrameNum, TILE_WIDTH, TILE_HEIGHT, DirtRoadTile::dirtRoadAnimationDelay, DIRT_ROAD_CURVE_UP_RIGHT_X*TILE_WIDTH, DIRT_ROAD_CURVE_UP_RIGHT_Y*TILE_HEIGHT);
	}
	else if (type == DirtRoadTile::dirtRoadTypeO)
	{
		this->sprite = Sprite(image, x, y, DirtRoadTile::dirtRoadFrameNum, TILE_WIDTH, TILE_HEIGHT, DirtRoadTile::dirtRoadAnimationDelay, DIRT_ROAD_CURVE_DOWN_LEFT_X*TILE_WIDTH, DIRT_ROAD_CURVE_DOWN_LEFT_Y*TILE_HEIGHT);
	}
	else if (type == DirtRoadTile::dirtRoadTypeP)
	{
		this->sprite = Sprite(image, x, y, DirtRoadTile::dirtRoadFrameNum, TILE_WIDTH, TILE_HEIGHT, DirtRoadTile::dirtRoadAnimationDelay, DIRT_ROAD_CURVE_DOWN_RIGHT_X*TILE_WIDTH, DIRT_ROAD_CURVE_DOWN_RIGHT_Y*TILE_HEIGHT);
	}

	this->infoSprite = Sprite(image, 0, 0, 1, TILE_WIDTH, TILE_HEIGHT, DirtRoadTile::dirtRoadAnimationDelay, DIRT_ROAD_VERTICAL_X*TILE_WIDTH, DIRT_ROAD_VERTICAL_Y*TILE_HEIGHT);
}

void DirtRoadTile::renderPreview(int posX, int posY) const
{
	int actualX = (x * TILE_PREVIEW_WIDTH) + posX;
	int actualY = (y * TILE_PREVIEW_HEIGHT) + posY;

	al_draw_filled_rectangle(actualX,actualY, actualX + TILE_PREVIEW_WIDTH, actualY + TILE_PREVIEW_HEIGHT,DIRT_ROAD_COLOR);
}

DirtRoadTile* DirtRoadTile::clone() const
{
	return new DirtRoadTile(*this);
}

DirtRoadTile::~DirtRoadTile()
{
}*/