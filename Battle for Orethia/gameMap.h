#ifndef GAME_MAP_H
#define GAME_MAP_H

#include "tile.h"
#include "player.h"
#include "gameExceptions.h"
#include <string>
#include <vector>
#include <fstream>
#include <climits>
#include <algorithm>
#include "pugixml.hpp"

using namespace std;

class Map
{
	enum Fog_Images{
		FOG_RIGHT, 
		FOG_DOWN, 
		FOG_LEFT,
		FOG_UP, 
		FOG_UP_RIGHT, 
		FOG_DOWN_RIGHT, 
		FOG_DOWN_LEFT, 
		FOG_UP_LEFT,
		FOG_ALL_SIDES, 
		FOG_DOWN_LEFT_UP, 
		FOG_LEFT_UP_RIGHT, 
		FOG_UP_RIGHT_DOWN, 
		FOG_LEFT_DOWN_RIGHT,
		FOG_CORNER_DOWN_LEFT,
		FOG_CORNER_UP_LEFT,
		FOG_CORNER_UP_RIGHT,
		FOG_CORNER_DOWN_RIGHT,
		FOG
	};

	string name;
	int sizeX, sizeY;
	vector <bool> playersAllowed;
	vector <vector <Tile>> tiles;
	map <int, TileInfo> tileInfo;

	Sprite fog_of_war;
	bool visibilityChanged;

	Tile loadTile(ALLEGRO_COLOR color, int x, int y);
	static Map loadMap(string filename, map<int,TileInfo> tileInfo);
	static map<int,TileInfo> loadTileInfo(string filename);

public:
	static vector<Map> loadMapsFromFolder(string folder);
	void print() const;

	Map() {};
	Map(string name, map<int,TileInfo> tileInfo);
	Map(const Map & other);
	void loadTerrainInfo(string terrainInfoSrc);

	string getName() const;

	const vector <bool> & getPlayersAllowed() const;
	int getSizeX() const;
	int getSizeY() const;

	Tile* getTile(int x, int y);

	void update(Player* player, int playerNum);

	void updateVisibility(Unit* unit);
	void resetVisibility();
	void switchVisibility(int x, int y, int VisRange);
	void setPlayerTilesVis(char color);
	void setVisibilityChanged(bool newVal);

	void gainResources(Player* activePlayer);

	void renderFoW(int camX, int camY) const;
	void render(int camX, int camY, int payerNum) const;
	void renderHW(int camX, int camY) const;
	void renderHR(int camX, int camY) const;
	void renderMinimap(int x, int y, int playerNum) const;

	void updateMapImages(vector<ALLEGRO_BITMAP*> newtiles,  ALLEGRO_BITMAP* fog);

	void initializeTiles(vector <Player> playersInGame);

	void highlightMovementNodes(Unit* unit);
	void resetMovementNodes();
	void highlightRangeNodes(Unit* unit);
	void highlightRangeNode(int x, int y);
	void resetRangeNodes();
	vector <Tile*> getShortestPath(Tile* dest);

	~Map();
};

#endif