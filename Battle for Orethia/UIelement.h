#ifndef UI_ELEMENT_H
#define UI_ELEMENT_H

class Scenario;

class UI_element
{
protected:
	Scenario* scenario;
public:
	UI_element(Scenario* scenario = 0);
	virtual void update() = 0;
	virtual void render() const = 0;
};

#endif