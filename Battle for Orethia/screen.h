#ifndef SCREEN_H
#define SCREEN_H

#include <vector>
#include "allegro5/allegro5.h"
#include "allegro5/allegro_image.h"

class Game;

using namespace std;

class Screen
{
protected:
	vector <ALLEGRO_BITMAP* > images;
	Game* game;

public:
	Screen(Game* game);

	ALLEGRO_BITMAP* getImage(int imageId);

	virtual void update(bool* keys) = 0;
	virtual void render() const = 0;
	virtual ~Screen();
};

#endif