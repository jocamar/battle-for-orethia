#include "unit.h"
#include "scenario.h"
#include "UnitContextMenu.h"

Unit::Unit(int x, int y, Tile* tile, ALLEGRO_BITMAP* sprite, char color, ALLEGRO_BITMAP* icons, ALLEGRO_FONT* hp_font)
{
	this->x = x;
	this->y = y;
	this->currTile = tile;
	this->visibility = 3;
	this->state = RESTING;
	this->cost = 1000;
	this->color = color;
	this->movePoints = 4;
	this->spriteX = x*TileInfo::TILE_WIDTH;
	this->spriteY = y*TileInfo::TILE_HEIGHT;
	this->screenX = 0;
	this->screenY = 0;
	this->currPathTile = 0;
	this->maxRange = 2;
	this->minRange = 2;
	this->attack = 5;
	this->canCap = true;
	this->moveType = UNIT_LAND_LIGHT;
	this->maxHP = 20;
	this->hp = 20;
	this->isCapturing = false;
	this->hp_font = hp_font;
	this->name = "Footmen";
	this->ammo = 10;
	this->maxAmmo = 10;
	this->turnedLeft = false;

	if(color == 'r')
	{
		this->sprite = Sprite(sprite, 0, 0, 4, TileInfo::TILE_WIDTH, TileInfo::TILE_HEIGHT, 15, RED_X*TileInfo::TILE_WIDTH, IDLE_RED_Y*TileInfo::TILE_HEIGHT);
		this->captureIcon = Sprite(icons, 0, 0, 1,CAP_ICON_WIDTH,CAP_ICON_WIDTH, 10, 0, CAP_RED*CAP_ICON_WIDTH);
		this->lifeIcon = Sprite(icons,0,0,1,CAP_ICON_WIDTH,CAP_ICON_WIDTH,10,0,LIFE_ICON_RED*CAP_ICON_WIDTH);
		this->infoSprite = Sprite(sprite,0,0,1, TileInfo::TILE_WIDTH,TileInfo::TILE_HEIGHT, 10, RED_X*TileInfo::TILE_WIDTH, IDLE_RED_Y*TileInfo::TILE_HEIGHT);
	}
	else if (color == 'b')
	{
		this->sprite = Sprite(sprite, 0, 0, 4, TileInfo::TILE_WIDTH, TileInfo::TILE_HEIGHT, 15, BLUE_X*TileInfo::TILE_WIDTH, IDLE_BLUE_Y*TileInfo::TILE_HEIGHT);
		this->captureIcon = Sprite(icons, 0, 0, 1,CAP_ICON_WIDTH,CAP_ICON_WIDTH, 10, 0, CAP_BLUE*CAP_ICON_WIDTH);
		this->lifeIcon = Sprite(icons,0,0,1,CAP_ICON_WIDTH,CAP_ICON_WIDTH,10,0,LIFE_ICON_BLUE*CAP_ICON_WIDTH);
		this->infoSprite = Sprite(sprite,0,0,1, TileInfo::TILE_WIDTH,TileInfo::TILE_HEIGHT, 10, BLUE_X*TileInfo::TILE_WIDTH, IDLE_BLUE_Y*TileInfo::TILE_HEIGHT);
	}
	else if (color == 'g')
	{
		this->sprite = Sprite(sprite, 0, 0, 4, TileInfo::TILE_WIDTH, TileInfo::TILE_HEIGHT, 15, GREEN_X*TileInfo::TILE_WIDTH, IDLE_GREEN_Y*TileInfo::TILE_HEIGHT);
		this->captureIcon = Sprite(icons, 0, 0, 1,CAP_ICON_WIDTH,CAP_ICON_WIDTH, 10, 0, CAP_GREEN*CAP_ICON_WIDTH);
		this->lifeIcon = Sprite(icons,0,0,1,CAP_ICON_WIDTH,CAP_ICON_WIDTH,10,0,LIFE_ICON_GREEN*CAP_ICON_WIDTH);
		this->infoSprite = Sprite(sprite,0,0,1, TileInfo::TILE_WIDTH,TileInfo::TILE_HEIGHT, 10, GREEN_X*TileInfo::TILE_WIDTH, IDLE_GREEN_Y*TileInfo::TILE_HEIGHT);
	}
	else if (color == 'y')
	{
		this->sprite = Sprite(sprite, 0, 0, 4, TileInfo::TILE_WIDTH, TileInfo::TILE_HEIGHT, 15, YELLOW_X*TileInfo::TILE_WIDTH, IDLE_YELLOW_Y*TileInfo::TILE_HEIGHT);
		this->captureIcon = Sprite(icons, 0, 0, 1,CAP_ICON_WIDTH,CAP_ICON_WIDTH, 10, 0, CAP_YELLOW*CAP_ICON_WIDTH);
		this->lifeIcon = Sprite(icons,0,0,1,CAP_ICON_WIDTH,CAP_ICON_WIDTH,10,0,LIFE_ICON_YELLOW*CAP_ICON_WIDTH);
		this->infoSprite = Sprite(sprite,0,0,1, TileInfo::TILE_WIDTH,TileInfo::TILE_HEIGHT, 10, YELLOW_X*TileInfo::TILE_WIDTH, IDLE_YELLOW_Y*TileInfo::TILE_HEIGHT);
	}

	this->shadow = Sprite(sprite, 0, 0, 4, TileInfo::TILE_WIDTH, TileInfo::TILE_HEIGHT, 15, SHADOW_IDLE_X*TileInfo::TILE_WIDTH, SHADOW_IDLE_Y*TileInfo::TILE_HEIGHT);
}

void Unit::update(Scenario* scenario)
{
	int camX = scenario->getActivePlayer()->getCamX();
	int camY = scenario->getActivePlayer()->getCamY();

	updateSprite(camX, camY);

	if(state == IDLE || state == RESTING)
	{
		spriteX = (x*TileInfo::TILE_WIDTH);
		spriteY = (y*TileInfo::TILE_HEIGHT);
	}
	else if (state == MOVING)
	{
		if((unsigned) currPathTile < path.size()-1)
		{
			if(spriteX == path[currPathTile]->getX()*TileInfo::TILE_WIDTH && spriteY == path[currPathTile]->getY()*TileInfo::TILE_HEIGHT)
			{
				if(spriteX < path[currPathTile+1]->getX()*TileInfo::TILE_WIDTH)
				{
					startMovingRight();
				}
				else if (spriteX > path[currPathTile+1]->getX()*TileInfo::TILE_WIDTH)
				{
					startMovingLeft();
				}
				else if (spriteY < path[currPathTile+1]->getY()*TileInfo::TILE_HEIGHT)
				{
					startMovingDown();
				}
				else if (spriteY > path[currPathTile+1]->getY()*TileInfo::TILE_HEIGHT)
				{
					startMovingUp();
				}

				currPathTile++;
			}
			else
			{
				if(spriteX < path[currPathTile]->getX()*TileInfo::TILE_WIDTH)
				{
					spriteX += UNIT_MOVE_SPEED;
				}
				else if (spriteX > path[currPathTile]->getX()*TileInfo::TILE_WIDTH)
				{
					spriteX -= UNIT_MOVE_SPEED;
				}
				else if (spriteY < path[currPathTile]->getY()*TileInfo::TILE_HEIGHT)
				{
					spriteY += UNIT_MOVE_SPEED;
				}
				else if (spriteY > path[currPathTile]->getY()*TileInfo::TILE_HEIGHT)
				{
					spriteY -= UNIT_MOVE_SPEED;
				}
			}
		}
		else
		{
			if(spriteX < path[currPathTile]->getX()*TileInfo::TILE_WIDTH)
			{
				spriteX += UNIT_MOVE_SPEED;
			}
			else if (spriteX > path[currPathTile]->getX()*TileInfo::TILE_WIDTH)
			{
				spriteX -= UNIT_MOVE_SPEED;
			}
			else if (spriteY < path[currPathTile]->getY()*TileInfo::TILE_HEIGHT)
			{
				spriteY += UNIT_MOVE_SPEED;
			}
			else if (spriteY > path[currPathTile]->getY()*TileInfo::TILE_HEIGHT)
			{
				spriteY -= UNIT_MOVE_SPEED;
			}
			else if (spriteX == path[currPathTile]->getX()*TileInfo::TILE_WIDTH && spriteY == path[currPathTile]->getY()*TileInfo::TILE_HEIGHT)
			{
				scenario->activateMenu(new UnitContextMenu(scenario, path[currPathTile], scenario->getFontMedium(), scenario->getFontLarge()));
				scenario->changeState(Scenario::IN_MENU);
			}
		}

		if(!canTraverse(path[currPathTile]))
		{
			scenario->trapSelectedUnit(path[currPathTile-1]);
		}
	}
}

void Unit::updateSprite(int camX, int camY)
{
	sprite.update();
	shadow.update();

	screenX = spriteX - (camX * TileInfo::TILE_WIDTH);
	screenY = spriteY - (camY * TileInfo::TILE_HEIGHT);

	sprite.setPos(screenX,screenY);
	shadow.setPos(screenX, screenY);
	captureIcon.setPos(screenX+CAP_X, screenY + CAP_Y);
	lifeIcon.setPos(screenX+HP_X,screenY+HP_Y);
}

void Unit::renderInfoSprite() const
{
	infoSprite.render();
}

void Unit::updateInfoSprite(int x, int y)
{
	infoSprite.setPos(x,y);
}

void Unit::render()
{
	if(turnedLeft)
		sprite.render(ALLEGRO_FLIP_HORIZONTAL);
	else
		sprite.render();

	if(state == RESTING)
	{
		if(turnedLeft)
			shadow.render(ALLEGRO_FLIP_HORIZONTAL);
		else
			shadow.render();
	}

	if(state == RESTING || state == IDLE)
	{
		if(isCapturing)
		{
			captureIcon.render();
		}

		renderHP();
	}
}

void Unit::renderHP()
{
	int convHP = getConvertedHP();

	if(convHP < maxHP)
	{

		char buffer [33];
		_itoa(convHP,buffer,10);

		lifeIcon.render();
		al_draw_text(hp_font,HP_TEXT_COLOR,spriteX+HP_X+HP_TEXT_X,spriteY+HP_Y+HP_TEXT_Y,0,buffer);
	}
}

Tile* Unit::getTile() const
{
	return currTile;
}

int Unit::getX() const
{
	return x;
}

int Unit::getY() const
{
	return y;
}

int Unit::getVis() const
{
	return visibility;
}

int Unit::getState() const
{
	return state;
}

int Unit::getCost() const
{
	return cost;
}

void Unit::select()
{
	startMovingRight();

	if(path.size() == 0)
		path.push_back(currTile);
}

void Unit::deselect()
{
	if(color == 'r')
		sprite.setAnimationStartY(IDLE_RED_Y*TileInfo::TILE_HEIGHT);
	else if (color == 'b')
		sprite.setAnimationStartY(IDLE_BLUE_Y*TileInfo::TILE_HEIGHT);
	else if (color == 'g')
		sprite.setAnimationStartY(IDLE_GREEN_Y*TileInfo::TILE_HEIGHT);
	else if (color == 'y')
		sprite.setAnimationStartY(IDLE_YELLOW_Y*TileInfo::TILE_HEIGHT);

	sprite.setAnimationDelay(15);
	sprite.restartAnimation();

	path.clear();
}

void Unit::changeState(int newState)
{
	this->state = newState;

	if(state == MOVING)
		currPathTile = 0;

	if(state == RESTING)
	{
		shadow.restartAnimation();
	}
}

int Unit::getMovePoints() const
{
	return movePoints;
}

bool Unit::canCapture() const
{
	return canCap;
}

void Unit::setPath(vector <Tile*> newPath)
{
	path = newPath;
}

char Unit::getColor() const
{
	return color;
}

void Unit::renderPath(ALLEGRO_BITMAP* arrows, int camX, int camY) const
{
	Tile* previousTile;
	Tile* nextTile;

	for(unsigned int i = 0; i < path.size(); i++)
	{
		int actualX = (path[i]->getX() * TileInfo::TILE_WIDTH) - (camX * TileInfo::TILE_WIDTH);
		int actualY = (path[i]->getY() * TileInfo::TILE_HEIGHT) - (camY * TileInfo::TILE_HEIGHT);

		if(i == 0)
		{
			previousTile = path[i];
		}
		else if (i == path.size() - 1)
		{
			if(previousTile->getX() < path[i]->getX())
			{
				al_draw_bitmap_region(arrows,0,160,TileInfo::TILE_WIDTH, TileInfo::TILE_HEIGHT, actualX, actualY, 0);
			}
			else if (previousTile->getX() > path[i]->getX())
			{
				al_draw_bitmap_region(arrows,0,240,TileInfo::TILE_WIDTH, TileInfo::TILE_HEIGHT, actualX, actualY, 0);
			}
			else if (previousTile->getY() < path[i]->getY())
			{
				al_draw_bitmap_region(arrows,0,80,TileInfo::TILE_WIDTH, TileInfo::TILE_HEIGHT, actualX, actualY, 0);
			}
			else
			{
				al_draw_bitmap_region(arrows,0,0,TileInfo::TILE_WIDTH, TileInfo::TILE_HEIGHT, actualX, actualY, 0);
			}
		}
		else
		{
			nextTile = path[i+1];

			if(previousTile->getY() != path[i]->getY() && nextTile->getX() == path[i]->getX())
			{
				al_draw_bitmap_region(arrows,0,320,TileInfo::TILE_WIDTH, TileInfo::TILE_HEIGHT, actualX, actualY, 0);
			}
			else if (previousTile->getY() == path[i]->getY() && nextTile->getY() == path[i]->getY())
			{
				al_draw_bitmap_region(arrows,0,400,TileInfo::TILE_WIDTH, TileInfo::TILE_HEIGHT, actualX, actualY, 0);
			}
			else if ((previousTile->getY() < path[i]->getY() && nextTile->getX() > path[i]->getX()) || (nextTile->getY() < path[i]->getY() && previousTile->getX() > path[i]->getX()))
			{
				al_draw_bitmap_region(arrows,0,480,TileInfo::TILE_WIDTH, TileInfo::TILE_HEIGHT, actualX, actualY, 0);
			}
			else if ((previousTile->getY() < path[i]->getY() && nextTile->getX() < path[i]->getX()) || (nextTile->getY() < path[i]->getY() && previousTile->getX() < path[i]->getX()))
			{
				al_draw_bitmap_region(arrows,0,560,TileInfo::TILE_WIDTH, TileInfo::TILE_HEIGHT, actualX, actualY, 0);
			}
			else if ((previousTile->getY() > path[i]->getY() && nextTile->getX() > path[i]->getX()) || (nextTile->getY() > path[i]->getY() && previousTile->getX() > path[i]->getX()))
			{
				al_draw_bitmap_region(arrows,0,640,TileInfo::TILE_WIDTH, TileInfo::TILE_HEIGHT, actualX, actualY, 0);
			}
			else if ((previousTile->getY() > path[i]->getY() && nextTile->getX() < path[i]->getX()) || (nextTile->getY() > path[i]->getY() && previousTile->getX() < path[i]->getX()))
			{
				al_draw_bitmap_region(arrows,0,720,TileInfo::TILE_WIDTH, TileInfo::TILE_HEIGHT, actualX, actualY, 0);
			}

			previousTile = path[i];
		}
	}
}

void Unit::startMovingRight()
{
	turnedLeft = false;
	if(color == 'r')
		sprite.setAnimationStartY(MOVING_SIDE_RED_Y*TileInfo::TILE_HEIGHT);
	else if (color == 'b')
		sprite.setAnimationStartY(MOVING_SIDE_BLUE_Y*TileInfo::TILE_HEIGHT);
	else if (color == 'g')
		sprite.setAnimationStartY(MOVING_SIDE_BLUE_Y*TileInfo::TILE_HEIGHT);
	else if (color == 'y')
		sprite.setAnimationStartY(MOVING_SIDE_BLUE_Y*TileInfo::TILE_HEIGHT);

	sprite.setAnimationDelay(5);
	sprite.restartAnimation();
}

void Unit::startMovingLeft()
{
	if(color == 'r')
		sprite.setAnimationStartY(MOVING_SIDE_RED_Y*TileInfo::TILE_HEIGHT);
	else if (color == 'b')
		sprite.setAnimationStartY(MOVING_SIDE_BLUE_Y*TileInfo::TILE_HEIGHT);
	else if (color == 'g')
		sprite.setAnimationStartY(MOVING_SIDE_BLUE_Y*TileInfo::TILE_HEIGHT);
	else if (color == 'y')
		sprite.setAnimationStartY(MOVING_SIDE_BLUE_Y*TileInfo::TILE_HEIGHT);

	turnedLeft = true;

	sprite.setAnimationDelay(5);
	sprite.restartAnimation();
}

void Unit::startMovingUp()
{
	turnedLeft = false;
	if(color == 'r')
		sprite.setAnimationStartY(MOVING_UP_RED_Y*TileInfo::TILE_HEIGHT);
	else if (color == 'b')
		sprite.setAnimationStartY(MOVING_UP_BLUE_Y*TileInfo::TILE_HEIGHT);
	else if (color == 'g')
		sprite.setAnimationStartY(MOVING_UP_GREEN_Y*TileInfo::TILE_HEIGHT);
	else if (color == 'y')
		sprite.setAnimationStartY(MOVING_UP_YELLOW_Y*TileInfo::TILE_HEIGHT);

	sprite.setAnimationDelay(5);
	sprite.restartAnimation();
}

void Unit::startMovingDown()
{
	turnedLeft = false;
	if(color == 'r')
		sprite.setAnimationStartY(MOVING_DOWN_RED_Y*TileInfo::TILE_HEIGHT);
	else if (color == 'b')
		sprite.setAnimationStartY(MOVING_DOWN_BLUE_Y*TileInfo::TILE_HEIGHT);
	else if (color == 'g')
		sprite.setAnimationStartY(MOVING_DOWN_GREEN_Y*TileInfo::TILE_HEIGHT);
	else if (color == 'y')
		sprite.setAnimationStartY(MOVING_DOWN_YELLOW_Y*TileInfo::TILE_HEIGHT);

	sprite.setAnimationDelay(5);
	sprite.restartAnimation();
}

void Unit::setPos(int x, int y, Tile* tile)
{
	this->x = x;
	this->y = y;

	this->currTile = tile;
}

void Unit::setTint(ALLEGRO_COLOR tint)
{
	this->sprite.setTint(tint);
	this->shadow.setTint(tint);
}

Tile* Unit::isPathObstructed()
{
	 for(unsigned int i = 0; i < path.size(); i++)
	 {
		 if(!canTraverse(path[i]) && i > 0)
		{
			return path[i-1];
		}
	 }

	 return NULL;
}

void Unit::addTileToPath(Tile* tile, vector<Tile*> shortestPath)
{
	int totalWeight = 0;

	for(unsigned int i = 0; i < path.size(); i++)
	{
		totalWeight += path[i]->getMoveCost(moveType);
	}

	totalWeight += tile->getMoveCost(moveType);

	if(isTileOnPath(tile))
	{
		path = shortestPath;
	}
	else if (!isTileAdjacentToPath(tile))
	{
		path = shortestPath;
	}
	else if(totalWeight > movePoints)
	{
		path = shortestPath;
	}
	else 
	{
		path.push_back(tile);
	}
}

bool Unit::isTileOnPath(Tile* tile) const
{
	for(unsigned int i = 0; i < path.size(); i++)
	{
		if(path[i] == tile)
			return true;
	}

	return false;
}

bool Unit::isTileAdjacentToPath(Tile* tile) const
{
	int tileX = tile->getX();
	int tileY = tile->getY();

	if(path.size() > 0)
	{
		int lastTileX = path[path.size()-1]->getX();
		int lastTileY = path[path.size()-1]->getY();

		if(tileX == lastTileX && (tileY == lastTileY + 1 || tileY == lastTileY - 1))
			return true;
		else if (tileY == lastTileY && (tileX == lastTileX + 1 || tileX == lastTileX - 1))
			return true;
	}

	return false;
}

bool Unit::canTraverse(Tile* tile)
{
	if(tile->isTraversableBy(this) && !tile->isOccupiedByEnemy(this))
		return true;
	
	return false;
}

bool Unit::isInRange(Tile* tile, Tile* target) const
{
	int x = tile->getX();
	int y = tile->getY();

	int targetX = target->getX();
	int targetY = target->getY();

	int difX = abs(x - targetX);
	int difY = abs(y - targetY);

	if((difX + difY) >= minRange && (difX + difY) <= maxRange)
		return true;

	return false;
}

bool Unit::isInMeleeRange(Tile* tile, Tile* target) const
{
	int x = tile->getX();
	int y = tile->getY();

	int targetX = target->getX();
	int targetY = target->getY();

	int difX = abs(x - targetX);
	int difY = abs(y - targetY);

	if((difX + difY) <= MELEE_RANGE && (difX + difY) >= 1)
		return true;

	return false;
}

bool Unit::canAttackRange(Unit* unit, Tile* tile) const
{
	if(isEnemy(unit) && isInRange(tile, unit->getTile()) && this->ammo > 0)
		return true;

	return false;
}

bool Unit::canAttackMelee(Unit* unit, Tile* tile) const
{
	if(isEnemy(unit) && isInMeleeRange(tile, unit->getTile()))
		return true;

	return false;
}

bool Unit::isEnemy(Unit* unit) const
{
	if(unit->getColor() != color)
		return true;
	else 
		return false;
}

void Unit::moveSpriteTo(int newX, int newY)
{
	spriteX = (newX*TileInfo::TILE_WIDTH);
	spriteY = (newY*TileInfo::TILE_HEIGHT);

	this->currPathTile = path.size() - 1;
}

void Unit::setEndMovementAnimation()
{
	if(path.size() >= 2)
	{
		Tile* endTile = path[path.size()-1];
		Tile* previousTile = path[path.size()-2];

		if(endTile->getX() < previousTile->getX())
			startMovingLeft();
		else if (endTile->getX() > previousTile->getX())
			startMovingRight();
		else if (endTile->getY() < previousTile->getY())
			startMovingUp();
		else
			startMovingDown();
	}
}

double Unit::getHP() const
{
	return hp;
}

int Unit::getConvertedHP() const
{
	int roundedDown = (int) hp;

	if((double) roundedDown == hp)
		return roundedDown;
	else
		return roundedDown+1;
}

void Unit::startCapturing()
{
	isCapturing = true;
}

void Unit::stopCapturing()
{
	isCapturing = false;
}

string Unit::getName() const
{
	return name;
}

int Unit::getAmmo() const
{
	return ammo;
}

int Unit::getMaxAmmo() const
{
	return maxAmmo;
}

int Unit::getMaxHp() const
{
	return maxHP;
}

int Unit::getType() const
{
	return type;
}

int Unit::getUnitMoveType() const
{
	return moveType;
}
