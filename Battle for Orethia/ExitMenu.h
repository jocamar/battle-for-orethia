#ifndef EXITMENU_H
#define EXITMENU_H

#include "menu.h"
#include "allegro5/allegro5.h"
#include "allegro5/allegro_font.h"

#define EXIT_MENU_TEXT_COLOR				al_map_rgb(255,255,255) //white
#define EXIT_MENU_BLOCKED_COLOR				al_map_rgb(150,150,150)  //grey

#define EXIT_MENU_OPTION1					"Yes"
#define EXIT_MENU_OPTION2					"No"

class MainMenuScreen;

class ExitMenu : public Menu
{
	static int const ExitMenuDefaultSelectedOption = 0;

	enum Fonts {UNSELECTED, SELECTED};
	enum textCoords {TEXT_X = 200, TEXT_INIT_UNSELECTED_Y = 400, TEXT_INIT_SELECTED_Y = 390, TEXT_UNSELECTED_SPACING = 100, TEXT_SELECTED_SPACING = 100, TEXT_WARNING_Y = 250, TEXT_WARNING_SPACING = 50};
	enum OptionNames {YES,NO};

	MainMenuScreen* screen;
	Menu* mainMenu;

public:

	ExitMenu(MainMenuScreen* screen, Menu* mainMenu, ALLEGRO_FONT* unselected, ALLEGRO_FONT* selected);

	void update(bool* keys);
	void render() const;

	~ExitMenu();
};



#endif