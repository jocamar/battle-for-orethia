#ifndef TRAPMENU_H
#define TRAPMENU_H

#include "allegro5/allegro5.h"
#include "allegro5/allegro_font.h"

#include "menu.h"
#include "Sprite.h"

class Scenario;
class Tile;

#define TRAP_WARNING_TIME 60
#define TRAP_TEXT "Trap!"
#define TRAP_TEXT_COLOR al_map_rgb(0,0,0)

class TrapMenu : public Menu
{
	static int const TrapMenuDefaultSelectedOption = 0;

	enum dimensions{MENUWIDTH = 137, MENUHEIGHT = 77, TEXT_Y_OFFSET = 13};

	Scenario* scenario;
	Tile* tile;
	ALLEGRO_BITMAP* background;
	ALLEGRO_FONT* font;
	int x, y;
	int counter;

public:

	TrapMenu(Scenario* scenario, Tile* tile, ALLEGRO_FONT* font, ALLEGRO_BITMAP* background);

	void update(bool* keys);
	void render() const;

	~TrapMenu();
};

#endif