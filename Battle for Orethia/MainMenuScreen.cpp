#include "MainMenuScreen.h"
#include "game.h"


MainMenuScreen::MainMenuScreen(Game* game) : Screen(game)
{
	images.push_back(al_load_bitmap("resources/images/Background.png"));

	for(unsigned int i = 0; i < images.size(); i++)
	{
		al_convert_mask_to_alpha(images[i], TRANSPARENT_MAINMENU);
	}

	titleFontSmall = al_load_font("resources/fonts/stonehen.ttf", SIZE_SMALL, 0);
	titleFontLarge = al_load_font("resources/fonts/celt.ttf", SIZE_LARGE, 0);
	menuSelected = al_load_font("resources/fonts/coolvetica rg.ttf", SIZE_MEDIUM, 0);
	menuUnselected = al_load_font("resources/fonts/coolvetica rg.ttf", SIZE_SMALL, 0);

	mainMenu = new MainMenu(this, menuUnselected, menuSelected);
}

void MainMenuScreen::update(bool* keys)
{
	mainMenu->update(keys);
}

void MainMenuScreen::drawBackground() const
{
	al_draw_bitmap(images[MMENU_BACKGROUND],0,0,0);
}

void MainMenuScreen::drawBanner() const
{
	al_draw_filled_rectangle(RECT_POINT1_X, RECT_POINT1_Y, RECT_POINT2_X, RECT_POINT2_Y, TITLE_SCREEN_BANNER_BACK_COLOR);

	al_draw_line(CONTOUR_RIGHT_LIMIT, CONTOUR_UPPER_LIMIT, CONTOUR_RIGHT_LIMIT, CONTOUR_LOWER_1, TITLE_SCREEN_BANNER_CONTOUR_COLOR, CONTOUR_THICKNESS);
	al_draw_line(CONTOUR_LEFT_LIMIT, CONTOUR_UPPER_LIMIT, CONTOUR_LEFT_LIMIT, CONTOUR_LOWER_1, TITLE_SCREEN_BANNER_CONTOUR_COLOR, CONTOUR_THICKNESS);
	al_draw_line(CONTOUR_RIGHT_LIMIT, CONTOUR_LOWER_1, CONTOUR_MIDDLE, CONTOUR_LOWER_LIMIT, TITLE_SCREEN_BANNER_CONTOUR_COLOR, CONTOUR_THICKNESS);
	al_draw_line(CONTOUR_LEFT_LIMIT, CONTOUR_LOWER_1, CONTOUR_MIDDLE, CONTOUR_LOWER_LIMIT, TITLE_SCREEN_BANNER_CONTOUR_COLOR, CONTOUR_THICKNESS);
	al_draw_line(CONTOUR_LEFT_LIMIT, CONTOUR_UPPER_LIMIT, CONTOUR_RIGHT_LIMIT, CONTOUR_UPPER_LIMIT, TITLE_SCREEN_BANNER_CONTOUR_COLOR, CONTOUR_THICKNESS);

	al_draw_text(titleFontSmall, TITLE_SCREEN_TEXT_COLOR, X1, Y1, ALLEGRO_ALIGN_CENTRE, TITLE_TEXT_1);
	al_draw_text(titleFontLarge, TITLE_SCREEN_TEXT_COLOR, X2, Y2, ALLEGRO_ALIGN_CENTRE, TITLE_TEXT_2);
}

void MainMenuScreen::render() const
{
	drawBackground();
	drawBanner();

	mainMenu->render();
}

void MainMenuScreen::switchToCampaign()
{
	//game->changeScreen(new CampaignScreen(game));
}

void MainMenuScreen::switchToBattle()
{
	game->changeScreen(new BattleMenuScreen(game));
}

void MainMenuScreen::exit()
{
	game->end();
}


MainMenuScreen::~MainMenuScreen()
{
	delete mainMenu;

	al_destroy_font(titleFontSmall);
	al_destroy_font(titleFontLarge);
	al_destroy_font(menuSelected);
	al_destroy_font(menuUnselected);
}