#include "game.h"

void Game::initializeGame(int height, int width, int fps)
{
	//--------------------------------------------------------------
	//               ALLEGRO INITIALIZATIONS
	//--------------------------------------------------------------

	al_init_primitives_addon();
	al_init_image_addon();
	al_install_mouse();
	al_install_keyboard();
	al_init_font_addon(); // initialize the font addon
	al_init_ttf_addon();// initialize the ttf (True Type Font) addon

	timer = al_create_timer(1.0 / fps);
	event_queue = al_create_event_queue();
	y

	al_register_event_source(event_queue, al_get_keyboard_event_source());  
	al_register_event_source(event_queue, al_get_display_event_source(display));
	al_register_event_source(event_queue, al_get_timer_event_source(timer));


	al_hide_mouse_cursor(display);  // uncomment to hide mouse cursor while over window
	al_start_timer(timer); 

	//------------------------------------------------------------------------
	//                      GAME INITIALIZATIONS
	//------------------------------------------------------------------------

	done = false;
	render = false;

	currScreen = NULL;

	for(int i = 0; i < NUM_OF_KEYS; i++)
	{
		keys[i] = false;
	}

	int windowWidth = al_get_display_width(display);
	int windowHeight = al_get_display_height(display);

	buffer = al_create_bitmap(width, height);

	// calculate scaling factor
	float sx = windowWidth / (float)width;
	float sy = windowHeight / (float)height;
	float scale = min(sx, sy);

	// calculate how much the buffer should be scaled
	scaleW = width * scale;
	scaleH = height * scale;
	scaleX = (windowWidth - scaleW) / 2;
	scaleY = (windowHeight - scaleH) / 2;
}

void Game::startGame()
{
	srand(unsigned(time(NULL)));  //sets the seed for rand()

	if(!al_init())  //if allegro fails to initiate does nothing, can be removed
	{
	}
	else
	{
		ALLEGRO_DISPLAY_MODE   disp_data;

		al_get_display_mode(al_get_num_display_modes() - 1, &disp_data);


		al_set_new_display_flags(/*ALLEGRO_FULLSCREEN | */ALLEGRO_OPENGL_3_0);
		display = al_create_display(WIDTH, HEIGHT);

		if(display)  
		{
			initializeGame(HEIGHT, WIDTH, FPS); // initializes game and after that changes the state to the INTRO state

			changeScreen(new IntroScreen(this));

			while(!done)  //game loop, updates and renders the game each cycle
			{
				updateGame();
				renderGame();
			}

			al_destroy_display(display);  //thrash cleaning functions for when the game is done
			al_destroy_event_queue(event_queue);
			al_destroy_timer(timer);
			delete currScreen;
		}
	}
}

void Game::updateGame()
{
	ALLEGRO_EVENT ev;  
	al_wait_for_event(event_queue, &ev);

	//==============================================================================================
	//                                   GAME UPDATE
	//==============================================================================================

	if (ev.type == ALLEGRO_EVENT_TIMER)  //these check which type of interrupt it is by checking the type data member of the event struct
	{
		render = true;  //this boolean is used to make the game only render every 1/60 of a second

		currScreen->update(keys);
	}
	else if(ev.type == ALLEGRO_EVENT_DISPLAY_CLOSE)
	{
		done = true;
	}
	else if (ev.type == ALLEGRO_EVENT_KEY_DOWN)
	{
		if (ev.keyboard.keycode == ALLEGRO_KEY_ESCAPE)
		{
			keys[ESC] = true;
		}
		else if (ev.keyboard.keycode == ALLEGRO_KEY_UP)
		{
			keys[UP] = true;
		}
		else if (ev.keyboard.keycode == ALLEGRO_KEY_DOWN)
		{
			keys[DOWN] = true; 
		}
		else if (ev.keyboard.keycode == ALLEGRO_KEY_RIGHT)
		{
			keys[RIGHT] = true;
		}
		else if (ev.keyboard.keycode == ALLEGRO_KEY_LEFT)
		{
			keys[LEFT] = true;
		}
		else if (ev.keyboard.keycode == ALLEGRO_KEY_Z)
		{
			keys[A] = true;
		}
		else if (ev.keyboard.keycode == ALLEGRO_KEY_X)
		{
			keys[B] = true;
		}
		else if (ev.keyboard.keycode == ALLEGRO_KEY_ENTER)
		{
			keys[ENTER] = true;
		}
	}
	else if (ev.type == ALLEGRO_EVENT_KEY_UP)
	{
		if (ev.keyboard.keycode == ALLEGRO_KEY_ESCAPE)
		{
			keys[ESC] = false;
		}
		else if (ev.keyboard.keycode == ALLEGRO_KEY_UP)
		{
			keys[UP] = false;
		}
		else if (ev.keyboard.keycode == ALLEGRO_KEY_DOWN)
		{
			keys[DOWN] = false; 
		}
		else if (ev.keyboard.keycode == ALLEGRO_KEY_RIGHT)
		{
			keys[RIGHT] = false;
		}
		else if (ev.keyboard.keycode == ALLEGRO_KEY_LEFT)
		{
			keys[LEFT] = false;
		}
		else if (ev.keyboard.keycode == ALLEGRO_KEY_Z)
		{
			keys[A] = false;
		}
		else if (ev.keyboard.keycode == ALLEGRO_KEY_X)
		{
			keys[B] = false;
		}
		else if (ev.keyboard.keycode == ALLEGRO_KEY_ENTER)
		{
			keys[ENTER] = false;
		}
	}
}

void Game::renderGame()
{
	if(render && al_is_event_queue_empty(event_queue)) //this means it only draws stuff to the screen if there are no events to process, I don't know yet if we'll have to do this or not
	{
		al_set_target_bitmap(buffer);
		al_clear_to_color(al_map_rgb(0, 0, 0));
		render = false;  //this boolean is used to make the game only render every 1/60 of a second

		currScreen->render();

		al_set_target_backbuffer(display);
		al_clear_to_color(al_map_rgb(0,0,0)); //this clears the screen to black
		al_draw_scaled_bitmap(buffer, 0, 0, WIDTH, HEIGHT, scaleX, scaleY, scaleW, scaleH, 0);
		al_flip_display();  //this is used for double buffering. It flips the display to the backbuffer
	}
}

void Game::end()
{
	done = true;
}

void Game::changeScreen(Screen* newScreen)
{
	delete currScreen;

	keys[A] = false;
	keys[B] = false;
	keys[ESC] = false;
	keys[ENTER] = false;
	keys[UP] = false;
	keys[DOWN] = false;
	keys[RIGHT] = false;
	keys[LEFT] = false;

	currScreen = newScreen;
}