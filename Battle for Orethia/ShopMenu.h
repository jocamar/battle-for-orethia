#ifndef SHOPMENU_H
#define SHOPMENU_H

#include "allegro5/allegro5.h"
#include "allegro5/allegro_font.h"

#include "menu.h"

#define SHOP_MENU_TEXT_COLOR					al_map_rgb(255,255,255) //white
#define SHOP_MENU_BLOCKED_COLOR				al_map_rgb(150,150,150)  //grey
#define SHOP_MENU_BACK_COLOR					al_map_rgba(0,0,0,220)  //transparent black

#define SHOP_MENU_OPTION1					"Infantry"

class ScenarioScreen;
class Scenario;
class Tile;

class ShopMenu : public Menu
{
	static int const ShopMenuDefaultSelectedOption = 0;

	enum Fonts {UNSELECTED, SELECTED};
	enum textCoords {TEXT_X = 640, TEXT_INIT_UNSELECTED_Y = 200, TEXT_INIT_SELECTED_Y = 190, TEXT_UNSELECTED_SPACING = 100, TEXT_SELECTED_SPACING = 100};
	enum Coords {MENU_LEFT_LIMIT = 490, MENU_RIGHT_LIMIT = 790, MENU_UPPER_LIMIT = 150, MENU_LOWER_LIMIT = 600};
	enum OptionNames {INFANTRY};

	ScenarioScreen* screen;
	Scenario* scenario;
	Tile* tile;

public:

	ShopMenu(ScenarioScreen* screen, Scenario* scenario, ALLEGRO_FONT* unselected, ALLEGRO_FONT* selected, Tile* tile);

	void update(bool* keys);
	void render() const;

	~ShopMenu();
};

#endif