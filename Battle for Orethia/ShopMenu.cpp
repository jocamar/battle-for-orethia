#include "ShopMenu.h"
#include "game.h"
#include "scenario.h"
#include "tile.h"

ShopMenu::ShopMenu(ScenarioScreen* screen, Scenario* scenario, ALLEGRO_FONT* unselected, ALLEGRO_FONT* selected, Tile* tile) : Menu(false, false, ShopMenu::ShopMenuDefaultSelectedOption)
{
	this->screen = screen;
	this->scenario = scenario;
	this->tile = tile;

	options.push_back(new Option(SHOP_MENU_OPTION1, true));

	fonts.push_back(unselected);
	fonts.push_back(selected);
}

void ShopMenu::update(bool* keys)
{
	scenario->updateTileInfo();
	if(active)
	{
		if(keys[DOWN])
		{
			selectedOption = getNextActive(DIR_DOWN);

			keys[DOWN] = false;
		}
		else if(keys[UP])
		{
			selectedOption = getNextActive(DIR_UP);

			keys[UP] = false;
		}
		else if(keys[A] || keys[ENTER])
		{
			if(selectedOption == INFANTRY)
			{
				keys[A] = false;
				keys[B] = false;
				keys[ESC] = false;
				keys[ENTER] = false;
				keys[UP] = false;
				keys[DOWN] = false;
				keys[RIGHT] = false;
				keys[LEFT] = false;


				if(scenario->getActivePlayer()->getResources() >= Unit::INFANTRY_COST)
				{
					this->active = false;
					this->draw = false;
					Unit* newUnit = new Unit(tile->getX(), tile->getY(), tile, screen->getImage(ScenarioScreen::FOOTTROOPER),scenario->getActivePlayer()->getColor(), screen->getImage(ScenarioScreen::UNIT_ICONS), screen->getFontTiny());
					newUnit->update(scenario);
					scenario->buyUnit(newUnit, tile);
					scenario->changeState(Scenario::IDLE);
				}
			}
		}
		else if (keys[B] || keys[ESC])
		{
			keys[A] = false;
			keys[B] = false;
			keys[ESC] = false;
			keys[ENTER] = false;
			keys[UP] = false;
			keys[DOWN] = false;
			keys[RIGHT] = false;
			keys[LEFT] = false;

			this->active = false;
			this->draw = false;
			scenario->changeState(Scenario::IDLE);
		}
	}
}

void ShopMenu::render() const
{
	if(draw)
	{
		scenario->renderBanner();

		al_draw_filled_rectangle(MENU_LEFT_LIMIT,MENU_UPPER_LIMIT, MENU_RIGHT_LIMIT, MENU_LOWER_LIMIT, SHOP_MENU_BACK_COLOR);

		for(unsigned int i = 0; i < options.size(); i++)
		{
			if(i == selectedOption)
			{
				if(options[i]->active)
					al_draw_text(fonts[SELECTED], SHOP_MENU_TEXT_COLOR, TEXT_X, TEXT_INIT_SELECTED_Y + i*TEXT_SELECTED_SPACING, ALLEGRO_ALIGN_CENTRE, options[i]->Name.c_str());
				else
					al_draw_text(fonts[SELECTED], SHOP_MENU_BLOCKED_COLOR, TEXT_X, TEXT_INIT_SELECTED_Y + i*TEXT_SELECTED_SPACING, ALLEGRO_ALIGN_CENTRE, options[i]->Name.c_str());
			}
			else
			{
				if(options[i]->active)
					al_draw_text(fonts[UNSELECTED], SHOP_MENU_TEXT_COLOR, TEXT_X, TEXT_INIT_UNSELECTED_Y + i*TEXT_UNSELECTED_SPACING, ALLEGRO_ALIGN_CENTRE, options[i]->Name.c_str());
				else
					al_draw_text(fonts[UNSELECTED], SHOP_MENU_BLOCKED_COLOR, TEXT_X, TEXT_INIT_UNSELECTED_Y + i*TEXT_UNSELECTED_SPACING, ALLEGRO_ALIGN_CENTRE, options[i]->Name.c_str());
			}
		}
	}
}

ShopMenu::~ShopMenu()
{
}