#include "CaptureMenu.h"
#include "game.h"
#include "scenario.h"

CaptureMenu::CaptureMenu(Scenario* scenario, Tile* tile, ALLEGRO_BITMAP* captureSprites, ALLEGRO_BITMAP* flagSprites, ALLEGRO_BITMAP* background) : Menu(false, false, CaptureMenu::CaptureMenuDefaultSelectedOption)
{
	this->scenario = scenario;
	this->tile = tile;
	state = RAISING_FLAG;

	x = (tile->getX()*TileInfo::TILE_WIDTH) - (scenario->getActivePlayer()->getCamX() * TileInfo::TILE_WIDTH);
	y = (tile->getY()*TileInfo::TILE_HEIGHT) - (scenario->getActivePlayer()->getCamY() * TileInfo::TILE_HEIGHT);

	if(x < WIDTH/2)
		x += MENUWIDTH/2;
	else 
		x -= (MENUWIDTH + MENUWIDTH/4);

	if(y < HEIGHT/2)
		y += MENUHEIGHT/4;
	else 
		y -= MENUHEIGHT/4;

	currFlagPosition = (tile->getLoyalty() * FLAG_POLE_PIXEL_NUM) / tile->getDefaultLoyalty();

	int finalLoyalty = tile->getLoyalty() - scenario->getSelectedUnit()->getConvertedHP();

	if (finalLoyalty <= 0)
		finished = true;
	else
		finished = false;

	finalFlagPosition =  (finalLoyalty * FLAG_POLE_PIXEL_NUM) / tile->getDefaultLoyalty();

	int cap_animation;
	int flag_animation;

	char color = scenario->getSelectedUnit()->getColor();

	if(color == 'r')
	{
		cap_animation = RED_RAISING;
		flag_animation = RED;
	}
	else if (color == 'b')
	{
		cap_animation = BLUE_RAISING;
		flag_animation = BLUE;
	}
	else if (color == 'g')
	{
		cap_animation = GREEN_RAISING;
		flag_animation = GREEN;
	}
	else if (color == 'y')
	{
		cap_animation = YELLOW_RAISING;
		flag_animation = YELLOW;
	}
	else
	{
		cap_animation = RED_RAISING;
		flag_animation = RED;
	}

	captureAnimation = Sprite(captureSprites,x,y+SPRITE_Y,CAPTURE_FRAME_NUM,MENUWIDTH,MENUHEIGHT,CAPTURE_ANIMATION_DELAY,0,cap_animation*MENUHEIGHT);
	flagAnimation = Sprite(flagSprites,x+FLAG_STARTING_X,y+(FLAG_STARTING_Y-(FLAG_POLE_PIXEL_NUM - currFlagPosition))+SPRITE_Y,FLAG_FRAME_NUM,FLAGWIDTH,FLAGHEIGHT,FLAG_ANIMATION_DELAY,0,flag_animation*FLAGHEIGHT);
	this->background = background;
}

void CaptureMenu::update(bool* keys)
{
	if(active)
	{
		scenario->updateTileInfo();

		if(state == RAISING_FLAG)
		{
			flagAnimation.setPos(flagAnimation.getX(),flagAnimation.getY()-FLAG_ASCEND_SPEED);

			if(flagAnimation.getY() <= y+(FLAG_STARTING_Y - FLAG_POLE_PIXEL_NUM)+SPRITE_Y || flagAnimation.getY() <= y+(FLAG_STARTING_Y - (FLAG_POLE_PIXEL_NUM - finalFlagPosition))+SPRITE_Y)
			{
				if(finished)
				{
					state = SALUTING;

					captureAnimation.setAnimationStartY(captureAnimation.getAnimationStartY()+MENUHEIGHT);
					captureAnimation.setAnimationDelay(SALUTE_ANIMATION_DELAY);
					captureAnimation.setFrameNum(SALUTE_FRAME_NUM);
				}
				else
				{
					scenario->stopSelectedUnitOnTile(tile);
					tile->capture();
				}
			}
		}
		else
		{
			if(captureAnimation.finishedAnimation())
			{
				scenario->stopSelectedUnitOnTile(tile);
				tile->capture();
			}
		}

		captureAnimation.update();
		flagAnimation.update();

		if(keys[A] || keys[ENTER] || keys[B] || keys[ESC])
		{
			keys[A] = false;
			keys[B] = false;
			keys[ESC] = false;
			keys[ENTER] = false;
			keys[UP] = false;
			keys[DOWN] = false;
			keys[RIGHT] = false;
			keys[LEFT] = false;

			scenario->stopSelectedUnitOnTile(tile);
			tile->capture();
		}
	}
}

void CaptureMenu::render() const
{
	if(draw)
	{
		al_draw_bitmap(background,x,y,0);
		captureAnimation.render();
		flagAnimation.render();

		scenario->renderBanner();
		scenario->renderTileInfo();
	}
}

CaptureMenu::~CaptureMenu()
{
}