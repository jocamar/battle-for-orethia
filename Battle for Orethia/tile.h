#ifndef TILE_H
#define TILE_H

#include <allegro5/allegro.h>           //   Includes allegro
#include <allegro5/allegro_primitives.h>        //   Includes allegro primitives
#include <allegro5/allegro_image.h>
#include "Sprite.h"
#include <string>
#include <vector>
#include <map>

using namespace std;

class Unit;
class Scenario;
class ScenarioScreen;

#define TILE_HIGHLIGHT_WHITE			al_map_rgba( 255*0.3f, 255*0.3f, 255*0.3f, 0.3f ) //transparent white
#define TILE_HIGHLIGHT_RED				al_map_rgba( 255*0.5f,0,0,0.5f) //transparent red
#define PLAYER_NUM 4

struct TileType {
	int id;
	char color;
	vector <Sprite> sprites;
	vector <Sprite> infoSprites;
	ALLEGRO_COLOR minimapColor;
};

class TileInfo {
	int id, spriteIndex, bgIndex;
	int moveCostLandLight, moveCostLandHeavy, moveCostWater;
	int resBonus, maxLoyalty;
	bool captureable, hideout;
	float defense;

	string name;
	Sprite battleBG;
	map<int,TileType> tileTypes;
	//TileAction whenPressedA; TODO inplement this

public:
	enum TileMeasures {TILE_WIDTH = 80, TILE_HEIGHT = 80, TILE_PREVIEW_WIDTH = 10, TILE_PREVIEW_HEIGHT = 10};
	TileInfo() {};
	TileInfo(int id, string name, int moveCostLandLight, int moveCostLandHeavy, int moveCostWater, int resBonus, int maxLoyalty, float defense, bool captureable, bool hideout, map<int,TileType> tileTypes, int spriteIndex = 0, int bgIndex = 13);
	
	int getMoveCost(int unitType) const;
	int getResourceBonus() const;
	float getDefense() const;
	string getName() const;
	int getID() const;
	char getOwnership(int tileType) const;
	bool isOwnable() const;
	bool isHideout() const;
	int getTypeByOwner(char color) const;
	int getMaxLoyalty() const;

	void updateInfoSprite(int x, int y);
	void update();
	void setSpritePos(int x, int y, int tileType);
	void setSprites(vector<ALLEGRO_BITMAP*> newSprites);

	void renderInfoSprite(int tileType) const;
	void render(int tileType, int x, int y) const;
	void renderMinimapTile(int tileType, int x, int y) const;

	void actionA(Scenario* scenario, ScenarioScreen* screen, bool* keys);
};

class Tile
{
	TileInfo* info;
	vector <TileInfo*> lastSeenByPlayer;
	vector <int> lastKnownType;
	int tileTypeID;
	bool highlightedWhite, highlightedRed;
	bool visible;
	bool occupied;
	int x, y;
	int loyalty;
	
	Unit* unitOnTile;

	//For pathfinding
	Tile* parent;
	bool traversable, traversableByHeavy;
	int moveCost;
	int dist;

public:
	Tile() {};
	Tile(const Tile & other, map<int,TileInfo> & info);
	Tile (int x, int y, TileInfo* info, int tileType, bool visible = false, bool occupied = false, bool highlightedWhite = false, bool highlightedRed = false);

	void setHW (bool newValue);
	void setHR (bool newValue);
	void setVisible (bool newValue);
	void setParent (Tile* parent);
	void setDist(int newDist);
	void setOwnership(char color);

	void occupy(Unit* unit);
	void unoccupy();

	void actionA(Scenario* scenario, ScenarioScreen* screen, bool* keys);
	void actionB(Scenario* scenario, ScenarioScreen* screen, bool* keys);

	void showSpriteFor(int playerNum); //TODO implement this functionality in render function
	void resetLoyalty();
	void capture();

	bool isHideout() const;
	bool isOwnable() const;

	void update(int camX, int camY);
	void updateInfoSprite(int x, int y);
	void updateTileFor(int playerNum);

	void render(int camX, int camY, int  playerNum) const;
	void renderMinimapTile(int mapx, int mapy, int playerNum) const;
	void renderInfoSprite(int playerNum) const;

	bool isHW() const;
	bool isHR() const;
	bool isOccupied() const;
	bool isOccupiedBy(Unit* unit) const;
	bool isVisible() const;
	bool isTraversableBy(Unit* unit) const;
	bool isOccupiedByEnemy(Unit* unit);
	int getX() const;
	int getY() const;
	int getDist() const;
	int getLoyalty() const;
	int getMoveCost(int unitType) const;
	int getResourceBonus() const;
	int getDefaultLoyalty() const;
	float getDefense() const;
	char getOwnership() const;
	char getPrevOwnershipFor(int playerNum) const;
	string getName() const;
	Unit* getUnit();
	Tile* getParent();

	//Tile* clone() const;
	~Tile();
};



#endif