#ifndef GAME_H
#define GAME_H


#include <allegro5/allegro.h>           //   Includes allegro
#include <allegro5/allegro_primitives.h>        //   Includes allegro primitives
#include <allegro5/allegro_image.h>
#include <allegro5/allegro_audio.h>
#include <allegro5/allegro_acodec.h>
#include <allegro5/allegro_font.h>
#include <allegro5/allegro_ttf.h>

#include <windows.h>
#include <fstream>
#include <string>
#include <cstdlib>
#include <iostream>
#include <vector>
#include <sstream>
#include <algorithm>

//if it stops compiling remember you deleted the screen.h include
#include "scenario.h"
#include "IntroScreen.h"
#include "MainMenuScreen.h"
#include "BattleMenuScreen.h"

using namespace std;

#define NUM_OF_KEYS 8

#define HEIGHT 720
#define WIDTH 1280
#define FPS 60

enum Screens {INTRO_SCR, MAINMENU_SCR, CAMPAIGNMENU_SCR, BATTLEMENU_SCR, EXITMENU_SCR, SCENARIO_SCR, SCENARIOSELECT_SCR};
enum Keys {UP, DOWN, LEFT, RIGHT, A, B, ESC, ENTER};


class Game
{
	//bools
	bool done, render;
	bool keys[NUM_OF_KEYS];
	ALLEGRO_BITMAP* buffer;
	int scaleW;
	int scaleH;
	int scaleX;
	int scaleY;

	Screen* currScreen;

	ALLEGRO_DISPLAY *display;
	ALLEGRO_EVENT_QUEUE *event_queue;  
	ALLEGRO_TIMER *timer;

public:

	void end();

	void startGame(); //game function

	void updateGame(); //updates the game on each cycle

	void renderGame(); //renders the game to the screen on each cycle

	void initializeGame(int height = HEIGHT, int width = WIDTH, int fps = FPS); //initializes everything that needs to be initialized before the game starts

	void changeScreen(Screen* newScreen); //changes the state of the game, executing al necessary actions
};

#endif