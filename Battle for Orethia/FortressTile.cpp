/*#include "FortressTile.h"
#include "unit.h"
#include "scenario.h"
#include "ScenarioPauseMenu.h"
#include "ScenarioScreen.h"
#include "ShopMenu.h"

float const FortressTile::fortressDefence = 0.5;
string const FortressTile::fortressTypeA = "FortressRed";
string const FortressTile::fortressTypeB = "FortressBlue";
string const FortressTile::fortressTypeC = "FortressGreen";
string const FortressTile::fortressTypeD = "FortressYellow";
string const FortressTile::fortressTypeE = "FortressWhite";
string const FortressTile::fortressTitle = "Redoubt";

FortressTile::FortressTile(int x,int y, string type, ALLEGRO_BITMAP* image) : Tile(x, y)
{
	traversable = true;
	traversableByHeavy = true;
	bonusDefense = FortressTile::fortressDefence;
	moveCost = FortressTile::fortressMoveCost;
	resourceBonus = FortressTile::fortressResourceBonus;
	loyalty = FortressTile::fortressLoyalty;
	this->tileName = FortressTile::fortressTitle;

	this->type = type;

	for(unsigned int i = 0; i < PLAYER_NUM; i++)
	{
		this->lastKnownType[i] = this->type;
	}

	if(type == FortressTile::fortressTypeA)
	{
		this->sprite = Sprite(image, x, y, FortressTile::fortressFrameNum, TILE_WIDTH, TILE_HEIGHT, FortressTile::fortressAnimationDelay, FORTRESS_RED_X*TILE_WIDTH, FORTRESS_RED_Y*TILE_HEIGHT);
		this->infoSprite = Sprite(image, 0, 0, 1, TILE_WIDTH, TILE_HEIGHT, FortressTile::fortressAnimationDelay, FORTRESS_RED_X*TILE_WIDTH, FORTRESS_RED_Y*TILE_HEIGHT);
	}
	else if (type == FortressTile::fortressTypeB)
	{
		this->sprite = Sprite(image, x, y, FortressTile::fortressFrameNum, TILE_WIDTH, TILE_HEIGHT, FortressTile::fortressAnimationDelay, FORTRESS_BLUE_X*TILE_WIDTH, FORTRESS_BLUE_Y*TILE_HEIGHT);
		this->infoSprite = Sprite(image, 0, 0, 1, TILE_WIDTH, TILE_HEIGHT, FortressTile::fortressAnimationDelay, FORTRESS_BLUE_X*TILE_WIDTH, FORTRESS_BLUE_Y*TILE_HEIGHT);
	}
	else if (type == FortressTile::fortressTypeC)
	{
		this->sprite = Sprite(image, x, y, FortressTile::fortressFrameNum, TILE_WIDTH, TILE_HEIGHT, FortressTile::fortressAnimationDelay, FORTRESS_GREEN_X*TILE_WIDTH, FORTRESS_GREEN_Y*TILE_HEIGHT);
		this->infoSprite = Sprite(image, 0, 0, 1, TILE_WIDTH, TILE_HEIGHT, FortressTile::fortressAnimationDelay, FORTRESS_GREEN_X*TILE_WIDTH, FORTRESS_GREEN_Y*TILE_HEIGHT);
	}
	else if (type == FortressTile::fortressTypeD)
	{
		this->sprite = Sprite(image, x, y, FortressTile::fortressFrameNum, TILE_WIDTH, TILE_HEIGHT, FortressTile::fortressAnimationDelay, FORTRESS_YELLOW_X*TILE_WIDTH, FORTRESS_YELLOW_Y*TILE_HEIGHT);
		this->infoSprite = Sprite(image, 0, 0, 1, TILE_WIDTH, TILE_HEIGHT, FortressTile::fortressAnimationDelay, FORTRESS_YELLOW_X*TILE_WIDTH, FORTRESS_YELLOW_Y*TILE_HEIGHT);
	}
	else if (type == FortressTile::fortressTypeE)
	{
		this->sprite = Sprite(image, x, y, FortressTile::fortressFrameNum, TILE_WIDTH, TILE_HEIGHT, FortressTile::fortressAnimationDelay, FORTRESS_WHITE_X*TILE_WIDTH, FORTRESS_WHITE_Y*TILE_HEIGHT);
		this->infoSprite = Sprite(image, 0, 0, 1, TILE_WIDTH, TILE_HEIGHT, FortressTile::fortressAnimationDelay, FORTRESS_WHITE_X*TILE_WIDTH, FORTRESS_WHITE_Y*TILE_HEIGHT);
	}
}

bool FortressTile::isOwnable() const
{
	return true;
}

void FortressTile::renderPreview(int posX, int posY) const
{
	int actualX = (x * TILE_PREVIEW_WIDTH) + posX;
	int	actualY = (y * TILE_PREVIEW_HEIGHT) + posY;

	if(type == FortressTile::fortressTypeA)
	{
		al_draw_filled_rectangle(actualX,actualY, actualX + TILE_PREVIEW_WIDTH, actualY + TILE_PREVIEW_HEIGHT,FORTRESS_RED_COLOR);
	}
	else if (type == FortressTile::fortressTypeB)
	{
		al_draw_filled_rectangle(actualX, actualY, actualX + TILE_PREVIEW_WIDTH, actualY + TILE_PREVIEW_HEIGHT,FORTRESS_BLUE_COLOR);
	}
	else if (type == FortressTile::fortressTypeC)
	{
		al_draw_filled_rectangle(actualX, actualY, actualX + TILE_PREVIEW_WIDTH, actualY + TILE_PREVIEW_HEIGHT,FORTRESS_GREEN_COLOR);
	}
	else if (type == FortressTile::fortressTypeD)
	{
		al_draw_filled_rectangle(actualX, actualY, actualX + TILE_PREVIEW_WIDTH, actualY + TILE_PREVIEW_HEIGHT,FORTRESS_YELLOW_COLOR);
	}
	else if (type == FortressTile::fortressTypeE)
	{
		al_draw_filled_rectangle(actualX, actualY, actualX + TILE_PREVIEW_WIDTH, actualY + TILE_PREVIEW_HEIGHT,FORTRESS_WHITE_COLOR);
	}
}

void FortressTile::setOwnership(char color)
{
	if(color == 'r')
	{
		this->type = FortressTile::fortressTypeA;
	}
	else if (color == 'b')
	{
		this->type = FortressTile::fortressTypeB;
	}
	else if (color == 'g')
	{
		this->type = FortressTile::fortressTypeC;
	}
	else if (color == 'y')
	{
		this->type = FortressTile::fortressTypeD;
	}
	else if (color == 'w')
	{
		this->type = FortressTile::fortressTypeE;
	}
}

char FortressTile::getOwnership() const
{
	if(type == FortressTile::fortressTypeA)
	{
		return 'r';
	}
	else if (type == FortressTile::fortressTypeB)
	{
		return 'b';
	}
	else if (type == FortressTile::fortressTypeC)
	{
		return 'g';
	}
	else if (type == FortressTile::fortressTypeD)
	{
		return 'y';
	}
	else if (type == FortressTile::fortressTypeE)
		return 'w';

	return '\0';
}

char FortressTile::getPrevOwnershipFor(int playerNum) const
{
	if(lastKnownType[playerNum] == FortressTile::fortressTypeA)
	{
		return 'r';
	}
	else if (lastKnownType[playerNum] == FortressTile::fortressTypeB)
	{
		return 'b';
	}
	else if (lastKnownType[playerNum] == FortressTile::fortressTypeC)
	{
		return 'g';
	}
	else if (lastKnownType[playerNum] == FortressTile::fortressTypeD)
	{
		return 'y';
	}
	else if (lastKnownType[playerNum] == FortressTile::fortressTypeE)
		return 'w';

	return '\0';
}

FortressTile* FortressTile::clone() const
{
	return new FortressTile(*this);
}

void FortressTile::doActionA(Scenario* scenario, ScenarioScreen* screen, bool* keys)
{
	if(scenario->getState() == Scenario::IDLE)
	{
		if(occupied)
		{
			if(unitOnTile->getState() == Unit::RESTING || !visible)
			{
				scenario->activateMenu(new ScenarioPauseMenu(screen, scenario, screen->getFontMedium(), screen->getFontLarge()));
				scenario->changeState(Scenario::IN_MENU);
			}
			else if (unitOnTile->getState() == Unit::IDLE && unitOnTile->getColor() == scenario->getActivePlayer()->getColor())
			{
				scenario->selectUnit(unitOnTile);
			}
			else if (unitOnTile->getState() == Unit::IDLE)
			{
				scenario->selectUnit(unitOnTile);
				keys[A] = true;
				scenario->changeState(Scenario::CHECKING_UNIT_MOVEMENT);
			}
		}
		else if (getOwnership() != scenario->getActivePlayer()->getColor())
		{
			scenario->activateMenu(new ScenarioPauseMenu(screen, scenario, screen->getFontMedium(), screen->getFontLarge()));
			scenario->changeState(Scenario::IN_MENU);
		}
		else
		{
			scenario->activateMenu(new ShopMenu(screen, scenario, screen->getFontMedium(), screen->getFontLarge(), this));
			scenario->changeState(Scenario::IN_MENU);
		}
	}
	else if(scenario->getState() == Scenario::SELECTING_UNIT)
	{
		if(( !occupied || isOccupiedBy(scenario->getSelectedUnit()) ) && highlightedWhite)
		{
			scenario->startUnitMovement();
			scenario->changeState(Scenario::UNIT_MOVING);
		}
	}
}

FortressTile::~FortressTile()
{
}

void FortressTile::updateTileFor(int playerNum)
{
	this->lastKnownType[playerNum] = this->type;
}

void FortressTile::switchSpriteFor(int playerNum)
{
	if(this->lastKnownType[playerNum] == FortressTile::fortressTypeA)
	{
		this->sprite.setAnimationStartX(FORTRESS_RED_X*TILE_WIDTH);
		this->sprite.setAnimationStartY(FORTRESS_RED_Y*TILE_HEIGHT);
		this->infoSprite.setAnimationStartX(FORTRESS_RED_X*TILE_WIDTH);
		this->infoSprite.setAnimationStartY(FORTRESS_RED_Y*TILE_HEIGHT);
	}
	else if (this->lastKnownType[playerNum] == FortressTile::fortressTypeB)
	{
		this->sprite.setAnimationStartX(FORTRESS_BLUE_X*TILE_WIDTH);
		this->sprite.setAnimationStartY(FORTRESS_BLUE_Y*TILE_HEIGHT);
		this->infoSprite.setAnimationStartX(FORTRESS_BLUE_X*TILE_WIDTH);
		this->infoSprite.setAnimationStartY(FORTRESS_BLUE_Y*TILE_HEIGHT);
	}
	else if (this->lastKnownType[playerNum] == FortressTile::fortressTypeC)
	{
		this->sprite.setAnimationStartX(FORTRESS_GREEN_X*TILE_WIDTH);
		this->sprite.setAnimationStartY(FORTRESS_GREEN_Y*TILE_HEIGHT);
		this->infoSprite.setAnimationStartX(FORTRESS_GREEN_X*TILE_WIDTH);
		this->infoSprite.setAnimationStartY(FORTRESS_GREEN_Y*TILE_HEIGHT);
	}
	else if (this->lastKnownType[playerNum] == FortressTile::fortressTypeD)
	{
		this->sprite.setAnimationStartX(FORTRESS_YELLOW_X*TILE_WIDTH);
		this->sprite.setAnimationStartY(FORTRESS_YELLOW_Y*TILE_HEIGHT);
		this->infoSprite.setAnimationStartX(FORTRESS_YELLOW_X*TILE_WIDTH);
		this->infoSprite.setAnimationStartY(FORTRESS_YELLOW_Y*TILE_HEIGHT);
	}
	else if (this->lastKnownType[playerNum] == FortressTile::fortressTypeE)
	{
		this->sprite.setAnimationStartX(FORTRESS_WHITE_X*TILE_WIDTH);
		this->sprite.setAnimationStartY(FORTRESS_WHITE_Y*TILE_HEIGHT);
		this->infoSprite.setAnimationStartX(FORTRESS_WHITE_X*TILE_WIDTH);
		this->infoSprite.setAnimationStartY(FORTRESS_WHITE_Y*TILE_HEIGHT);
	}
}

void FortressTile::resetLoyalty()
{
	loyalty = FortressTile::fortressLoyalty;
}

void FortressTile::capture()
{
	unitOnTile->startCapturing();
	loyalty -= unitOnTile->getConvertedHP();

	if(loyalty <= 0)
	{
		setOwnership(unitOnTile->getColor());
		resetLoyalty();
		unitOnTile->stopCapturing();
	}
}

int FortressTile::getDefaultLoyalty() const
{
	return FortressTile::fortressLoyalty;
}*/