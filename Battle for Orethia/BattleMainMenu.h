#ifndef BATTLEMAINMENU_H
#define BATTLEMAINMENU_H

#include "menu.h"
#include "allegro5/allegro5.h"
#include "allegro5/allegro_font.h"
#include "NewBattleMenu.h"
//#include "LoadBattleMenu.h"

#define BATTLE_MAIN_MENU_TEXT_COLOR				al_map_rgb(255,255,255) //white
#define BATTLE_MAIN_MENU_BLOCKED_COLOR			al_map_rgb(150,150,150)  //grey
#define BATTLE_MAIN_MENU_CONTOUR_COLOR			al_map_rgb(255,255,255) //white

#define BATTLE_MAIN_MENU_OPTION1			"New Battle"
#define BATTLE_MAIN_MENU_OPTION2			"Load Battle"

class BattleMenuScreen;

class BattleMainMenu : public Menu
{
	static int const BattleMainMenuDefaultSelectedOption = 0;

	enum Fonts {ACTIVE, NORMAL};
	enum textCoords {
					 TEXT_X = 160, 
					 INIT_OPTION_UNSELECTED_Y = 255, 
					 INIT_OPTION_SELECTED_Y = 250, 
					 OPTION2_UNSELECTED_SPACING = 200, 
					 OPTION2_SELECTED_SPACING = 200
					};
	enum OptionNames {NEW_BATTLE, LOAD_BATTLE};
	enum ContourCoords {
						LEFT_LIMIT = 300,
						LEFTEST_LIMIT = 30,
						RIGHT_LIMIT = 1250, 
						UPPER_LIMIT = 80, 
						LOWER_LIMIT = 690, 
						OPTION1_UPPER_LIMIT = 230, 
						OPTION1_LOWER_LIMIT = 330, 
						OPTION2_UPPER_LIMIT = 430, 
						OPTION2_LOWER_LIMIT = 530,
						TITLE_LEFT_LIMIT = 500,
						TITLE_RIGHT_LIMIT = 780,
						CONTOUR_THICKNESS = 4
					};

	NewBattleMenu* newBattleMenu;
	//LoadBattleMenu* loadBattleMenu;
	
	int activeOption;

	BattleMenuScreen* screen;

	void drawContours() const;

public:

	BattleMainMenu(BattleMenuScreen* screen, ALLEGRO_FONT* unselected, ALLEGRO_FONT* selected);

	void update(bool* keys);
	void render() const;

	~BattleMainMenu();
};



#endif