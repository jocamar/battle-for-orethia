#include "SelectScenarioMenu.h"
#include "game.h"
#include "BattleMenuScreen.h"
#include "NewBattleMenu.h"

SelectScenarioMenu::SelectScenarioMenu(BattleMenuScreen* screen, NewBattleMenu* newBattleMenu, ALLEGRO_FONT* unselected, ALLEGRO_FONT* selected) : Menu(false, false, SelectScenarioMenu::SelectScenarioMenuDefaultSelectedOption)
{
	this->screen = screen;

	vector<Map> maps = screen->getMaps();

	for(int i = 0; (unsigned)i < maps.size(); i++)
	{
		options.push_back(new Option(maps[i].getName(), true));
	}

	fonts.push_back(unselected);
	fonts.push_back(selected);

	this->newBattleMenu = newBattleMenu;

	previouslySelectedOption = selectedOption;
}

void SelectScenarioMenu::update(bool* keys)
{
	if(active)
	{
		if(keys[DOWN])
		{
			selectedOption = getNextActive(DIR_DOWN);

			keys[DOWN] = false;
		}
		else if(keys[UP])
		{
			selectedOption = getNextActive(DIR_UP);

			keys[UP] = false;
		}
		else if(keys[A] || keys[ENTER])
		{
			
				keys[A] = false;
				keys[B] = false;
				keys[ESC] = false;
				keys[ENTER] = false;
				keys[UP] = false;
				keys[DOWN] = false;
				keys[RIGHT] = false;
				keys[LEFT] = false;

				screen->setChosenMapIndex(selectedOption);
				previouslySelectedOption = selectedOption;

				newBattleMenu->resetPlayers();

				this->active = false;
				this->draw = false;
				newBattleMenu->setActive(true);
				newBattleMenu->setDrawing(true);
		}
		else if (keys[B] || keys[ESC])
		{
			keys[A] = false;
			keys[B] = false;
			keys[ESC] = false;
			keys[ENTER] = false;
			keys[UP] = false;
			keys[DOWN] = false;
			keys[RIGHT] = false;
			keys[LEFT] = false;

			selectedOption = previouslySelectedOption;

			this->active = false;
			this->draw = false;
			newBattleMenu->setActive(true);
			newBattleMenu->setDrawing(true);
		}
	}
}

void SelectScenarioMenu::render() const
{
	if(draw)
	{
		for(unsigned int i = 0; i < options.size(); i++)
		{
			if(i == selectedOption)
			{
				if(options[i]->active)
					al_draw_text(fonts[SELECTED], SELECT_SCENARIO_MENU_TEXT_COLOR, MAP_TITLE_X, MAP_TITLE_SELECTED_INIT_Y + i*MAP_TITLE_SPACING, ALLEGRO_ALIGN_CENTRE, options[i]->Name.c_str());
				else
					al_draw_text(fonts[SELECTED], SELECT_SCENARIO_MENU_BLOCKED_COLOR, MAP_TITLE_X, MAP_TITLE_SELECTED_INIT_Y + i*MAP_TITLE_SPACING, ALLEGRO_ALIGN_CENTRE, options[i]->Name.c_str());
			}
			else
			{
				if(options[i]->active)
					al_draw_text(fonts[UNSELECTED], SELECT_SCENARIO_MENU_TEXT_COLOR, MAP_TITLE_X, MAP_TITLE_UNSELECTED_INIT_Y + i*MAP_TITLE_SPACING, ALLEGRO_ALIGN_CENTRE, options[i]->Name.c_str());
				else
					al_draw_text(fonts[UNSELECTED], SELECT_SCENARIO_MENU_BLOCKED_COLOR, MAP_TITLE_X, MAP_TITLE_UNSELECTED_INIT_Y + i*MAP_TITLE_SPACING, ALLEGRO_ALIGN_CENTRE, options[i]->Name.c_str());
			}
		}

		screen->getMaps()[selectedOption].renderMinimap(MAP_PREVIEW_X, MAP_PREVIEW_Y,0);
	}
}

SelectScenarioMenu::~SelectScenarioMenu()
{
}