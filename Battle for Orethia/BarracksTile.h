#ifndef BARRACKSTILE_H
#define BARRACKSTILE_H
/*
#include "tile.h"

#define BARRACKS_RED_COLOR				al_map_rgb(255,0,0)
#define BARRACKS_BLUE_COLOR				al_map_rgb(0,0,255)
#define BARRACKS_GREEN_COLOR			al_map_rgb(0,255,0)
#define BARRACKS_YELLOW_COLOR			al_map_rgb(255,255,0)
#define BARRACKS_WHITE_COLOR			al_map_rgb(255,255,255)

class BarracksTile : public Tile
{
	static float const barracksDefence;
	static int const barracksFrameNum = 1;
	static int const barracksMoveCost = 2;
	static int const barracksAnimationDelay = 15;
	static int const barracksResourceBonus = 0;
	static int const barracksLoyalty = 20;
	static string const barracksTypeA;
	static string const barracksTypeB;
	static string const barracksTypeC;
	static string const barracksTypeD;
	static string const barracksTypeE;
	static string const barracksTitle;

	enum TileTypeAnimationStartCoords {BARRACKS_RED_X = 5, BARRACKS_RED_Y = 0, 
										BARRACKS_BLUE_X = 5, BARRACKS_BLUE_Y = 1, 
										BARRACKS_GREEN_X = 5, BARRACKS_GREEN_Y = 2,
										BARRACKS_YELLOW_X = 5, BARRACKS_YELLOW_Y = 3, 
										BARRACKS_WHITE_X = 5, BARRACKS_WHITE_Y = 4};

	string type;
	string lastKnownType[PLAYER_NUM];

public:
	BarracksTile(int x,int y,string type, ALLEGRO_BITMAP* image);

	bool isOwnable() const;

	void updateTileFor(int playerNum);
	void switchSpriteFor(int playerNum);

	void renderPreview(int x, int y) const;

	void setOwnership(char color);
	void resetLoyalty();
	void capture();
	char getOwnership() const;
	char getPrevOwnershipFor(int playerNum) const;
	int getDefaultLoyalty() const;

	void doActionA(Scenario* scenario, ScenarioScreen* screen, bool* keys);

	BarracksTile* clone() const;
	~BarracksTile();
};
*/
#endif