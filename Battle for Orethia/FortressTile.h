#ifndef FORTRESSTILE_H
#define FORTRESSTILE_H
/*
#include "tile.h"

#define FORTRESS_RED_COLOR			al_map_rgb(255,0,0)
#define FORTRESS_BLUE_COLOR			al_map_rgb(0,0,255)
#define FORTRESS_GREEN_COLOR		al_map_rgb(0,255,0)
#define FORTRESS_YELLOW_COLOR		al_map_rgb(255,255,0)
#define FORTRESS_WHITE_COLOR		al_map_rgb(255,255,255)

class FortressTile : public Tile
{
	static float const fortressDefence;
	static int const fortressFrameNum = 1;
	static int const fortressMoveCost = 2;
	static int const fortressAnimationDelay = 15;
	static int const fortressResourceBonus = 1000;
	static int const fortressLoyalty = 20;
	static string const fortressTypeA;
	static string const fortressTypeB;
	static string const fortressTypeC;
	static string const fortressTypeD;
	static string const fortressTypeE;
	static string const fortressTitle;

	enum TileTypeAnimations {
							FORTRESS_RED_X = 0, FORTRESS_RED_Y = 0,
							FORTRESS_BLUE_X = 0, FORTRESS_BLUE_Y = 1,
							FORTRESS_GREEN_X = 0, FORTRESS_GREEN_Y = 2, 
							FORTRESS_YELLOW_X = 0, FORTRESS_YELLOW_Y = 3, 
							FORTRESS_WHITE_X = 0, FORTRESS_WHITE_Y = 4
	};

	string type;
	string lastKnownType[PLAYER_NUM];

public:
	FortressTile(int x,int y,string type, ALLEGRO_BITMAP* image);

	bool isOwnable() const;

	void updateTileFor(int playerNum);
	void switchSpriteFor(int playerNum);

	void renderPreview(int x, int y) const;

	void setOwnership(char color);
	char getOwnership() const;
	char getPrevOwnershipFor(int playerNum) const;
	void resetLoyalty();
	int getDefaultLoyalty() const;
	void capture();

	void doActionA(Scenario* scenario, ScenarioScreen* screen, bool* keys);

	FortressTile* clone() const;
	~FortressTile();
};
*/
#endif