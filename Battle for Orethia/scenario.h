#ifndef SCENARIO_H
#define SCENARIO_H

#include "tile.h"
#include "gameMap.h"
#include "player.h"
#include "BannerElement.h"
#include "TileInfoElement.h"
#include <vector>
#include <stdlib.h>

using namespace std;

class Menu;
class ScenarioScreen;

#define HOLD_TIME 5
#define DELAY_TIME 1
#define NUM_KEYS 8

class Scenario
{
	int numRemainingPlayers;
	int turnNum, activePlayer;
	int scenarioState;

	enum DefaultInitialization {TURN_NUM = 1, ACTIVE_PLAYER = 0, STARTING_STATE = 0};

	ScenarioScreen* screen;

	Menu* currMenu;
	Map map;
	Unit* selectedUnit;
	vector <Player> players;
	BannerElement currBanner;
	TileInfoElement tileInfoBanner;


	int keyHoldCounter;
	int tileMoveDelayCounter;
	bool scrolling;

	bool keysHeld[NUM_KEYS];

public:
	enum ScenarioStates {PRETURN, IDLE, IN_MENU, SELECTING_UNIT, UNIT_MOVING, CHECKING_MAP, CHECKING_UNIT_RANGE, CHECKING_UNIT_MOVEMENT};

	Scenario(int numRemainingPlayers, vector <Player> players, Map map);

	int getNumPlayers() const;
	int getTurnNum() const;
	int getActivePlayerNum() const;
	int getState() const;
	Player* getActivePlayer();
	ScenarioScreen* getScreen();
	bool getNumRemainingPlayers() const;
	Unit* getSelectedUnit() const;
	Tile* getSelectedTile();
	Map* getMap();
	vector <Unit*> getAttackableUnitsMelee(Tile* tile);
	vector <Unit*> getAttackableUnitsRange(Tile* tile);
	ALLEGRO_FONT* getFontMedium() const;
	ALLEGRO_FONT* getFontLarge() const;

	void update(bool* keys);
	void render(ALLEGRO_FONT* font) const;
	void printMap() const;
	void passTurn();
	void buyUnit(Unit* newUnit, Tile* tile);
	void setActivePlayer(int newValue);
	void setScreen(ScenarioScreen* screen);
	void changeState(int newState);
	void updateMapImages(vector<ALLEGRO_BITMAP*> tiles, ALLEGRO_BITMAP* fog);
	void updatePlayerImages(ALLEGRO_BITMAP* cursor);
	void activateMenu(Menu* newMenu);
	void selectUnit(Unit* unitToSelect);
	void unselectUnit();
	void stopSelectedUnitOnTile(Tile* tile);
	void setSelectedUnitPath(Tile* dest);
	void startUnitMovement();
	void initializeBanner(ALLEGRO_BITMAP* backgrounds, ALLEGRO_FONT* font, ALLEGRO_FONT* smallfont);
	void initializeTileInfo(ALLEGRO_BITMAP* backgrounds, ALLEGRO_BITMAP* icons, ALLEGRO_FONT* font, ALLEGRO_FONT* smallfont);
	void setUnitsTint(ALLEGRO_COLOR tint);
	void checkUnitRange(Unit* unit);
	void trapSelectedUnit(Tile* tile);

	void updatePreturn(bool* keys);
	void updateIdle(bool* keys);
	void updateInMenu(bool* keys);
	void updateSelectingUnit(bool* keys);
	void updateUnitMoving(bool* keys);
	void updateCursor(bool* keys, Player* player);
	void updateTileInfo();
	void updateCheckingMap(bool* keys);
	void updateCheckingRange(bool* keys);
	void updateCheckingMovement(bool* keys);

	void renderPreturn(ALLEGRO_FONT* font) const;
	void renderIdle() const;
	void renderInMenu() const;
	void renderCheckingMap() const;
	void renderBanner() const;
	void renderTileInfo() const;


	~Scenario();

};

#endif