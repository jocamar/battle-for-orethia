#ifndef MAINMENUSCREEN_H
#define MAINMENUSCREEN_H

#include "screen.h"

#include "allegro5/allegro5.h"
#include "allegro5/allegro_primitives.h"
#include "allegro5/allegro_font.h"

#include "MainMenu.h"

#define TITLE_SCREEN_TEXT_COLOR					al_map_rgb(255,255,255) //white
#define TITLE_SCREEN_BANNER_BACK_COLOR			al_map_rgba(0,0,0,220)  //transparent black
#define TITLE_SCREEN_BANNER_CONTOUR_COLOR		al_map_rgb(255,255,255) //white
#define TRANSPARENT_MAINMENU					al_map_rgb(255,0,255) //magenta

#define TITLE_TEXT_1 "Battle for"
#define TITLE_TEXT_2 "Orethia"

class Game;

class MainMenuScreen : public Screen
{
	enum ImageID {MMENU_BACKGROUND};
	enum FontSizes {SIZE_LARGE = 60, SIZE_SMALL = 35, SIZE_MEDIUM = 45};
	enum TitleCoords {X1 = 133, Y1 = 67, X2 = 200, Y2 = 107};
	enum BannerCoords {
					   RECT_POINT1_X = 0, 
					   RECT_POINT1_Y = 0, 
					   RECT_POINT2_X = 400, 
					   RECT_POINT2_Y = 720, 
					   CONTOUR_LEFT_LIMIT = 20, 
					   CONTOUR_RIGHT_LIMIT = 380, 
					   CONTOUR_UPPER_LIMIT = 50, 
					   CONTOUR_LOWER_1 = 580, 
					   CONTOUR_LOWER_LIMIT = 680,
					   CONTOUR_MIDDLE = 200,
					   CONTOUR_THICKNESS = 4
					  };

	MainMenu* mainMenu;

	ALLEGRO_FONT* titleFontLarge;
	ALLEGRO_FONT* titleFontSmall;
	ALLEGRO_FONT* menuSelected;
	ALLEGRO_FONT* menuUnselected;

	void drawBanner() const;
	void drawBackground() const;

public:
	MainMenuScreen(Game* game);

	void update(bool* keys);
	void render() const;

	void switchToCampaign();
	void switchToBattle();
	void exit();

	~MainMenuScreen();
};

#endif