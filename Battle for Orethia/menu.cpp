#include "menu.h"
#include "game.h"


Menu::Menu()
{
	this->active = true;
	this->draw = true;
	this->defaultSelectedOption = 0;
	this->selectedOption = 0;
}

Menu::Menu(bool active, bool draw, int defaultSelectedOption)
{
	this->active = active;
	this->draw = draw;
	this->defaultSelectedOption = defaultSelectedOption;
	this->selectedOption = defaultSelectedOption;
}

void Menu::setActive(bool newActive, bool resetSelectedOption)
{
	if(resetSelectedOption)
		this->selectedOption = defaultSelectedOption;

	active = newActive;
}

bool Menu::isActive() const
{
	return active;
}

int Menu::getNextActive(int direction) const
{
	int currOption = selectedOption;

	if(atLeastOneActive())
	{
		do
		{
			if(direction == DIR_UP)
				currOption--;
			else if (direction == DIR_DOWN)
				currOption++;

			if(currOption < 0)
				currOption = options.size()-1;
			else if ((unsigned)currOption >= options.size())
				currOption = 0;
		}
		while(!options[currOption]->active);
	}

	return currOption;
}

bool Menu::atLeastOneActive() const
{
	bool rtrnval = false;

	for(unsigned int i = 0; i < options.size(); i++)
	{
		if(options[i]->active)
			rtrnval = true;
	}

	return rtrnval;
}

bool Menu::isDrawing() const
{
	return draw;
}

void Menu::setDrawing(bool newDraw)
{
	draw = newDraw;
}

Menu::~Menu()
{
	for(unsigned int i = 0; i < options.size(); i++)
	{
		delete options[i];
	}
}