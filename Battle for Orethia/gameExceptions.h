#ifndef GAME_EXCEPTIONS_H
#define GAME_EXCEPTIONS_H

#include <string>
#include <iostream>

using namespace std;

class gameException {
public:
	virtual void getInfo() = 0;
};

class errorOpeningMap : public gameException {
	string fileName;
public:
	errorOpeningMap(string fileName) : fileName(fileName) {};
	void getInfo() { cout << "Error opening map: " << fileName << endl; };
};

class errorOpeningDirectory {
	string directory;
public:
	errorOpeningDirectory(string directory) : directory(directory) {};
	void getInfo() {cout << "Error opening directory: " << directory << endl; }
};

class errorLoadingTileInfo : public gameException {
public:
	void getInfo() { cout << "Error loading tile information" << endl; };
};

class errorLoadingTile : public gameException {
public:
	void getInfo() { cout << "Error loading tile" << endl; };
};

class errorLoadingTerrain : public gameException {
	string file;
public:
	errorLoadingTerrain(string file) : file(file) {};
	void getInfo() { cout << "Error loading terrain info from " << file << endl; };
};

#endif