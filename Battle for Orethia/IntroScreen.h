#ifndef INTROSCREEN_H
#define INTROSCREEN_H

#include "screen.h"
#include "allegro5/allegro5.h"
#include "game.h"

#define INTRO_TIME 300

class IntroScreen : public Screen
{
	enum ImageID {INTRO_BCKGROUND};
	int counter, startCounter;

public:
	IntroScreen(Game* game);

	void update(bool* keys);
	void render() const;

	~IntroScreen();
};

#endif